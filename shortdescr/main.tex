\documentclass[a4paper]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usetikzlibrary{trees,shapes}
\usepackage[appendix=append]{apxproof}
\usepackage{relsize}
\usepackage{stackengine}
\stackMath
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{todonotes}
\usepackage{graphics,color,caption,subcaption}
\usepackage{hyperref}
\hypersetup{
	    colorlinks,
	    linkcolor={red!50!black},
	    citecolor={blue!50!black},
	    urlcolor={blue!30!black},
	    breaklinks=true
	    }
\usepackage{mathabx}
\usepackage{bbm}

\newtheoremrep{theorem}{Theorem}
\newtheorem*{example*}{Example}
\newtheorem*{conjecture*}{Conjecture}
% \newtheoremrep{theorem}{Theorem}[section]
\newtheorem{openpb}[theorem]{Open problem}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{fact}[theorem]{Fact}
\newtheoremrep{proposition}[theorem]{Proposition}
\newtheoremrep{claim}[theorem]{Claim}
\newtheoremrep{corollary}[theorem]{Corollary}
\newtheoremrep{lemma}[theorem]{Lemma}
\newtheoremrep{example}[theorem]{Example}
\newtheoremrep{definition}[theorem]{Definition}

\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\NN}{\mathbb{N}}


\input{macros}


\begin{document}
\title{\vspace{-3cm}The Non-Cancelling Intersections Conjecture}
\author{Antoine Amarilli \and Mikaël Monet \and
Dan Suciu}
\date{}
\maketitle

 We write~$[n]=\{1,\ldots,n\}$. Let~$\calF = \{S_1,\ldots,S_n\}$ be a
 finite family of pairwise incomparable finite sets. For~$T \subseteq
 [n], T\neq \emptyset$, define~$S_T = \bigcap_{j\in T} S_j$.
 By inclusion-exclusion we have 
 \begin{equation}
   \label{eq:ie}
   |\bigcup_{i=1}^n S_i| =  \sum_{\substack{T\subseteq [n]\\T\neq \emptyset}} (-1)^{|T|+1} |S_T|.
 \end{equation}

 It can happen that~$S_T = S_{T'}$ for~$T \neq T'$, so that some of the
 intersections disappear from the above sum. Call such an intersection \emph{cancelling},
 the other intersections being \emph{non-cancelling}.

 \begin{example*}
   Take $\calF = \{S_1,S_2,S_3\}$ with  $S_1=\{a,b,d\}$, $S_2=\{a,b,c,e\}$,
 $S_3=\{a,c,f\}$. Then:
 \begin{align*}
   |S_1 \cup S_2 \cup S_3| &= |S_1| + |S_2| + |S_3| \\
                           &\phantom{=} - (|S_1 \cap S_2| + |S_1 \cap S_3| + |S_2 \cap S_3|) \\
                           &\phantom{=} + |S_1 \cap S_2 \cap S_3|.
 \end{align*}
 Since we have~$S_1 \cap S_3 = S_1 \cap S_2 \cap S_3$, we obtain
 \begin{align*}
   |S_1 \cup S_2 \cup S_3| &= |\{a,b,d\}| + |\{a,b,c,e\}| + |\{a,c,f\}|- |\{a,b\}| - |\{a,c\}|.
 \end{align*}
 The non-cancelling intersections are the ones that remain, i.e., $\{a,b,d\}$, $\{a,b,c,e\}$, $\{a,c,f\}$, $\{a,b\}$, and $\{a,c\}$, while
 $\{a\}$ ($=S_1 \cap S_3 = S_1 \cap S_2 \cap S_3$) is a cancelling term.
 \end{example*}

 The conjecture is that we can always express the union~$\bigcup_{i=1}^n S_i$ (not its size, but the set itself)
 from the non-cancelling intersections, using only the operations of \emph{disjoint
 union} and \emph{subset complement}. Formally, for two sets $A,B$ such that
 $A\cap B = \emptyset$, define the \emph{disjoint union}~$A\cupdot B = A
 \cup B$. For two sets $A,B$ such that $B\subseteq A$, define the \emph{subset
 complement}~$A\minusdot B = A \setminus B$.

 \begin{conjecture*}[Non-cancelling intersections conjecture]
   For any finite family of pairwise incomparable finite sets $\calF =
   \{S_1,\ldots,S_n\}$, we can express $\bigcup_{i=1}^n S_i$ using only the
   non-cancelling intersections and the operations of disjoint union and subset
   complement.
 \end{conjecture*}

 \begin{example*}
   Continuing the example, we can express~$S_1 \cup S_2 \cup S_3 =
   \{a,b,c,d,e,f\}$ with $ \big[\big((\{a,b,d\} \minusdot \{a,b\}) \cupdot
   \{a,b,c,e\}\big) \minusdot \{a,c\}\big] \cupdot \{a,c,f\}$: the reader can
   easily check that each $\cupdot$ (resp., each $\minusdot$) is a valid
   disjoint union (resp., subset complement), and that we have only used the
   non-cancelling intersections. Note that this is not the only valid
   expression, for instance we can also obtain the union with the expression
   $[\{a,b,d\} \minusdot \{a,b\}] \cup [\{a,b,c,e\} \minusdot \{a,c\}] \cup
   \{a,c,f\}$.
 \end{example*}
\end{document}
