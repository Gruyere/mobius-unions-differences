\subsection{Facts known by bruteforce}
\label{subsec:bruteforce}

In this section, we discuss some preliminary results obtained experimentally by
implementing several ways of solving the problem by bruteforce. We have put the
necessary code publicly online
\url{https://gitlab.com/Gruyere/mobius-unions-differences}. The code is very
rough, and of course there could be errors affecting the
results presented here. The code is not documented, but don't hesitate to ask
if you have questions.


\paragraph{Values $k \leq 3$.}
For small values of~$k$, we can reach all configurations, even with the stronger
notion of cone-reachability:
\begin{propositionrep}
    For~$k \in \{1,2\}$, all configurations (in~$2^{[k]}$) are cone-reachable.
\end{propositionrep}
\begin{proof}
  By bruteforce:

  \noindent \verb?g++ -ocones1 -DN=1 -DONLY_CONES=1 -O2 bottom_up.cpp; ./cones1?\\
  \noindent \verb?g++ -ocones2 -DN=2 -DONLY_CONES=1 -O2 bottom_up.cpp; ./cones2?
\end{proof}

For $k=3$, we start to see that some configurations are not cone-reachable, but
all monotone configurations are still cone-reachable, and all configurations are
reachable:

\begin{propositionrep}
    For~$k=3$, all configurations (in~$2^{[3]}$) are reachable, and all
    configurations are cone-reachable except $\{\emptyset, \{0, 1, 2\}\}$ and
    its complement.
\end{propositionrep}
\begin{proof}
  By bruteforce:

  \noindent \verb?g++ -oall3 -DN=3 -O2 bottom_up.cpp; ./all3?\\
  \noindent \verb?g++ -ocones3 -DN=3 -DONLY_CONES=1 -O2 bottom_up.cpp; ./cones3?.
\end{proof}

\paragraph{Value $k=4$.}
For $k=4$, we start to see that some configurations are not reachable, but all
monotone configurations are still cone-reachable.

To present these results, we first introduce the notion of \emph{equivalence}.
We say that two configurations $\mathbf{s}$ and $\mathbf{s'}$ are
\emph{equivalent} if there exists a permutation $\sigma$ of~$[k]$ such that,
writing $\sigma(n) = \{\sigma(i) \mid i \in n\}$ for all $n \in 2^{[k]}$, then
$\{\sigma(n) \mid n \in \mathbf{s}\} = s'$. This is clearly an equivalence
relation. As our problem is invariant under permutations of~$[k]$, when
considering sets of configurations, it always suffices to study them up to
equivalence.

Second, we introduce the notion of \emph{irreducibility}. We say that a
configuration~$\mathbf{s}$ is \emph{reducible} if it can be expressed as a
cancellation-free sum~$\mathbf{s_1} \oplus \mathbf{s_2}$ or
difference~$\mathbf{s_1} \ominus \mathbf{s_2}$ for some
configurations~$\mathbf{s_1}, \mathbf{s_2}$ which are not the empty
configuration. A configuration that is not reducible is said to be
\emph{irreducible}. We define \emph{cone-irreducibility} in the expected way.
Observe that the empty configuration and all cones are
irreducible and in particular cone-irreducible. Moreover, all irreducible
configurations that are not the empty configuration or a cone are not reachable.
Intuitively, an irreducible configuration is a minimal kind of unreachable
configuration, but as we will see there are some configurations that are
reducible but not reachable.

We can now claim our result:

\begin{proposition}
\label{prp:k3}
For $k = 4$, out of the~$2^{2^4} = 65536$ configurations that exist, there are
  exactly~$24$ configurations that are not reachable, and none of them are
  monotone. There are~$12$ of these configurations that are irreducible, and they
  are all equivalent to the configuration~$\mathbf{s_3}$ depicted in
  Figure~\ref{fig:s3}. The other~$12$ are not irreducible and are all
  equivalent to the complement of~$\mathbf{s_3}$.

  Moreover, there are exactly~$2748$ configurations that are not cone-reachable ($248$ up
  to equivalence), none of which are monotone, and $910$ of which are
  irreducible ($90$ up to equivalence).
\end{proposition}
\begin{proof}
  Bruteforce:

  \noindent \verb?g++ -oall4 -DN=4 -O2 bottom_up.cpp; ./all4?\\
  \noindent \verb?g++ -ocones4 -DN=4 -DONLY_CONES=1 -O2 bottom_up.cpp; ./cones4?\\
  \noindent and variants with \verb?TEST_IRRED? and \verb?ONLY_MINIMAL?
\end{proof}

\begin{figure}
\begin{center}
\input{figures/s3.tex}
\label{fig:s3}
\caption{The configuration~$\mathbf{s_3}$ from
Proposition~\ref{prp:k3} and its associated Möbius
function~$\mu_\mathbf{s_3}$.}
\end{center}
\end{figure}

\paragraph{Value $k=5$ and up.}
For $k=5$, there are $2^{2^5}$ configurations, so around 4 billion, and we can
no longer afford to test all of them. However, testing specifically the monotone
configurations, we can show

\begin{proposition}
    For~$k\in \{1,\ldots,5\}$ all monotone configurations are cone-reachable.
\end{proposition}
\begin{proof}
  For~$k\in \{1,\ldots,5\}$ this follows from what precedes. Bruteforce for~$k=5$:

\verb?g++ -omonotone_cones5 -DN=5 -DALL=1 -DONLY_MONOTONE=1 -DEXPLAIN=0 \?\\
\verb?   -DEXPLAIN=0 -DONLY_CONES=1 -O2 top_down.cpp && ./monotone_cones5?
\end{proof}

We nevertheless managed to test irreducibility for all these configurations:

\begin{proposition}
  For~$k = 5$, there are exactly 38 irreducible configurations (up to
  equivalence).
\end{proposition}
\begin{proof}
  Bruteforce:

\verb? g++ -o all5reach -DN=5 -DALL=1 -DSTEP=1000000 -DONLY_MONOTONE=0 \?\\
\verb?   -DONLY_MINIMAL=1 -DEXPLAIN=0 -O2 top_down.cpp && ./all5reach?

  The bruteforce considered 37333248 minimal functions, as per sequence 1405 of
  the Sloane handbook.
\end{proof}



\begin{proposition}
    For~$k\in \{1,\ldots,6\}$ all monotone configurations are reachable.
\end{proposition}
\begin{proof}
  For~$k\in \{1,\ldots,6\}$ this follows by what precedes. Bruteforce for~$k=6$ (this concludes extremely fast, with very little backtracking):

\verb?g++ -omonotone6 -DN=5 -DALL=1 -DONLY_MONOTONE=1 -DEXPLAIN=0 \?\\
\verb?   -DEXPLAIN=0 -O2 top_down.cpp && ./monotone6?
\end{proof}

We do not know if all monotone configurations are cone-reachable for $k=6$, as
the execution of our program is too slow to conclude in that case.

For standard reachability, for $k=7$, there are several hundred million monotone
Boolean functions even up to symmetry, so it has not been possible so far to
test all of them, but all tested functions were reachable (and the computation
was instantaneous).

For larger values of~$k$, the program is sometimes too slow to conclude to the
reachability or even reducibility of some monotone functions, but we do not know
whether this because it is inefficient or whether it could be a counterexample
to the conjecture.



\subsection{Facts that hold in all generality}
\label{subsec:general}

In this section, we present some miscellaneous facts that we obtained.

\paragraph{Connection with the notion of Euler characteristic.}
The following can be seen as an alternative way of defining the Möbius function of a configuration:
\begin{proposition}
\label{prp:euler}
    Let~$\mathbf{s} \subseteq 2^{[k]}$ be a configuration, for some~$k>1$.
    Then, for all~$n\in 2^{[k]}$, we have that
    \[\mu_\mathbf{s}(n) = \sum_{\substack{n' \subseteq n\\n' \in \mathbf{s}}} (-1)^{|n\setminus n'|}.\]
\end{proposition}
\begin{proof}
    Direct application of Möbius inversion formula on the poset~$2^{[k]}$.
\end{proof}

It is related to the notion of \emph{Euler characteristic} of a Boolean
function. To establish the connection, observe that a configuration~$\mathbf{s}
\subseteq 2^{[k]}$ can be seen as a Boolean function over variables~$[k]$. The
\emph{Euler characteristic} of~$\mathbf{s}$ is~$\eul(\mathbf{s}) \defeq
\sum_{\substack{n \in 2^{[k]}\\n \in \mathbf{s}}}
(-1)^{|n|}$~\cite{roune2013complexity,stanley2011enumerative}.
Hence,~$\mu_\mathbf{s}(n)$ is the Euler characteristic of the
sub-configuration below~$n$, multiplied
by~$(-1)^{|n|}$. 

\paragraph{Constructing unreachable configurations from simpler unreachable configurations.}

\begin{definition}
\label{def:lower-subconfig}
    Let~$k>1$, $\mathbf{s} \subseteq 2^{[k]}$ be a configuration in~$2^{[k]}$
and~$\mathbf{s'}\in 2^{[k+1]}$ be a configuration in~$2^{[k+1]}$.  We say that
\emph{$\mathbf{s'}$ contains~$\mathbf{s}$ as a lower-subconfiguration} when
there exists~$j \in [k+1]$ and a bijection~$\sigma:[k] \to [k+1] \setminus \{j\}$
such that we have~$\mathbf{s'} \cap 2^{[k+1] \setminus \{j\}} =
\sigma(\mathbf{s})$.
\end{definition}
Here is a simple way to take an unreachable configuration for~$k$ and obtain
unreachable configurations for~$k+1$:

\begin{lemma}
\label{lem:lower-subconf}
    Let~$k>1$,~$\mathbf{s} \subseteq 2^{[k]}$ be an unreachable configuration
for~$k$, and~$\mathbf{s'} \subseteq 2^{[k+1]}$ be a configuration for~$k+1$.
Then, if~$\mathbf{s'}$ contains~$\mathbf{s}$ as a lower-subconfiguration
then~$\mathbf{s'}$ is unreachable as well (in~$2^{[k+1]}$).
\end{lemma}
\begin{proof}
Up to permutations, we can assume without loss of generality that~$j$ is~$k$ and that~$\sigma: [k] \to [k]$ is the identity in Definition~\ref{def:lower-subconfig}.
Assume by way of contradiction that~$\mathbf{s'}$ is reachable in~$2^{[k+1]}$,
and let~$T'$ be a parse tree of one of its decomposition; that is, a rooted
ordered binary tree, whose leaves are labeled by cones and whose internal nodes
are labeled by~$\oplus$ or~$\ominus$. For a node~$i$ of~$T$ let us
write~$\mathbf{s'}_i$ the corresponding configuration (so that~$\mathbf{s'}_r$
is~$\mathbf{s'}$ for~$r$ the root of~$T'$). For a configuration~$\mathbf{s'}
\subseteq 2^{[k+1]}$, let us write~$\widecheck{\mathbf{s'}} \subseteq 2^{[k]}$ the configuration of~$2^{[k]}$ defined by
$\widecheck{\mathbf{s'}} \defeq \mathbf{s'} \cap 2^{[k]}$.
Now, let~$T$ be the rooted ordered binary tree obtained from~$T'$ by replacing
every cone~$C_n$ by~$\widecheck{C_n}$, and considering that the operations are
done in~$2^{[k]}$. One can easily check that~$T$ is a valid
parse tree (meaning that all the operations are well-defined and
cancellation-free). Furthermore it is easy to check that,
letting~$\mathbf{s_i}$ be the configuration corresponding to node~$i$ of~$T$,
we have~$\mathbf{s_i} = \widecheck{\mathbf{s'_i}}$ for all nodes~$i$.
Hence~$\mathbf{s} = \widecheck{\mathbf{s'}} = \widecheck{\mathbf{s'_r}} = \mathbf{s_r}$ is reachable (in~$2^{[k]}$), a contradiction.
\end{proof}

\paragraph*{Degeneracy.}

\begin{definition}
    Let~$k >1$, $\mathbf{s} \subseteq 2^{[k]}$ be a configuration and~$j
\in[k]$. We say that~$\mathbf{s}$ \emph{does not depend on~$j$} if for
all~$n\in 2^{[k]}$ the following holds: we have~$n\in \mathbf{s}$ if and only
if~$n\cup\{j\} \in \mathbf{s}$. We call $\mathbf{s}$ \emph{degenerate} if there
exists~$j \in [k]$ such that~$\mathbf{s}$ does not depend on~$j$, and
\emph{nondegenerate} otherwise.
\end{definition}

\begin{lemma}
    Let~$\mathbf{s}\subseteq 2^{[k]}$ be a configuration that does not depend
on~$j\in [k]$. Then, for every~$n\in 2^{[k]}$ such that~$j \in n$, we have
that~$\mu_\mathbf{s}(n)=0$.
\end{lemma}
\begin{proof}
    Direct by bottom-up induction on~$\{n\in 2^{[k]} \mid j\in n\}$.
\end{proof}

The following lemma tells us intuitively that an unreachable configuration~$\mathbf{s'}$
of~$2^{[k+1]}$ either contains an unreachable
configuration of~$2^{[k]}$ as a lower-subconfiguration (in which case Lemma~\ref{lem:lower-subconf} “explains” why~$\mathbf{s'}$ is unreachable), or it must be nondegenerate:

\begin{lemma}
\label{lem:degenerate_zero}
    Let~$k > 1$, and let~$\mathbf{s'} \subseteq 2^{[k+1]}$ be a configuration
that is not reachable. Then one of the following it true (it could be both):
\begin{enumerate}
    \item there exists a configuration~$\mathbf{s} \subseteq 2^{[k]}$ such
that~$\mathbf{s} \notin \R_k$ and such that~$\mathbf{s'}$
contains~$\mathbf{s}$ as a lower-subconfiguration;
    \item $\mathbf{s}$ is nondegenerate (that is, $\mathbf{s'}$ depends on all~$j\in [k+1]$).
\end{enumerate}
\end{lemma}
\begin{proof}
    Assume by way of contradiction that both 1 and 2 are false.
Since~$\mathbf{s'}$ is degenerate, and up to permutation of~$[k+1]$, we can
assume without loss of generality that~$\mathbf{s'}$ does not depend on~$k$.
Let~$\mathbf{s} \defeq \mathbf{s'} \cap 2^{[k]}$. Clearly, $\mathbf{s'}$
contains~$\mathbf{s}$ as a lower-subconfiguration.  Because we assumed 2 to be
false,~$\mathbf{s}$ must then be reachable (in~$2^{[k]}$).  Let~$T$ be a parse tree
of a decomposition of~$\mathbf{s}$.  For a configuration~$\mathbf{s''}
\subseteq 2^{[k]}$, let us write~$\widehat{\mathbf{s''}}$ the configuration
of~$2^{[k+1]}$ defined by $\widehat{\mathbf{s''}} \defeq \mathbf{s''} \cup \{n
\cup \{k\} \mid n \in \widehat{\mathbf{s''}}\}$.  Now, let~$T'$ be the parse
tree obtained from~$T$ by replacing every cone~$C_n \subseteq 2^{[k]}$ at a
leaf of~$T$ by the cone~$\widehat{C_n} \subseteq 2^{[k+1]}$,
and considering that the operations are done in~$2^{[k+1]}$. For a node~$i$, we
write~$\mathbf{s_i}$ (resp., $\mathbf{s'_i}$) the configuration corresponding
to node~$i$ in $T$ (resp., in~$T'$). Then, one can show by bottom-up induction
on~$T$ that for all nodes~$i$ the following holds:
\begin{itemize}
    \item the operation at that node in~$T'$ is
well-defined and cancellation-free (this uses in particular
Lemma~\ref{lem:degenerate_zero});
    \item we have that~$\mathbf{s'_i} = \widehat{\mathbf{s_i}}$.
\end{itemize}
But then this implies that~$\mathbf{s'} =
\widehat{\mathbf{s}}$ is reachable, a contradiction.
\end{proof}
