\documentclass[a4paper]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usetikzlibrary{trees,shapes}
\usepackage[appendix=append]{apxproof}
\usepackage{relsize}
\usepackage{stackengine}
\stackMath
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{todonotes}
\usepackage{graphics,color,caption,subcaption}
\usepackage{hyperref}
\hypersetup{
	    colorlinks,
	    linkcolor={red!50!black},
	    citecolor={blue!50!black},
	    urlcolor={blue!30!black},
	    breaklinks=true
	    }
\usepackage{mathabx}
\usepackage{bbm}

\newtheoremrep{theorem}{Theorem}[section]
\newtheorem{openpb}[theorem]{Open problem}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{fact}[theorem]{Fact}
\newtheoremrep{proposition}[theorem]{Proposition}
\newtheoremrep{claim}[theorem]{Claim}
\newtheoremrep{corollary}[theorem]{Corollary}
\newtheoremrep{lemma}[theorem]{Lemma}
\newtheoremrep{example}[theorem]{Example}
\newtheoremrep{definition}[theorem]{Definition}


\input{macros}


\begin{document}
  \title{Sum-Expressions and Polynomials}
\author{}
\date{}
\maketitle


\section{Sum Expressions}


Fix $n \geq 0$.  A {\em sum-expression} is a function
$E:2^{[n]} \rightarrow \ZZ$.  Equivalently, it is a vector
$E \in \ZZ^{2^{[n]}}$.  We denote its support by
$\supp(E) = \setof{A\subseteq [n]}{E(A) \neq 0}$, and denote its
domain by $\dom(E) = \bigcup \supp(E)$.  We write $E + E'$ or $E - E'$
for the usual operations on vectors.

To each set $A \subseteq [n]$ we can associate the sum-expression that
has a single 1 on position $A$, i.e. $E_A(A) = 1$ and $E_A(B)=0$ if
$B\neq A$.  The set of $2^n$ sum-expressions
$(E_A)_{A \subseteq 2^{[n]}}$ forms a basis of the module
$\ZZ^{2^{[n]}}$.  To each set $A$, we can also associate the
sum-expression $E_{\uparrow(A)}$ given by the characteristic function
of the principal filter $\uparrow(A)$; in other words
$E_{\uparrow(A)}(B) = 1$ iff $B \supseteq A$.  The set of $2^n$
sum-expressions $(E_{\uparrow(A)})_{A \subseteq [n]}$ is also a basis
of $\ZZ^{2^{[n]}}$: this can be seen by observing that the following
mappings are inverse to each other:

\begin{align*}
  E \mapsto & E', & \mbox{where } E'(A) \defeq & \sum_{B: A \subseteq B} E(B)=E_{\uparrow(A)}\cdot E\\
  E' \mapsto & E, & \mbox{where } E(A) \defeq & \sum_{B: A \subseteq B} (-1)^{|B-A|}E'(B)
\end{align*}

In particular, if two sum-expressions $E_1, E_2$ satisfy
$E_{\uparrow(A)} \cdot E_1 = E_{\uparrow(A)} \cdot E_2$ for all
$A \subseteq [n]$, then $E_1 = E_2$.  When no confusion arises, we
will denote $E_A, E_{\uparrow(A)}$ by $A, \uparrow(A)$ respectively.


% For that, it suffices to prove that the mapping
% $\ZZ^{2^{[n]}}\rightarrow \ZZ^{2^{[n]}}$ given by
% $E \mapsto (\uparrow(A)\cdot E)_{A \subseteq [n]}$ is injective (where
% $\uparrow(A)$ denotes the sum-expression $E_{\uparrow(A)}$):
% 
% 
% \begin{fact} \label{fact:principal:filters:to:eq} Let $E, E'$ be two
%   sum-expressions such that, for every $A \subseteq [n]$,
%   $E \cdot \uparrow(A) = E'\cdot \uparrow(A)$.  Then $E = E'$.
% \end{fact}
% 
% \begin{proof}
%   We prove by downwards induction on $|A|$ that $E(A)=E'(A)$ for all
%   $A \subseteq [n]$.  We start with $A = [n]$:
%   %
%   \begin{align*}
%     E([n]) = & E\cdot \uparrow([n]) = E'\cdot \uparrow([n]) = E'([n])
%   \end{align*}
%   %
%   For the induction step, we observe that:
%   %
%   \begin{align*}
%     E\cdot \uparrow(A) = & \sum_{B: A\subseteq B \subseteq [n]}E(B)\\
%     E'\cdot \uparrow(A) = & \sum_{B: A\subseteq B \subseteq [n]}E'(B)\\
%   \end{align*}
%   %
%   Since $E\cdot \uparrow(A)=E'\cdot \uparrow(A)$ and $E(B)=E'(B)$ for
%   all $B \supsetneq A$, it follows that we have $E(A)=E'(A)$.
% \end{proof}

\begin{example}
  Assume $[n] = \set{a,b,c}$ (i.e. $a=1, b=2, c=3$).  We abbreviate a
  set $\set{a,b}$ with $ab$.  Sum-expressions are 8-dimensional
  vectors.  Some examples:
  %
  \begin{align*}
    a = & (0,1,0,0,0,0,0,0) & \supp = & \set{a} & \dom = & \set{a}\\
    ab = & (0,0,0,0,1,0,0,0,0) & \supp = & \set{ab} & \dom = & \set{a,b}\\
    abc = & (0,0,0,0,0,0,0,1) \\
    ab+bc-a = & (0,-1,0,0,1,0,1,0) & \supp= & \set{a,ab,bc} & \dom  = & \set{a,b,c} \\
    \uparrow(ab) = ab+abc = & (0,0,0,0,1,0,0,1) \\
    \uparrow(\emptyset) = & (1,1,1,1,1,1,1,1)
  \end{align*}
\end{example}

\begin{definition} \label{def:valid}
  A sum-expression $E$ is {\em valid} if:
  %
  \begin{align}
    \forall A \subseteq [n],\ \  E_{\uparrow(A)} \cdot E \in & \set{0,1}\label{eq:valid}
  \end{align}
  %
  Equivalently, its representation in the basis
  $(E_{\uparrow(A)})_{A\subseteq [n]}$ has all its coordinates in
  $\set{0,1}$.
\end{definition}

If $\calI \subseteq 2^{[n]}$ is an ideal, then we denote by
$\hat\calI$ the lattice $\hat \calI \defeq \calI \cup \set{\hat 1}$.

\begin{definition}
  Let $\calI \subseteq 2^{[n]}$ be an ideal.  The
  $\calI$-sum-expression is:
  %
  \begin{align*}
    E_\calI(A) = &
                   \begin{cases}
                     -\mu_{\hat \calI}(A,\hat 1) & \mbox{if $A \in \calI$}\\
                     0 & \mbox{otherwise}
                   \end{cases}
  \end{align*}
\end{definition}

\begin{lemma} \label{lemma:valid}
  $E_\calI$ is valid.
\end{lemma}

\begin{proof} If $A \not\in \calI$, then $E_{\uparrow(A)} \cdot E = 0$
  holds trivially.  Assume $A \in \calI$:
  %
  \begin{align*}
    E_{\uparrow(A)} \cdot E_\calI  = & \sum_{B: A \subseteq B} E_B   = -\sum_{B \in \calI: A \subseteq B} \mu_{\hat\calI}(B,\hat 1) \\
    = &  -\sum_{B \in \hat \calI: A \subseteq B} \mu_{\hat\calI}(B,\hat 1) + \mu_{\hat\calI}(\hat 1, \hat 1) = 0 + 1 = 1
  \end{align*}
\end{proof}

Our conjecture is:

\begin{conjecture}
  For every ideal $\calI$,
  $\dom(E_\calI) \in \bullet(\supp(E_\calI))$.
\end{conjecture}

\begin{example} \label{ex:good:and:not:good} (1) Consider the $Q9$
  intersection lattice, and let $\calI$ be its ideal, i.e.
  $\calI = \downarrow(abxw,bcyw,aczw,dw)$.  Then:
  %
  \begin{align*}
    E_\calI = & abxw + bcyw + aczw - aw - bw - cw + dw \\
    \supp(E) = & \set{abxw,bcyw,\ldots} \\
    \dom(E) = & \set{a,b,c,d,x,y,z,w}
  \end{align*}
  %
  By Lemma~\ref{lemma:valid}, $E_\calI$ is valid.  We check here a few
  conditions:
   %
   \begin{align*}
     E_\calI \cdot \uparrow(a) = & (abxw + aczw - aw) \cdot \uparrow(a) = 1+1-1=1\\
     E_\calI \cdot \uparrow(ac) = & (aczw) \cdot \uparrow(ac) = 1\\
     E_\calI \cdot \uparrow(xy) = & 0 \\
     E_\calI \cdot \uparrow(\emptyset) = & 1+1+1-1-1-1+1 = 1
   \end{align*}
   %
   We already know that $abcdxyzw \in \bullet(\supp(E_\calI))$.

   (2) The following is a valid sum-expression which is not derived
   from an ideal:
   %
   \begin{align*}
     E = & ab + c +def
   \end{align*}
   %
   Yet we still have $abcdef \in \bullet(\set{ab,c,def})$.

   (3) This expression is not valid:
   %
   \begin{align*}
     E = & abx+bcy+acz - abc
   \end{align*}
   %
   because $E \cdot \uparrow(abc) = -1$.  Also,
   $abcxyz \not\in \bullet(\set{abx,bcy,acz,abc})$.

   (4) Another example that comes from an intersection lattice:
   %
   \begin{align*}
     E = & aw + bw + cw - 2w
   \end{align*}
   %
   $E$ is valid, and illustrates a coefficient other than $\pm 1$.
\end{example}

\section{Polynomials}

Fix $n$ variables $\bm X=\set{X_1, \ldots, X_n}$.  A {\em monomial} is
$\bm X_A = \prod_{i \in A}X_i$; an {\em integer-valued, multi-linear
  polynomial}, or just {\em polynomial} is
$F = \sum_{A \subseteq [n]} c_A \bm X_A$, where $c_A \in \ZZ$ for all
$A$.  There is a 1-1 correspondence between polynomials $F$ and
sum-expressions $E$, where each polynomial $F$ corresponds to the
sum-expressions defined by its coefficients, and each sum-expression
defines the polynomial
%
\begin{align*}
  F \defeq & \sum E(A)\bm X_A
\end{align*}

\begin{definition}
  $F\in \ZZ[\bm X]$ is {\em valid} if its associated sum-expression is
  valid (Def.~\ref{def:valid}).  Equivalently, $F$ is valid if:
  %
  \begin{align*}
    \forall A \subseteq [n]: \ \ \frac{\partial^{|A|}F}{\partial \bm
    X_A}(1,\ldots,1) \in & \set{0,1}
  \end{align*}
  %
  We denote by $\VV[\bm X]$ the set of valid polynomials; this
  notation is with some abuse, because there is no set $\VV$.

  Fix an ideal $\calI$.  The $\calI$-polynomial $F_\calI$ is the
  following:
  %
  \begin{align*}
    F_\calI \defeq & - \sum_{A \subseteq \calI}\mu_{\hat \calI}(A,\hat 1)\bm X_A
  \end{align*}
  %
  Then $F_\calI \in \VV[\bm X]$ and its associated sum-expression is
  $E_\calI$.
\end{definition}

Fix a fresh set of variables $\bm x = \set{x_1, \ldots, x_n}$.  We
consider the following mappings from $\ZZ[\bm X]$ to $\ZZ[\bm x]$ and
back:
%
\begin{align*}
  \forall F \in \ZZ[\bm X], \tolower(F) \defeq f, \mbox{ where: } f(x_1,\ldots,x_n) \defeq & F(x_1+1, \ldots, x_n+1)\\
  \forall f \in \ZZ[\bm x], \toupper(f) \defeq F, \mbox{ where: } F(X_1,\ldots,X_n) \defeq & f(X_1-1, \ldots, X_n-1)
\end{align*}

We call $F \in \ZZ[\bm X]$ and {\em upper} polynomial, and
$f \in \ZZ[\bm x]$ a {\em lower} polynomial.  By convention, if
$F, F_1, G$ denote upper polynomials, then $f, f_1, g$ denote
$\tolower(F), \tolower(F_1), \tolower(G)$.

\begin{lemma}
  (1) $F \in \VV[\bm X]$ iff $f\defeq\tolower(F) \in \BB[\bm x]$,
  where $\BB = \set{0,1}$.

  (2) For every ideal $\calI$,
  $f_\calI \defeq \tolower(F_\calI) = \sum_{A \in \calI} \bm x_A$.
\end{lemma}

The proof is immediate.

For every $S \subseteq 2^{[n]}$, denote by
$f_S \defeq \sum_{A \in S} \bm x_A$, and $F_S \defeq \toupper(f_S)$.

\begin{definition} \label{def:valid:polynomials} (1) $F \pdot G$ is
  defined to be $F+G$ assuming the following conditions hold: $F, G,
  F+G \in \VV[\bm X]$, and  $F+G$ is {\em cancellation-free},   formally:
  \begin{align}
    \forall A \subseteq [n]: \ \ \frac{\partial^{|A|}F}{\partial \bm  X_A}(0,\ldots,0)\cdot \frac{\partial^{|A|}G}{\partial \bm X_A}(0,\ldots,0) \geq & 0 \label{eq:cancellation:free}
  \end{align}
  If any of these conditions is violated, then $F\pdot G$ is
  undefined.

  (2) $F \mdot G$ is defined to be $F-G$ assuming similar
  conditions hold, where~\eqref{eq:cancellation:free} is replaced by
  $\leq 0$.

  (3) The set $\bullet \VV[\bm X]$ is defined as follows: (a) every
  monomial $\bm X_A$ is in $\bullet \VV[\bm X]$, (b) if
  $F, G \in \bullet\VV[\bm X]$ then
  $F \pdot G \in \bullet\VV[\bm X]$, and (c)
  $F, G \in \bullet\VV[\bm X]$ then
  $F \mdot G \in \bullet\VV[\bm X]$.
\end{definition}

\begin{definition}
  We carry over Definition~\ref{def:valid:polynomials} from
  $\VV[\bm X]$ to $\BB[\bm x]$, More precisely: $f \pdot g$,
  $f \mdot g$ are defined (and equal to $f+g$, $f-g$ respectively) iff
  $F \pdot G$, $F\mdot G$ are defined, and
  $\bullet \BB[\bm x] \defeq \tolower(\bullet \VV[\bm X])$.
\end{definition}

Notice that the base case $\bm X_A \in \bullet \VV[\bm X]$ becomes:
$\prod_{i \in A}(x_i+1) \in \BB[\bm x]$.

\begin{conjecture}
  For every ideal $\calI$, $F_\calI \in \bullet \VV[\bm X]$;
  equivalently, $f_\calI \in \bullet \BB[\bm x]$.
\end{conjecture}

\begin{example}
  Consider four variables $X,Y,Z,U$.  We illustrate with two ideals:

  Let $\calI = \downarrow\set{X,Y,Z}$.  Then:
  \begin{align*}
    F_\calI = & X+Y+Z-2 &  f_\calI = & x + y + z + 1\\
    F_\calI = & ((X\mdot 1)\pdot(Y\mdot 1))\pdot Z
    & f_\calI = & ((x+1)\mdot 1) \pdot ((y+1)\mdot 1)\pdot (z+1)
  \end{align*}

  Let $\calI = \downarrow\set{XY,XZ,YZ,U}$ ($\hat \calI$ is the Q9
  lattice).  Then:
  %
  \begin{align*}
    F_\calI = & XY+YZ+XZ+U-X-Y-Z & f_\calI = & xy+xz+yz+x+y+z+1 \\
    F_\calI = & ((XY\mdot X) \pdot (YZ\mdot Y) \pdot (XZ\mdot Z) \pdot U & f_\calI = & \cdots \mbox{ (replace $X$ with $x+1$ etc)}
  \end{align*}
\end{example}

\section{Simple Properties}

\begin{lemma}
  Let $\bm X, \bm Y$ be two disjoint sets of variables.  If
  $F \in \VV[\bm X]$ and $G \in \VV[\bm Y]$ then
  $FG \in \VV[\bm X \cup \bm Y]$.
\end{lemma}

\begin{proof}
  It suffices to observe that $f\defeq \tolower(F)$ is in
  $\BB[\bm x]$, $g\defeq \tolower(G)$ is in $\BB[\bm y]$, and
  therefore their product $fg$ is in $\BB[\bm x \cup \bm y]$.
\end{proof}

\begin{lemma}[Product Lemma] \label{lemma:product} If
  $F \in \bullet \VV[\bm X]$ and $G \in \bullet \VV[\bm Y]$, then
  $FG \in \bullet \VV[\bm X \cup \bm Y]$, where $\bm X, \bm Y$ are
  disjoint sets of varaibles.
\end{lemma}

\begin{proof}
  (Sketch).  We prove the claim by induction on the structure of the
  $\bullet$-expressions for $F$ and $G$ respectively.  The base case
  is when both $F$ and $G$ are monomials, i.e. $F=\bm X_A$,
  $G = \bm Y_B$.  Then $FG = \bm X_A\bm Y_B$ is also a monomial, which
  is in $\bullet \VV[\bm X \cup \bm Y]$ by definition.  Assume
  $F = F_1 \pdot F_2$, where $F_1, F_2 \in \VV[\bm X]$.  By induction
  hypothesis, both $F_1G$ and $F_2G$ are in $\VV[\bm X \cup \bm Y]$
  and $F_1G \pdot F_2G$ is cancellation-free because $F_1 \pdot F_2$
  was cancellation-free, therefore
  $FG \in \bullet \VV[\bm X \cup \bm Y]$.  The other cases are similar
  and omitted.
\end{proof}

\begin{lemma} \label{lemma:simple:3} The following hold:
  \begin{itemize}
  \item If $A \subseteq [n]$ then
    $f_{\downarrow(A)} \in \bullet \BB[\bm x]$.
  \item If $A \subseteq B \subseteq [n]$ then
    $f_{\downarrow(B)-\downarrow(A)} \in \bullet \BB[\bm x]$.
  \item If $A, B \subseteq [n]$ then
    $f_{\downarrow(A \cup B)} \in \bullet \BB[\bm x]$.
  \end{itemize}
\end{lemma}

\begin{proof}
  $f_{\downarrow(A)} = \sum_{C \subseteq A} \bm x_C = \prod_{i \in A}
  (1+x_i)$.  Its upper polynomial is $F_{\downarrow(A))}=\bm X_A$,
  which is in $\bullet\VV[\bm X]$ by definition.

  $f_{\downarrow(B) - \downarrow(A)} = \prod_{i \in B}(1+x_i) -
  \prod_{i \in A}(1+x_i)$, and its upper polynomial is
  $\bm X_B - \bm X_A$, which is in $\bullet\VV[\bm X]$ because the
  difference is cancellation-free.

  $f_{\downarrow(A \cup B)} = f_{\downarrow(A)} + f_{\downarrow(B)} -
  f_{\downarrow(A \cap B)}$ and its upper polynomial is
  $F_{\downarrow(A \cup B)} = \bm X_A + \bm X_B - \bm X_{A \cap B}$.
  We write it as $(\bm X_A \mdot \bm X_{A \cap B}) \pdot \bm X_B$ and
  observe that both $\mdot$ and $\pdot$ are defined, proving that
  $F_{\downarrow(A \cup B)} \in \bullet \VV[\bm X]$.
\end{proof}


\begin{lemma} \label{lemma:uniform}
  For $k \leq n$ denote by
  $\calI_k \defeq \setof{A \subseteq [n]}{|A|\leq k}$ (the ideal of
  sets of size $\leq k$). Then $f_{\calI_k} \in \bullet\BB[\bm x]$.
\end{lemma}

Before we prove the lemma, we establish two simple facts.

\begin{fact} \label{fact:binomial} If $1 \leq k < n$ then:
  \begin{align}
    \sum_{\ell = 0,k}(-1)^\ell {n \choose \ell} = & (-1)^k{n-1 \choose k}\label{eq:choose:k:ell:n}
  \end{align}
\end{fact}

\begin{proof}
  For $\ell\geq 1$, $\ell < n$ we can write
  ${n \choose \ell} = {n-1\choose \ell-1} + {n-1\choose \ell}$ and
  obtain:
  %
  \begin{align*}
    \sum_{\ell = 0,k}&(-1)^\ell {n \choose \ell} = 1 + \sum_{\ell=1,k}(-1)^\ell {n \choose \ell} 
    = 1 + \sum_{\ell=1,k}(-1)^\ell\left[{n-1\choose \ell-1} + {n-1  \choose \ell}\right] \\
    = & 1 - \left[{n-1\choose 0} + {n-1\choose 1}\right] + \left[{n-1\choose    1}+{n-1\choose 2}\right] - \cdots (-1)^k\left[{n-1\choose k-1} + {n-1  \choose k}\right]\\
    = &  (-1)^k{n-1 \choose k}
  \end{align*}
\end{proof}

Denote by $f_{n,k} \defeq f_{\calI_k}$.  The second fact is:

\begin{fact} \label{fact:partial:a:n:k:m} If $k<n$ then, for any
  $A \subseteq [n]$, denoting $m \defeq |A|$, the following holds:
  %
  \begin{align*}
  \frac{\partial^{|A|}f_{n,k}}{\partial \bm x_A}(-1,\ldots,-1) = & (-1)^{k-m}{n-1\choose k-m}
  \end{align*}
  %
\end{fact}

\begin{proof}
  By direct calculation we obtain:
  %
  \begin{align*}
    \frac{\partial^{|A|}f_{n,k}}{\partial \bm x_A}(-1,\ldots,-1) =
    & \sum_{C: A \subseteq C; |C|\leq k} \frac{\partial^{|A|} \bm x_C}{\partial \bm x_A}(-1,\ldots,-1)=\sum_{C: A \subseteq C; |C|\leq k} (-1)^{|C-A|}\\
    = & \sum_{\ell=0,k-m}(-1)^\ell{n \choose \ell} = (-1)^{k-m}{n-1\choose k-m}
  \end{align*}
\end{proof}

We can now prove Lemma~\ref{lemma:uniform}:

\begin{proof} (of Lemma~\ref{lemma:uniform}) If $k=0$ then
  $f_{n,k} = 1$ and is trivially in $\bullet \BB[\bm x]$, so assume
  $k > 0$.  Similarly, if $k=n$ then
  $f_{n,k} = \prod_{i \in [n]}(1+x_i)$ and it belongs to
  $\bullet \BB[\bm x]$ by definition, so assume $k < n$.  We prove the
  claim by induction on $n$.  If $n=k$ then
  $f_{n,n} = f_{2^{[n]}} = \prod_{i \in [n]}(1+x_i)$, which is in
  $\bullet \BB[\bm x]$ by definition.  If $n>k$, then consider any
  variable $x_n$, and observe that:
  %
  \begin{align}
    f_{n,k} = x_nf_{n-1,k-1} + f_{n-1,k} \label{eq:f:n:k:minus:1}
  \end{align}
  %
  By induction hypothesis both polynomials $f_{n-1,k-1}$ and
  $f_{n-1,k}$ are in $\bullet \BB[\bm x]$, and by the Product
  Lemma~\ref{lemma:product}, $x_n f_{n-1,k-1} \in \bullet \BB[\bm x]$.
  Thus, it suffices to prove that the sum~\eqref{eq:f:n:k:minus:1} is
  cancellation-free, by checking the following condition, for every
  set $A \subseteq [n]$:
  %
  \begin{align*}
    \frac{\partial^{|A|} x_nf_{n-1,k-1}}{\partial \bm x_A}(-1,\ldots,-1) \cdot
    \frac{\partial^{|A|} f_{n-1,k}}{\partial \bm x_A}(-1,\ldots,-1)\geq & 0
  \end{align*}
  %
  We can assume w.l.o.g. that $n \not\in A$, otherwise then the second
  factor above is $=0$, and the condition holds trivially.  Since we
  set $x_n := -1$, then claim above becomes:
  %
  \begin{align*}
    \frac{\partial^{|A|} f_{n-1,k-1}}{\partial \bm x_A}(-1,\ldots,-1) \cdot
    \frac{\partial^{|A|} f_{n-1,k}}{\partial \bm x_A}(-1,\ldots,-1)\leq & 0
  \end{align*}
  %
  We can assume w.l.o.g.  $|A|\leq k-1$, otherwise the first factor is
  $=0$.  The claim follows from Lemma~\ref{fact:partial:a:n:k:m},
  because the two factors have opposite signs, since one has the sign
  $(-1)^{k-1-|A|}$ and the other has sign $(-1)^{k-|A|}$.
\end{proof}


\begin{lemma}
  For every set $S \subseteq 2^{[n]}$,  $f_S \in \bullet \BB[\bm x]$
  iff $f_{2^n-S} \in \bullet \BB[\bm x]$.
\end{lemma}

Notice that $f_{2^{[n]}-S} = \prod_{i\in[n]}(1+x_i)-f_S$.

\begin{proof}
  It suffices to prove one implication.  Assume
  $f_S \in \bullet \BB[\bm x]$.  If the degree of $f_S$ is $<n$ (in
  other words, if $f_S$ does not contain the monomial $\bm x_{[n]}$)
  then $F_S = \toupper(f_S)$ has degree $<n$, i.e. it doesn't contain
  the monomial $\bm X_{[n]}$, which implies that the difference
  $\bm X_{[n]}\mdot F_S$ is cancellation-free, and therefore
  $\tolower(\bm X_{[n]})\mdot f_S = \prod_{i\in[n]}(1+x_i) \mdot f_S$
  is a correct expression, proving that it is in $\bullet\BB[\bm x]$.
  However, this argument fails when the degree of $f_S$ is $n$.

  Instead, we use an inductive proof based on the dot-algebra
  expression for $f_S$.

  The base case is when $f_S = \prod_{i \in A}(1+x_i)$ for some set $A
  \subseteq [n]$.  In other words, $S = \downarrow(A)$.   Then
  $f_{2^{[n]}-S} = f_{\downarrow([n])-\downarrow(A)}$ and the claim
  follows from Lemma~\ref{lemma:simple:3}.

  Assume $f_S = f_{S_1} \pdot f_{S_2}$, where $S_1, S_2$ are disjoint
  sets and $S_1 \cup S_2 = S$.  Assume w.l.o.g. that $n\not\in S_2$:
  otherwise switch the roles of $S_1$ and $S_2$ (since at most one of
  the can contain $n$).  Then:
  %
  \begin{align}
    2^{[n]}- (S_1 \cup S_2) = & (2^{[n]}-S_1) - S_2 \nonumber\\
    f_{2^{[n]}-(S_1\cup S_2)} & = f_{2^{[n]}-S_1}-f_{S_2} \label{eq:s1:s2:complement}
  \end{align}
  %
  We apply induction hypothesis to $f_{2^{[n]}-S_1}$ (which is equal
  to $\prod_{i \in [n]}(1+x_i)-f_{S_1}$) to prove that it is in
  $\bullet \BB[\bm x]$, and we also have
  $f_{S_2} \in \bullet \BB[\bm x]$ by assumption.  It remains to check
  that the difference~\ref{eq:s1:s2:complement} is cancellation-free.
  We claim that the following holds for all $A \subseteq [n]$:
  %
  \begin{align*}
    \frac{\partial^{|A|}\left(\prod_{i \in [n]}(1+x_i)-f_{S_1}\right)}{\partial \bm x_A}(-1,\ldots,-1)\cdot \frac{\partial^{|A|}f_{S_2}}{\partial\bm x_A}(-1,\ldots,-1) \leq & 0
  \end{align*}
  %
  To prove the claim, observe that, if $A \neq [n]$, then
  $\partial^{|A|}\prod_{i \in [n]}(1+x_i)/\partial\bm
  x_A(-1,\ldots,-1)=0$, and the claim follows from the fact that
  $f_{S_1}\pdot f_{S_2}$ was cancellation-free.  If $A = [n]$, then
  the second factor above is $=0$, because the degree of $f_{S_2}$ is
  $\leq n$.

  It remains to consider the case $f_S = f_{S_1} \mdot f_{S_2}$, where
  $S_2 \subseteq S_1$.  Here we write:
  %
  \begin{align}
    2^{[n]}-(S_1-S_2) = & (2^{[n]}-S_1) \cup S_2 \nonumber\\
    f_{2^{[n]}-(S_1-S_2)} = & f_{2^{[n]}-S_1} + f_{S_2}\label{eq:s1:s2:difference:complement}
  \end{align}
  %
  As before, we use induction hypothesis to claim that
  $f_{2^{[n]}-S_1}\in \bullet\BB[\bm x]$, and prove that the plus
  in~\eqref{eq:s1:s2:difference:complement} is cancellation free.  The
  argument is the same as above, with one difference: when $A=[n]$
  then either one of the derivatives is 0, or, if both polynomials
  in~\eqref{eq:s1:s2:difference:complement} have degree $n$, then both
  derivatives are 1, hence they have the same sign, proving that the
  sum is cancellation-free.
\end{proof}

\section{Two Types of Induction}

Write the polynomials as:

\begin{align}
  F = & \sum_{A \subseteq [n]} C_A \bm X_A \label{eq:fupper:coeff} \\
  f = & \sum_{A \subseteq [n]} c_A \bm x_A \label{eq:flower:coeff}
\end{align}
%
with $c_A \in \set{0,1}$ and:
%
\begin{align}
  C_A = & \sum_{B \subseteq A} c_A & c_A = & \sum_{B \supseteq A} (-1)^{|B-A|} c_B \label{eq:upper:lower:coefficients}
\end{align}

\subsection{Induction on the  Upper Variables}

Fix $i \in [n]$ and $X_i$ ($x_i$ respectively) the corresponding
variable.  Group the monomials in $F$ by $X_i$, and write it as:
%
\begin{align*}
  F = & X_i G + H & &\mbox{equivalently:} & f = & (x_i+1) g + (h-g)
\end{align*}
%
If we can prove inductively that $G, H \in \bullet \ZZ[\bm X]$, then
so is $F$, because the summation $X_i G + H$ is cancellation-free.
Thus, it suffices to prove by induction that $g$ and $h-g$ are both in
$\bullet \BB[\bm x]$.  Notice how we wrote $f$: first we group by
$x_i$, $f=x_ig + h$, then we force a factor $x_i+1$, since
$\tolower(X_i) = x_i+1$.  We have:
%
\begin{align*}
  \tolower(G) = & g & \tolower(H) = & h-g
\end{align*}
%
Since all coefficients of $f$ are in $\set{0,1}$, so are those of $g$,
i.e. $g \in \BB[\bm x]$.  We need to check that
$f^{(i)} \defeq h-g \in \BB[\bm x]$.  Its coefficients, $c_A^{(i)}$,
are:
%
\begin{align*}
  f^{(i)} \defeq & h - g &  c_A^{(i)} = & c_A - c_{A \cup \set{i}}
\end{align*}
%
Assuming $f$ was defined by an ideal (in this case we call $f$ {\em
  monotone}), then $0 \leq c_{A \cup \set{i}} \leq c_A \leq 1$,
therefore $c_A^{(i)} \in \set{0,1}$ and $f^{(i)} \in \BB[\bm x]$.
However, $f^{(i)}$ is no longer monotone, so we cannot repeat this
argument at the next step.

Let's see what happens if we continue with the induction removing
variables $x_j, x_k, \ldots$  Then we obtain a sequence of polynomials
$f^{(\emptyset)}\defeq f, f^{(i)}, f^{(i,j)}, f^{(i,j,k)}, \ldots$,
whose coefficients are:
%
\begin{align*}
  c_A^{(i)} = & c_A - c_{A \cup \set{i}} \\
  c_A^{(i,j)} = & c_A - c_{A \cup \set{i}} - c_{A \cup \set{j}} + c_{A\cup\set{i,j}} \\
              & \ldots\\
A\cap B=\emptyset:\ \   c_A^{(B)} = & \sum_{S \subseteq B}(-1)^{|S|}c_{A\cup S}
\end{align*}
%

This fails for Q9, because if we eliminate the variables $d,a$ then
the coefficient of $\emptyset$ is:
%
\begin{align*}
  c_\emptyset^{a,d} = & c_\emptyset - c_a - c_d + c_{ad} = 1 - 1 -1 +  0 = -1
\end{align*}
%
But maybe we can use a difference instead of a sum? \dan{to finish
  here}.


Induction on the number of upper variables fails on $\calI_k$ (see
Lemma~\ref{lemma:uniform}).  To see this, let $A$ be any set of size
$|A| = k-1$, and consider eliminating any two variables
$i,j\not\in A$.  Then the residual coefficient of $\bm x_A$ is:
%
\begin{align*}
  c_A^{(i,j)} = & c_A - c_{A \cup \set{i}} - c_{A \cup \set{j}} + c_{A  \cup\set{i,j}}=1-1-1+0 = -1
\end{align*}
%



% Combining this with~\eqref{eq:upper:lower:coefficients} we obtain
% \dan{I didn't check this}:
% %
% \begin{align*}
%   A\cap B=\emptyset:\ \   c_A^{(B)} = & \sum_{S: A \subseteq S \subseteq [n]-B}C_S
% \end{align*}
% %
% Lets denote by $\mu([A,[n]-B])\defeq -\sum_{S: A \subseteq S \subseteq
%   [n]-B}\mu(S,\hat 1)$.  We obtain:
% %
% \begin{lemma}
%   Suppose that the ideal $\calI$ such that, for any disjoint sets
%   $A, B \subseteq [n]$ with $B\neq\emptyset$,
%   $\mu([A,[n]-B]) \in \set{0,1}$.  Then
%   $f_\calI \in \bullet \BB[\bm x]$.
% \end{lemma}
% 
% In particular, $Q_9$ (where $f = ab+ac+bc+a+b+c+d+1$,
% $F = AB+AC+BD-A-B-C+D$) has this property.  A few checks:
% %
% \begin{align*}
%   \mu([\emptyset,ABC] = & 1+1+1-1-1-1 = 0\\
%   \mu([A,ABC]) = & 1+1 -1 = 1\\
%   \mu([A,AB]) = & 1 - 1 = 0\\
%   \mu([\emptyset,ABD]) = & 1-1-1+1 = 0 \\
%   \mu([\emptyset,D]) = & 1 \\
%   \ldots
% \end{align*}

Variable elimination also fails on the counterexample in
Fig.~\ref{fig:e4} \dan{to finish here}


\begin{figure}
  \centering
  \includegraphics[width=1.0\linewidth]{FIGS/e4}
  \caption{The counterexample from Fig.4 in~\cite{note}.}
  \label{fig:e4}
\end{figure}

\subsection{Induction on the Lower Variables}

Recall that in Lemma~\ref{lemma:uniform} we applied induction on the
lower variables:
%
\begin{align*}
  f = & x_i g + h & F = & (X_i-1) G + H
\end{align*}
%
If $f$ was monotone, then so are $g, h$, and we can apply induction
hypthesis to prove that $g, h$ are in $\bullet \BB[\bm x]$; it follows
immediately that $x_ig \in \bullet \BB[\bm x]$ (by the product lemma
and the fact that $x_i = (x_i+1) - 1 \in \bullet \BB[\bm x]$).  The
question is now whether the sum is cancellation-free.  This worked,
for example, in Lemma~\ref{lemma:uniform}.

\bibliographystyle{apalike}
\bibliography{main}



\end{document}
