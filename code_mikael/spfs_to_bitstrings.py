#!/usr/bin/env python2

import os, sys
import itertools


N=4

def spf_to_string(spf):
  res = ''
  for j in range(1<<N):
    if spf & (1<<j):
      res+='1'
    else:
      res+='0'
  return res
    

if __name__ == '__main__':
  file = open(sys.argv[1],'r');
  f = open(sys.argv[1]+"_as_bitstrings", 'w')
  for l in file:
    clauses = [ int(bla) for bla in l.strip().split()]
    spf_as_int=0
    for j in range(1<<N):
      for cl in clauses:
        if (j & cl) == cl:
          spf_as_int |= (1<<j)
          break
    f.write(spf_to_string(spf_as_int)+'\n')
  f.close()
