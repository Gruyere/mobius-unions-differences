#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>
#include <map>
#include <deque>

#define N 4

namespace std { // https://stackoverflow.com/a/27216842/9374294
  template<> struct hash<std::vector<int>> {
    std::size_t operator()(std::vector<int> const& vec) const {
      size_t seed = vec.size();
      for(auto& i : vec) {
        seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
      }
      return seed;
    }
  };
}

using namespace std;
typedef vector<int> mob;

deque<mob> mobs_treated;
deque<mob> mobs_to_be_treated;
map<mob,uint> mobs_to_states;
unordered_set<uint> seen_states;


string mob_to_string(mob &m){
  string res;
  for (int i: m)
  res += to_string(i);
  return res;
}


void do_plus(const mob &mymob, const mob &truc){
  if (mobs_to_states[mymob] & mobs_to_states[truc]) return;
  uint new_state = mobs_to_states[mymob] | mobs_to_states[truc];
  if (seen_states.find(new_state) != seen_states.end()) return;
  mob new_mob(1<<N);
  for (int i=0; i < (1<<N); ++i){
    if ((mymob[i] > 0 && truc[i] < 0) || (mymob[i] < 0 && truc[i] > 0)) return;
    new_mob[i] = mymob[i] + truc[i];
  }
  mobs_to_be_treated.push_back(new_mob);
  seen_states.insert(new_state);
  mobs_to_states[new_mob] = new_state;
}


void do_minus(const mob &mymob, const mob &truc){
  if ((mobs_to_states[mymob] & mobs_to_states[truc]) != mobs_to_states[truc]) return;
  uint new_state = mobs_to_states[mymob] & ~mobs_to_states[truc];
  if (seen_states.find(new_state) != seen_states.end()) return;
  mob new_mob(1<<N);
  for (int i=0; i < (1<<N); ++i){
    if ((mymob[i] > 0 && truc[i] > 0) || (mymob[i] < 0 && truc[i] < 0)) return;
    new_mob[i] = mymob[i] - truc[i];
  }
  mobs_to_be_treated.push_back(new_mob);
  seen_states.insert(new_state);
  mobs_to_states[new_mob] = new_state;
}


int main(){
  cout << "Testing the conjecture for the powerset with " << N << " elements.\n";

  //Initialize seen_states, mobs_to_states and mobs_to_be_treated with the base elements (the cones)
  for (int i=0; i < (1<<N); ++i){
    mob cone_as_mob(1<<N,0);
    cone_as_mob[i]=1;
    uint cone_as_int=0;
    for (int j=i; j < (1<<N); ++j)
      if ( (j|i) == j ) cone_as_int |= (1<<j);
    seen_states.insert(cone_as_int);
    mobs_to_states[cone_as_mob] = cone_as_int;
    mobs_to_be_treated.push_back(cone_as_mob);
  }

  //Do the search
  while (!mobs_to_be_treated.empty()){
    mob mymob = mobs_to_be_treated.front(); mobs_to_be_treated.pop_front();
    for (const mob &truc: mobs_treated){
      do_plus(mymob, truc);
      do_minus(mymob, truc);
      do_minus(truc, mymob);
    }
    mobs_treated.push_back(mymob);
    cout << "seen_states: " << seen_states.size() << "\tmobs_treated: " << mobs_treated.size() << "\tmobs_to_be_treated: " << mobs_to_be_treated.size() << endl;
  }

  if (mobs_treated.size() != (1<<(1<<N)) - 1){
    cout << "Conjecture is false. The missing things are (mobius vector -- 0/1 corresponding state):";
  }
  else cout << "No problem\n";
}
