#!/usr/bin/env python2

import os, sys
import itertools



'''
  given a sorted tuple of elements, concatenate them all to give a name to the node.
'''
def name(tup):
  if len(tup)==0: return 'emptyset'
  return ''.join((str(el) for el in tup))


'''
  given a sorted tuple of elements, concatenate them all to give a label to the node.
'''
def label(tup):
  if len(tup)==0: return '$\\emptyset$'
  return '$' + ''.join((str(el) for el in tup)) + '$' # shortlabel
#  return '$\\{' + ''.join((str(el) + ', ' for el in tup))[:-2] + '\\}$' # longlabel


'''
  given a sorted tuple and position and Boolean color, generates tikz code that draws the node. If color is True then apply a color.
'''
def draw_node(tup, x_pos, y_pos, color):
  if color: return '\\node[nodestyle,fill=orange] (' + name(tup) + ') at (' + x_pos + ', ' + y_pos + ') {' + label(tup) + '};\n'
  return '\\node[nodestyle] (' + name(tup) + ') at (' + x_pos + ', ' + y_pos + ') {' + label(tup) + '};\n'

N=4

if __name__ == '__main__':
  file = open(sys.argv[1],'r');
  count = int(sys.argv[2])
  os.system('rm figures/*')
  for l in file:
    count -=1
    if count == -1:
      exit()
    print("Current line is: " + l[:-1]) 
    l_int = int(l[:-1],2)
    colored_subsets_l=[]
    for i in range(len(l[:-1])):
      if (l_int & (1<<i)):
        print i
        tup_l=()
        for j in range(N):
          if (i & (1<<j)):
            tup_l += (j,)
        colored_subsets_l.append(tup_l) 
    print colored_subsets_l

    filename = sys.argv[1]
    k=N-1
    H_SPACING = 1.9
    V_SPACING = 1.3
    elements = range(k+1)
  
    tikzcode = '\\documentclass{standalone}\n\\usepackage{tikz}\n\\begin{document}\n\\definecolor{dartmouthgreen}{rgb}{0.05, 0.5, 0.06}\n\n\\begin{tikzpicture}\n\\tikzset{nodestyle/.style={draw,rectangle}}\n'
  # ===================== DRAW THE NODES ========================
    tikzcode += ' ===== NODES ====\n\n'
    # We will consider that the bottom node (emptyset) is at position (0,0)
  
    for i in range(k+2):
      subsets_size_i = list(itertools.combinations(elements,i)) # lexicographically sorted list of sorted tuples of i elements
      y_pos = str(float(i * V_SPACING))
      h_length = H_SPACING * (len(subsets_size_i)-1) # the total length that the i-th layer will occupy
      for j in range(len(subsets_size_i)):
        x_pos = str(float(-h_length/2 + j * H_SPACING))
        tikzcode += draw_node(subsets_size_i[j], x_pos, y_pos, subsets_size_i[j] in colored_subsets_l)
  
  # ===================== DRAW THE EDGES ========================
    tikzcode += '\n\n ===== EDGES ====\n\n'
  
    for i in range(k+1):
      subsets_size_i = list(itertools.combinations(elements,i))
      for subset_size_i in subsets_size_i:
        for r in [el for el in range(k+1) if el not in subset_size_i]:
          neigh = tuple(sorted(subset_size_i + (r,))) # because sorted(tuple) returns a list (it does not really matter for the function name and label but eh..)
          if ((subset_size_i in colored_subsets_l) and (neigh in colored_subsets_l)):
            tikzcode+= '\\draw[dartmouthgreen,very thick] '
          else:
            tikzcode += '\\draw[black,thick,dashed] '
          tikzcode += '(' + name(subset_size_i) + ') -- (' + name(neigh) + ');\n'
    tikzcode += '\n\\end{tikzpicture}\n\\end{document}'
    f = open("figures/"+l[:-1]+".tex", 'w')
    f.write(tikzcode)
    f.close()
    os.system('cd figures/; latexmk -pdf ' + l[:-1] + '.tex')
