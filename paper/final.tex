In this section we present a generalization of Theorem~\ref{thm:partial} that
we believe to be true, present variants of the conjectures, and talk about our
experimental search for counterexamples. 

\paragraph*{Extension of Theorem~\ref{thm:partial}.}
We sketch here a proposed generalization that allows us to avoid not one single zero,
but subsets of zeros while requiring that the lattice has a certain
structure. We first define a few notions.

\begin{definition}
  \label{def:covering-zeros}
  Let~$\calI$ be a downset of~$\bbmB_S$.
  For~$X\in \bbmB_S$, the \emph{non-trivial covering zeros of~$X$ relative
  to~$\calI$} are
  %
  %
  \[\ntcz_{\bbmB_S,\calI}(X) \defeq \{Z \in \ntz_{\bbmB_S}(\calI) \mid  X \subsetneq Z \text{ and there is no } Z' \in \ntz_{\bbmB_S}(\calI) \text{ s.t. } X \subsetneq Z' \subsetneq Z\}. \]
%
\end{definition}

In other words, $\ntcz_{\bbmB_S,\calI}(X)$ simply consists of the minimal
non-trivial zeros of $\calI$ that are strictly above~$X$.
Observe that we have~$\ntcz_{\bbmB_S,\calI}(X) = \emptyset$ whenever~$X\notin
\calI$. We now define \emph{$k$-decomposable} downsets.

\begin{definition}
  \label{def:2decomp}
  A configuration~$\calI$ of~$\bbmB_S$ that is a downset is
  called \emph{$k$-decomposable} if for every~$X\in \bbmB_S$ we have
  $|\ntcz_{\bbmB_S,\calI}(X)| \leq k$.
\end{definition}

This intuitively means that, in every upset, there are at most two non-trivial
covering zeroes.
We believe that the following is true, but have not completely formalized the
proof.

\begin{conjecture}
  \label{conj:main}
For every finite~$S$, for every configuration~$\calI$
of~$\bbmB_{S}$ that is a $2$-decomposable downset of~$\bbmB_{S}$, we
have that~$\calI \in \bullet(\ncpd_{\bbmB_{S}}(\calI))$.
\end{conjecture}

\begin{proof}[Proof sketch.]
  For an element~$X \in \bbmB_S$, let~$I_X \defeq \{{\frakI_{\bbmB_S}(Y)} \cap
  {\frakF_{\bbmB_S}(X)} \mid Y \in {\frakF_{\bbmB_S}(X)}, \allowbreak \muhat_{\bbmB_S,\calI}(Y)
\neq 0 \}$. The idea of the proof would be to show by top-down induction on~$\bbmB_S$
  that for every~$X \in \bbmB_S$, we have~$\calI \cap {\frakF_{\bbmB_S}(X) \in
  \bullet(I_X)}$. Applying this claim to~$X = \emptyset$ yields the desired
result. To show this, we use similar tools to those developed for the proof
of Theorem~\ref{thm:partial}, extended with an accounting of Euler
  characteristics of diverse sets of configurations. It does not seem that the
  proposed proof would directly generalize to the general case, or indeed to
  $3$-decomposable configurations.
\end{proof}

\paragraph*{Strengthenings.} We present here two possible orthogonal ways in which we can\linebreak
strengthen the conjecture; we do not know whether the stronger conjectures are
true, nor whether they are equivalent to the original conjecture.

First, in view of Proposition~\ref{prp:full-multiplicity}, we could require that
the non-trivial intersections are only used positively or only negatively depending
on the sign of their Möbius value. Indeed, 
Proposition~\ref{prp:full-multiplicity} implies that, on tight intersection
lattices, the non-trivial intersections have total multiplicity equal to their
Möbius value, but they may be used both positively and negatively. Likewise, in
view of Proposition~\ref{prp:ncpdfull}, we could require that the principal
downsets are only used positively and negatively. 
%
%
%
%
%
%
%

Second, we do not know if the witnessing trees can be required to be
left-linear, i.e., whether we can obtain $\onehat$ (for the $\nci$ conjecture)
or the desired configuration (for the $\ncpd$ conjecture) by a series of
operations where, at each step, we add a (disjoint) non-trivial intersection (or principal
downset), or subtract one (which must be a subset). For instance, this
formulation does not allow us to express a disjoint union of two configurations
themselves obtained with more complex witnessing trees. This stronger conjecture
is the topic of the question asked in~\cite{cstheory} (up to replacing upsets by
downsets and to working in general lattices instead of only on Boolean lattices). This question also explains why the conjecture is false if asked
about general DAGs (instead of lattices), and shows that the conjecture can be
true for so-called \emph{crown-free lattices}.

We note that the construction for the proof of Theorem~\ref{thm:partial} (or
the one that we have in mind for Conjecture~\ref{conj:main}) does not satisfy either of
these strengthenings. However, the proof of Fact~\ref{fact:annoyingfact} can be
adjusted to satisfy the left-linear condition.


\paragraph*{Counterexample search.}
We have implemented a search for counterexamples of the $\nci$ conjecture. We
consider Sperner families~\cite{sperner}, which give all sets of subsets of a
base set of $n$ elements that are non-equivalent (i.e., that are not the same up
to permuting the elements). We have checked in a brute-force fashion that the conjecture holds up to
$n=5$, i.e., on all intersection lattices such that $\onehat$ has cardinality at
most $5$. Unfortunately, already for $n=6$ there are intersection lattices that
are too large for the computation to finish sufficiently quickly. We also
generated random intersection lattices over larger sets of elements, but could
not find a counterexample (assuming that our code is correct). The code checks
the strong version of the $\nci$ conjecture in the previous terminology, i.e.,
it searches for left-linear trees and requires the non-trivial intersections to
be used precisely with the right polarity. The code is available
as-is~\cite{code}. We have also improved the implementation to search for
solutions on each lattice using a SAT-solver rather than a brute-force search:
this speeds up the computation but also did not yield any counterexample.

We had also implemented, earlier, a search for counterexamples of an
alternative phrasing of the $\ncpd$ conjecture: see~\cite{note}. This also
illustrates that the $\ncpd$ conjecture is false if the configuration to reach
is not a downset; see Figure~4 of~\cite{note} (up to reversing directions, i.e.,
considering downsets instead of upsets).


