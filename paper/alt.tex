In this section we give an alternative formulation of the
conjecture which is about the Boolean lattice. It will in
particular use results from the previous section. As the structure of the
Boolean lattice is ``simpler'', the hope is that 
%
the alternative formulation of the conjecture may be
easier to attack. 
We present the alternative formulation and show that it is
equivalent in Section~\ref{subsec:alt}, and show in
Section~\ref{subsec:useful} some useful properties that we can deduce
from this alternative formulation.

\subsection{The $\ncpd$ conjecture}
\label{subsec:alt}

For~$S$ finite, we call a \emph{configuration of~$\bbmB_S$}, or
simply \emph{configuration} when clear from context, a 
subset of~$\bbmB_S$. 
%
%
%
%
 Since configurations are in particular set
 families, we will generally denote them with cursive letters.

We start by defining what we call \emph{generalized Möbius
functions}, that are parameterized by a configuration, and that
take only one argument.

\begin{definition}
\label{def:muhat}
Let~$S$ finite and~$\calC \subseteq \bbmB_S$ a configuration.
We define the \emph{(generalized) Möbius function~$\muhat_{\bbmB_S,\calC} : \bbmB_S \to \ZZ$ associated
to~$\calC$} by top-down induction by:
\[\muhat_{\bbmB_S,\calC}(X)\defeq \begin{cases}
  1 - \sum_{X \subsetneq X'} \muhat_{\bbmB_S,\calC}(X')\text{ if }X \in \calC\\
                           - \sum_{X \subsetneq X'} \muhat_{\bbmB_S,\calC}(X')\text{ if }X \notin \calC
                          \end{cases}. \]
In particular, the value $\muhat_{\bbmB_S,\calC}(S)$ is~$0$ if $S \notin
\calC$, and $1$ if $S \in \calC$.
\end{definition}

\begin{example}
\label{expl:0111001001110010}
    Let~$\calC$ be the configuration \[\{\{0\}, \{1\}, \{0,1\}, \{0,3\},
    \{1,2\}, \{1,3\}, \{0,1,3\}, \{1,2,3\}  \}\] of~$\bbmB_{\{0,1,2,3\}}$. We
    have depicted in Figure~\ref{fig:0111001001110010}
    this configuration and
    its associated Möbius function.
\end{example}

\begin{figure}
\begin{center}
\input{figures/0111001001110010.tex}
\caption{Visual representation of the configuration~$\calC$ from
Example~\ref{expl:0111001001110010} and of its associated Möbius
  function~$\muhat_{\bbmB_S,\calC}$ beside each node. Here, $\calC$ consists of all the
colored nodes.}
\label{fig:0111001001110010}
\end{center}
\end{figure}



%
%
%
%
%
%
%
%

Next, we define the \emph{non-cancelling principal downsets} of such a
configuration.


\begin{definition}
\label{def:nonzeros}
Let~$S$ finite and~$\calC$ a configuration of~$B_S$. The set of
\emph{non-cancelling principal downsets of~$\bbmB_S$ with respect to~$\calC$},
denoted~$\ncpd_{\bbmB_S}(\calC)$, is defined by
\[\ncpd_{\bbmB_S}(\calC) \defeq \{\frakI_{\bbmB_S}(X) \mid X \in \bbmB_s,\  \muhat_{\bbmB_S,\calC}(X) \neq 0\}.\]
(Recall that we write $\frakI_{\bbmB_S}(X)$ for $\frakI_{\bbmB_S}(\{X\})$.)
\end{definition}

  Note that the elements of $\ncpd_{\bbmB_S}(\calC)$ are subsets of~$\bbmB_S$. The notation might appear heavy at first sight, e.g., using subscripts
  in $\muhat_{\bbmB_S,\calC}$ and $\ncpd_{\bbmB_S}$ to always recall which
  Boolean lattice we are considering. This will however help to avoid confusion
  in later proofs, when multiple Boolean lattices and configurations will be
  involved.

  We are now ready to state the alternative formulation, which we call for convenience the~\emph{$\ncpd$ conjecture}.

\begin{conjecture}[$\ncpd$ conjecture]
\label{conj:ncpd}
For every finite~$S$, for every configuration~$\calI$ of~$\bbmB_{S}$ that is a
downset of~$\bbmB_{S}$, we have that~$\calI \in \bullet(\ncpd_{\bbmB_{S}}(\calI))$.
\end{conjecture}


%
%
%

We point out that, if we do not ask the configuration to be a
downset of~$\bbmB_S$ then the statement is false, i.e., there exists
a finite~$S$ and configuration~$\calC$ of~$\bbmB_S$ such
that~$\calC \notin \bullet(\ncpd_{\bbmB_S}(\calC))$: see
Section~\ref{sec:final}.


We prove in the remaining of this section that this conjecture is
equivalent to the~$\nci$ conjecture. We start with a general-purpose lemma
that gives a correspondence between both formulations:

\begin{lemmarep}
  \label{lem:embeds}
  Let~$S$ be a finite set and let~$\calI$ be a downset of~$\bbmB_S$ of the form~$\calI =
  {\frakI_{\bbmB_S}(\calF)}$ for~$\calF$ a non-trivial set family.
  For~$X\in \calF$ let~$X' \defeq \frakI_{\bbmB_S}(X)$ (which is $2^X$),
  and let~$\calF' \defeq \{X' \mid X \in \calF\}$. Note that 
  $\calF'$ is non-trivial as well, and let then~$L$ be its intersection
  lattice. 
Then we have~$\ncpd_{\bbmB_S}(\calI) = \nci(L)$.
\end{lemmarep}
\begin{proof}
  Observe that~$\onehat_L = \calI$. For~$\calT \subseteq \calF$, we
  write~$\calT'$ for the corresponding subset of~$\calF'$,
  i.e.,~$\calT' = \{X' \mid X\in \calT\}$, and vice versa. For
  $\calT \subseteq \calF, \calT \neq \emptyset$ let~$X_\calT \defeq
  \bigcap \calT$. We show that the following hold:
  \begin{enumerate}
    \item \label{firstitem} For~$\calT'\subseteq \calF', \calT'\neq
      \emptyset$ we have~$S_{\calT'} = {\frakI_{B_S}(X_\calT)}$.
    \item \label{seconditem} Let~$L'$ be the lattice~$(\{X_\calT \mid
      \calT\subseteq \calF, \calT \neq \emptyset\} \cup \{\onehat_{L'}\},
      \leq_{L'})$ where~$\onehat_{L'}$ is a fresh element
      and~$Y\leq_{L'} \onehat_{L'}$ for all $Y\in L'$ and~$X_{\calT_1} \leq_{L'} X_{\calT_2}$
      for~$\calT_1,\calT_2 \subseteq \calF$ if~$X_{\calT_1} \subseteq X_{\calT_2}$. Then~$L
      \simeq L'$ via the isomorphism that sends~$\onehat_L$
      to~$\onehat_{L'}$ and~$S_{\calT'}$ for~$\calT' \subseteq \calF'$, $\calT' \neq \emptyset$ to~$X_T$.
    \item \label{thirditem} For every~$Y\in \bbmB_S$, if~$Y$ is of the form~$X_\calT$ 
      then~$\muhat_{\bbmB_S,\calI}(Y) = - \mu_L(S_{\calT'},\onehat_L)$, otherwise $\muhat_{\bbmB_S,\calI}(Y) = 0$.
  \end{enumerate}

  Notice that Item (\ref{thirditem}) together with Item (\ref{firstitem})
  implies that~$\ncpd_{\bbmB_S}(\calI) = \nci(L)$, which is what we need to
  prove.
  Item (\ref{firstitem}) follows from the chain of trivialities~$S_{\calT'}
  \defeq \bigcap \calT' \defeq \bigcap_{X'\in \calT'} X' = \bigcap_{X\in \calT}
  2^X = 2^{\bigcap \calT} \defeq 2^{X_\calT} \defeq
  {\frakI_{B_S}(X_\calT)}$, and Item (\ref{seconditem}) is easy to
  prove using Item (\ref{firstitem}). 
  For Item (\ref{thirditem}), we show
  that~$\muhat_{\bbmB_S,\calI}$ is as claimed. For~$Y\in \bbmB_S$,
  $Y\notin \calI$, it is clear 
  that~$\muhat_{\bbmB_S,\calI}(Y) = 0$, by definition of $\muhat_{\bbmB_S,\calI}$ and because~$\calI$ is a downset, so we
  can focus on~$Y\in \calI$. We show by top-down induction on~$\calI$ that
  $\muhat_{\bbmB_S,\calI}$ is as claimed on~$\calI$. The base case is
  when~$Y$ is a maximal element of~$\calI$, i.e.,~$Y=X$ for
  some maximal~$X\in \calF$. Then we have~$\muhat_{\bbmB_S,\calI}(Y) = 1 = -
  \mu_L(X')$ indeed.
  The inductive case is when~$Y$ is not a maximal element of~$\calI$.
  Let~$\calT_Y \defeq \{X\in \calF \mid Y \subseteq X\}$, which is non-empty. Notice
  the following fact ($\dagger$): we have $Y \subseteq X_{\calT_Y}$, and more specifically we
  have~$Y\subseteq X_\calT$ for~$\calT\subseteq \calF, \calT\neq \emptyset$ if
  and only if~$\calT \subseteq \calT_Y$. We then distinguish two cases.
  \begin{itemize}
    \item The first case is when~$Y=X_{\calT_Y}$. We
      have $\muhat_{\bbmB_S,\calI}(Y) = 1 - \sum_{Y \subsetneq Y'}
      \muhat_{\bbmB_S,\calI}(Y')$ by definition. Now by what precedes, if $Y'
      \notin \calI$ then $\muhat_{\bbmB_S,\calI}(Y') = 0$, and by
      induction hypothesis if $Y'$ is not of the form $X_\calT$
      then $\muhat_{\bbmB_S,\calI}(Y') = 0$. Hence, we have
      $\muhat_{\bbmB_S,\calI}(Y) = 1 - \sum_{\substack{Y \subsetneq
      Y'\\ Y' = X_{\calT}}} \muhat_{\bbmB_S,\calI}(X_{\calT})$, and
      the suitable $\calT$ are the subsets of~$\calT_Y$ by
      ($\dagger$), i.e., $\muhat_{\bbmB_S,\calI}(Y) = 1 - \sum_{Y'
      \in \{X_{\calT} \mid \calT \subseteq \calT_Y\} \setminus
    \{Y\}} \muhat_{\bbmB_S,\calI}(X_{\calT})$. Now, by induction
    hypothesis, we have $\muhat_{\bbmB_S,\calI}(X_{\calT}) = -
    \mu_L(S_{\calT'},\onehat_L)$ for all $\calT \subseteq \calT_Y$
    such that~$X_{\calT} \neq Y$, therefore
    $\muhat_{\bbmB_S,\calI}(Y) = 1 + \sum_{Y' \in \{X_{\calT} \mid
    \calT \subseteq \calT_Y\} \setminus \{Y\}}
    \mu_L(S_{\calT'},\onehat_L)$. Now by Item (\ref{seconditem})
    this is equal to $1 + \sum_{\substack{U \in L\\U\neq \onehat_L
    \\ S_{\calT_Y'} \subsetneq U}} \mu_L(U,\onehat_L)$, which is
    equal to $\sum_{\substack{U \in L\\ S_{\calT_Y'} \subsetneq U}}
    \mu_L(U,\onehat_L)$, which is $-\mu_L(S_{\calT_Y'})$ by
    definition. So indeed $\muhat_{\bbmB_S,\calI}(Y) = -
    \mu_L(S_{\calT_Y'})$.

    \item The second case is when~$Y \neq X_{\calT_Y}$. Then, the
      nodes of $\calI$ in the principal upset of~$Y$ minus~$\{Y\}$ consists of
      the nodes of~$\calI$ in the principal upset of
      $X_{\calT_Y}$, and of nodes that are not of the form
      $X_\calT$ by ($\dagger$) and that are strict supersets of~$Y$. By induction hypothesis, the
      $\muhat_{\bbmB_S,\calI}$-value of the latter is zero. So
      $\muhat_{\bbmB_S,\calI}(Y) = 1 - \sum_{X_{\calT_Y} \subseteq
      Y'} \muhat_{\bbmB_S,\calI}(Y') = 1 -
      \muhat_{\bbmB_S,\calI}(X_{\calT_Y}) - \sum_{X_{\calT_Y}
        \subsetneq Y'} \muhat_{\bbmB_S,\calI}(Y')$, but by
        definition of $\muhat_{\bbmB_S,\calI}$ we have
        $\muhat_{\bbmB_S,\calI}(X_{\calT_Y}) = 1 -
        \sum_{X_{\calT_Y} \subsetneq Y'}
        \muhat_{\bbmB_S,\calI}(Y')$, so indeed
        $\muhat_{\bbmB_S,\calI}(Y) = 0$. This concludes the proof.\qedhere
  \end{itemize}
\end{proof}

The proof of this lemma is tedious and we defer it to
Appendix~\ref{apx:alt}. We then show each direction of the
equivalence in turn.

\subparagraph*{$\nci$ conjecture (formulation I) $\implies$ $\ncpd$ conjecture.}
Assume Conjecture~\ref{conj:general} to be true, let~$\calI$ be a
configuration of~$\bbmB_{S}$ for some finite set~$S$, assume that $\calI$ is a downset, and let us show that~$\calI \in
\bullet(\ncpd_{\bbmB_{S}}(\calI))$. If~$\calI$ is empty this is clear because then we have 
$\bullet(\ncpd_{\bbmB_{S}}(\calI)) = \bullet(\emptyset) = \{\emptyset\}$.
If~$\calI$ is a principal downset
of~$\bbmB_S$ this is also clear, because in that case we have
that $\muhat_{\bbmB_S,\calI}(X)$ for~$X\in \bbmB_S$ equals~$1$ if~$\calI = \frakI_{\bbmB_S}(X)$ and~$0$ otherwise,
so assume that~$\calI$ is not empty and is not a principal
downset. Then~$\calI$ is of the form $\frakI_{\bbmB_S}(\calF)$ for some non-trivial set family~$\calF$.  
Let then~$L$ be the intersection lattice constructed from~$S$ and~$\calF$
as in Lemma~\ref{lem:embeds}. By this Lemma we have
that $\ncpd_{\bbmB_S}(\calI) = \nci(L)$. By the hypothesis of
Conjecture~\ref{conj:general} being true we have~$\onehat_L \in
\bullet(\nci(L))$, but~$\onehat_L$ is~$\calI$ so indeed $\calI \in
\bullet(\ncpd_{\bbmB_{S}}(\calI))$.\\

%

%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%


\subparagraph*{$\ncpd$ conjecture $\implies$ $\nci$ conjecture (formulation II).}
Assume now that Conjecture~\ref{conj:ncpd} is true, let~$\calF$ be a
non-trivial set family 
such that the intersection lattice~$L = \bbmL_\calF$ is tight, 
%
and let us show that~$\onehat_L \in \bullet(\nci(L))$. For~$X\in \calF$ let~$X'
\defeq 2^X$, and let~$\calF'  \defeq \{X' \mid X \in \calF\}$. Note that~$\calF'$ is non-trivial as well, so let us consider~$L'=\bbmL_{\calF'}$.
For~$\calT \subseteq \calF$, let~$\calT'$ be the corresponding subset of~$\calF'$, i.e.,~$\calT' = \{X' \mid X \in \calT\}$, and vice versa.
Then observe that
for~$\calT\subseteq \calF, \calT \neq \emptyset$ we have~$S_{\calT'} =
2^{S_\calT}$, so that~$L\simeq L'$.
Moreover we have that~$L'$ is full: indeed it is clear that
for~$\calT\subseteq \calF, \calT\neq \emptyset$ we have $S_\calT
\in \{X \in \onehat_{L'} \mid \min_{L'}(X) =
S_{\calT'}\}$.\footnote{But note that~$L'$ is never tight: indeed
we have, e.g., $\{X \in \onehat_{L'} \mid \min_{L'}(X) =
\zerohat_{L'}\} = \{\zerohat_L, \emptyset\}$.}
Define now~$S= \onehat_L$ (which is finite because~$L$ is tight), and let~$\calI$ be the downset of~$\bbmB_S$ that is
$\frakI_{\bbmB_S}(\calF)$. Observe then that~$L'$ is exactly the
intersection lattice that we would obtain from the construction described in
Lemma~\ref{lem:embeds} applied on~$S$ and~$\calF$, so that~$\ncpd_{\bbmB_S}(\calI) =
\nci(L')$ by that lemma. Since Conjecture~\ref{conj:ncpd} is true by hypothesis we have~$\calI
\in \bullet(\ncpd_{\bbmB_S}(\calI))$, hence~$\calI \in \bullet(\nci(L'))$. But $\calI =
\{Y\in \bbmB_S \mid \exists X \in \calF,~Y\subseteq X\}$ by definition, which
is equal to $\bigcup \calF'$, which
is~$\onehat_{L'}$ by definition. Therefore~$\onehat_{L'} \in
\bullet(\nci(L'))$. 
This implies~$\onehat_L \in \bullet(\nci(L))$ as well by
Lemma~\ref{lem:from-full}.

\subsection{Useful facts}
\label{subsec:useful}
We now present useful facts about the $\ncpd$ formulation of the
conjecture that motivate its introduction.

First, we observe that the generalized Möbius functions are linear (in their parameters)
under disjoint union and subset complement.

\begin{fact}
\label{fact:gen-mob-linear}
    Let~$S$ finite and~$\calC_1,\calC_2$ be two configurations of~$\bbmB_S$ such
that~$\calC_1 \cupdot \calC_2$ (resp., $\calC_1 \minusdot \calC_2$) is well defined. Then
$\muhat_{\bbmB_S,\calC_1 \cupdot \calC_2} = \muhat_{\bbmB_S,\calC_1 } + \muhat_{\bbmB_S,\calC_2}$ (resp.,
$\muhat_{\bbmB_S,\calC_1 \minusdot \calC_2} = \muhat_{\bbmB_S,\calC_1} - \muhat_{\bbmB_S,\calC_2}$).
\end{fact}
\begin{proof}
    One can simply check by top-down induction that for all $X\in \bbmB_S$ we
have $\muhat_{\bbmB_S,\calC_1 \cupdot \calC_2}(X) = \muhat_{\bbmB_S,\calC_1}(X) +
\muhat_{\bbmB_S,\calC_2}(X)$ (resp., $\muhat_{\bbmB_S,\calC_1 \minusdot \calC_2}(X) =
\muhat_{\bbmB_S,\calC_1}(X) - \muhat_{\bbmB_S,\calC_2}(X)$).
\end{proof}

%
%
%
%
%

%
%
%

Second, we show that, if we allow any principal downset in the dot algebra, then we can
build any configuration: this is the analogue of
Fact~\ref{fact:annoyingfact}. In fact we will use the following
stronger result:\footnote{In this lemma and its proof we
correctly use the notation $\frakI_{\bbmB_S}(\{X\})$ for~$X\in
\bbmB_S$ instead of $\frakI_{\bbmB_S}(X)$ to avoid confusion.}


\begin{lemma}
\label{lem:allreach}
Let~$\calC$ be a configuration of~$\bbmB_S$ (for $S$ finite), and let
  \[A_\calC \defeq \{\frakI_{\bbmB_S}(\{X\}) \mid X\in
  {\frakI_{\bbmB_S}(\calC)} \}.\]
Then we have~$\calC \in \bullet(A_\calC)$.
\end{lemma}
\begin{proof}
By induction on the size of~$\frakI_{\bbmB_S}(\calC)$.
%
%
%
%
%
 The base case is when~$|\frakI_{\bbmB_S}(\calC)| = 0$, which
 can only happen when
 $\calC$ is $\emptyset$.
  Then clearly $\calC \in \bullet(A_\calC)$ as $A_\calC = \emptyset$ and $\bullet(\emptyset) = \{\emptyset\}$.

For the inductive case, assume the
claim is true for all~${\calC'}$ with~$|\frakI_{\bbmB_S}({\calC'})| \leq n$, and let us
show it is true for~$\calC$ with $|\frakI_{\bbmB_S}(\calC)| = n+1$. Let~$X \in
\calC$ be maximal, and consider~${\calC'} \defeq \calC \setminus
\{X\}$.  Then $|\frakI_{\bbmB_S}({\calC'})| \leq n$, hence by induction hypothesis 
we have $\calC' \in \bullet(A_{\calC'})$.
But it is clear that~$A_{{\calC'}} \subseteq A_\calC$, so that
$\calC' \in \bullet(A_{\calC})$ as well.
Moreover,
  consider~$\calC_1 \defeq {\frakI_{\bbmB_S}(\{X\}) \setminus \{X\}}$. By induction hypothesis and for the same reasons 
we have~$\calC_1 \in \bullet(A_{\calC})$.
Then~$\calC' \cupdot (\frakI_{\bbmB_S}(\{X\}) \minusdot \calC_1)$ is a valid expression
witnessing that~$\calC \in \bullet(A_{\calC})$.
\end{proof}


Third, we show that all instances of the~$\ncpd$ conjecture are
already “full”, in the sense that an analogous version of
Proposition~\ref{prp:full-multiplicity} automatically holds for them
(whereas we saw that Proposition~\ref{prp:full-multiplicity} might
be false for intersection lattices that are not full). 
 Let~$\pd(\bbmB_S)$ be the set of principal downsets of~$\bbmB_S$, i.e.,~$\pd(\bbmB_S) \defeq \{ \frakI_{\bbmB_S}(X) \mid X \in \bbmB_S\}$. We show:

\begin{proposition}
  \label{prp:ncpdfull}
   Let~$S$ finite and~$\calI$ be a configuration of~$\bbmB_S$ that
   is a downset and~$T$ a tree witnessing that $\calI \in \bullet(\pd(\bbmB_S))$. Then we have $\mult_T(\frakI_{\bbmB_S}(X)) = 
   \muhat_{\bbmB_S,\calI}(X)$ for every~$X \in \bbmB_S$.
   %
   %
\end{proposition}
\begin{proof}
  %
  %
  %
  %
  %
  %
  It is routine to show by (bottom-up) induction on~$T$ that, for every
  node~$n\in T$, letting~$\calC_n$ be the corresponding
  configuration represented by the subtree of~$T$ rooted at~$n$ we
  have that $\mult_{T_n}(\frakI_{\bbmB_S}(X)) =
  \muhat_{\bbmB_S,\calC_n}(X)$ for every~$X \in \bbmB_S$; this in particular uses the linearity of
  generalized Möbius functions under disjoint unions and subset complements. Applying
  this claim to the root of~$T$ gives the claim.
\end{proof}

Last, we establish a connection between the generalized Möbius
function of a configuration~$\calC$ and the \emph{Euler
characteristic} of~$\calC$, that will be crucial in the next
section.

\begin{definition}
  Let~$\calC$ be a set family of finite sets. We define
the~\emph{Euler characteristic of~$\calC$}, denoted~$\eul(\calC)$,
by $\eul(\calC) \defeq \sum_{X\in \calC} (-1)^{|X|}$.
\end{definition}

\begin{proposition}
  \label{prp:mob-eul}
  Let~$S$ finite and $\calC$ a configuration of~$\bbmB_S$. Then we have $\muhat_{\bbmB_S,\calC}(X) = (-1)^{|X|} \times \eul(\calC \cap \frakF_{\bbmB_S}(X))$ for every~$X\in \bbmB_S$.
\end{proposition}
\begin{proof}
  Define~$f,g:\bbmB_S \to \ZZ$ by~$f(X) \defeq
  \muhat_{\bbmB_S,\calC}(X)$ and~$g(X) = 1$ if~$X\in \calC$ and~$0$
  otherwise. Observe then that, by definition
  of~$\muhat_{\bbmB_S,\calC}$ we have $g(X) = \sum_{\substack{X'\in
  \bbmB_S \\ X\subseteq X'}} f(X')$ for all $X\in \bbmB_S$.
  Then by Proposition~\ref{prp:inv_bool} we have that
  $f(X) =
    \sum_{\substack{X'\in \bbmB_S \\ X\subseteq X'}}
    (-1)^{|X'|-|X|} g(X')$ for all $X\in \bbmB_S$, i.e.,
    \begin{align*}
      \muhat_{\bbmB_S,\calC}(X) &= \sum_{\substack{X'\in \bbmB_S \\ X\subseteq X'}}
    (-1)^{|X'|-|X|} g(X')\\
      &= \sum_{X'\in {\calC \cap \frakF_{\bbmB_S}(X)} }
    (-1)^{|X'|-|X|}\\
&= (-1)^{|X|} \times \sum_{X'\in \calC \cap \frakF_{\bbmB_S}(X) }
    (-1)^{|X'|}\\
&= (-1)^{|X|} \times \eul(\calC \cap \frakF_{\bbmB_S}(X))
    \end{align*}
    for all~$X\in \bbmB_S$, which is what we wanted to show.
\end{proof}

Connections between the Möbius function and the Euler
characteristic have already been shown, for instance see Philip
Hall's theorem~\cite[Proposition 6 and Theorem
3]{rota1964foundations}. As far as we can tell, however, the
connection from Proposition~\ref{prp:mob-eul} seems new.
