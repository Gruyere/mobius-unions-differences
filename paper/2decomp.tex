In this section we show that the $\ncpd$ conjecture holds in the particular
case when the downset is what we call \emph{2-decomposable}.  We start by
defining this notion.

\begin{definition}
  \label{def:ntcz}
  Let~$\calI$ be a downset of~$\bbmB_S$. We define the \emph{non-trivial zeros
  of~$\calI$}, denoted~$\ntz_{\bbmB_S}(\calI)$, by
  \[\ntz_{\bbmB_S}(\calI) \defeq \{Z \in \calI \mid \muhat_{\bbmB_S,\calI}(Z) = 0 \}.\]
    By opposition, the \emph{trivial} zeros of~$\calI$ are the elements~$Z \in
    \bbmB_S \setminus \calI$ such that~$\muhat_{\bbmB_S,\calI}(Z) = 0$ (notice
    that these form a upset of~$\bbmB_S$).
\end{definition}


\begin{definition}
  \label{def:covering-zeros}
  Let~$\calI$ be a downset of~$\bbmB_S$.
  For~$X\in \bbmB_S$, define the \emph{non-trivial covering zeros of~$X$ relative to~$\calI$}, denoted
  $\ntcz_{\bbmB_S,\calI}(X)$, by
  \[\ntcz_{\bbmB_S,\calI}(X) \defeq \{Z \in \ntz_{\bbmB_S}(\calI) \mid  X \subsetneq Z \text{ and there is no } Z' \in \ntz_{\bbmB_S}(\calI) \text{ s.t. } X \subsetneq Z' \subsetneq Z\}. \]
%
\end{definition}

In other words, $\ntcz_{\bbmB_S,\calI}(X)$ simply consists of the maximal
non-trivial zeros of $\calI$ that are strictly above~$X$.
Observe that we have~$\ntcz_{\bbmB_S,\calI}(X) = \emptyset$ whenever~$X\notin
\calI$. We can now define $k$-decomposable downsets.

\begin{definition}
  \label{def:2decomp}
  A configuration~$\calI$ of~$\bbmB_S$ that is a downset is
  called~$k$-decomposable if for every~$X\in \bbmB_S$ we have
  $|\ntcz_{\bbmB_S,\calI}(X)| \leq k$.
\end{definition}

Our main positive result is then the following:

\begin{theorem}
  \label{thm:main}
For every finite~$S$, for every configuration~$\calI$
of~$\bbmB_{S}$ that is a $2$-decomposable downset of~$\bbmB_{S}$, we
have that~$\calI \in \bullet(\ncpd_{\bbmB_{S}}(\calI))$.
\end{theorem}

The proof reuses and non-trivially generalizes some ideas and
notions that are introduced in~\cite{monet2020solving}. We define
and extend in Section~\ref{subsec:pairs} notions from
\cite{monet2020solving}, in particular the notion of \emph{adjacent
pairs} and certain equivalence classes. In
Section~\ref{subsec:lifting-and-simul} we show what we call the \emph{lifting lemma}, and show adjacent pairs can be
simulated with principal downsets.
We combine everything in Section~\ref{subsec:proof} to prove Theorem~\ref{thm:main}.
  From now on all Boolean lattices~$\bbmB_S$ are for~$S$ finite.

\subsection{Adjacent pairs}
  \label{subsec:pairs}


\begin{definition}
\label{def:pair}
An \emph{adjacent pair} of~$\bbmB_S$ is a configuration~$\calP$ of
the form~$\{X,X\minusdot \{x\}\}$ (so for~$X \in \bbmB_S$ and~$x\in
X$).
\end{definition}

\begin{definition}
\label{def:pairs_game}
Let~$A$ be a set of adjacent pairs of~$\bbmB_S$, 
  %
%
  and let~$\calC,\calC'$ two configurations 
of~$\bbmB_S$.  We then write $\calC \rewr{+ A}{3} \calC'$ when there
exists an adjacent pair~$\calP\in A$ such that
%
%
$\calC' = \calC \cupdot \calP$.  
Similarly we write
$\calC \rewr{-A}{3} \calC'$ whenever $\calC' \rewr{+A}{3}
\calC$ (i.e., when there exists~$\calP\in A$ such that~$\calC' = \calC \minusdot \calP$), and 
write $\calC \rewr{$\pm$ A}{3} \calC'$ when
$\calC \rewr{+A}{3} \calC'$ or $\calC \rewr{-A}{3}
\calC'$.  Observe that $\rewr{$\pm A$}{3}$ is symmetric.  We write
$\rewr{$\pm A$}{3}^*$ the reflexive transitive closure of $\rewr{$\pm A$}{3}$,
and write~$\simeq_{A}$ the induced equivalence relation.
\end{definition}

In other words, we have $\calC \simeq_{A} \calC'$ when we can go
from~$\calC$ to~$\calC'$ by iteratively (1) adding an 
adjacent pair from~$A$ to the current configuration if the pair is disjoint from the
configuration; or (2) removing an adjacent pair from~$A$ to the current
configuration if the pair was included in the configuration.

\begin{definition}
  \label{def:bla}
  For a subset~$\calG$ of~$\bbmB_S$, define the set of 
  \emph{allowed adjacent pairs of~$\bbmB_S$ relative to~$\calG$},
  denoted~$\ap_{\bbmB_S}(\calG)$, by
  \[\ap_{\bbmB_S}(\calG) \defeq \{\calP \mid \calP \text { is and adjacent pair of } \bbmB_S \text { and } \calP \subseteq \calG\}.\]
\end{definition}

\begin{definition}
  \label{def:connected}
  For~$S$ finite, define $G_{S}$ to be the undirected graph whose vertices
  are~$2^S$ and whose edges are all adjacent pairs of~$\bbmB_S$.
  A subset~$\calG$ of~$\bbmB_S$ is \emph{connected} if it is
  a connected set of nodes in $G_S$.
\end{definition}

The goal of this section is to establish the following proposition:

\begin{proposition}
  \label{prp:eul-equiv}
  Let~$\calG$ be a connected subset of~$\bbmB_S$, and
  $\calC_1, \calC_2$ two configurations that are included in~$\calG$. We have $\eul(\calC_1) = \eul(\calC_2)$ if and only if
  $\calC_1 \simeq_{\ap_{\bbmB_S}(\calG)} \calC_2$.
\end{proposition}

This result already appears in \cite[Proposition
6.1]{monet2020solving}, but only when~$\calG$ is the whole Boolean
lattice, i.e., when~$\calG = 2^S$.\footnote{We will actually only use this result
when~$\calG$ is a downset of~$\bbmB_S$, but we still prove this more
general version for completeness and because it is not much more complicated.} Observe that the “if” direction in Proposition~\ref{prp:eul-equiv} is trivial, since adding or removing an adjacent pair (with disjoint union or subset complement) does not modify the Euler characteristic. Hence we need to prove the “only if” direction.
To this end, we reproduce the following lemma from~\cite{monet2020solving}:


\begin{lemma}[Lemma 5.10 of \cite{monet2020solving}]
\label{lem:chaining}
	Let~$\calC$ be a configuration of~$\bbmB_S$, and $X \neq X'$ be two subsets of~$S$ such that there is a simple path~$P$ of the form
	$X = X_0 - \cdots - X_{n+1} = X'$ from $X$ to $X'$
	in~$G_S$ with $n \geq 0$ and $X_i \notin \calC$ for $1 \leq i \leq n$.
	Then we have the following:
	\begin{description}
          \item[\textbf{Erasing.}] If $(-1)^{|X|} \neq (-1)^{|X'|}$ (i.e., $n$ is even) and $\{X,X'\} \subseteq \calC$ then,
			defining~$\calC'$ by $\calC' \defeq \calC \setminus \{X,X'\}$, we have $\calC \rewr{$\pm \ap_{\bbmB_S}(P)$}{8}^* \calC'$.
			We say that we go from~$\calC$ to~$\calC'$ by \emph{erasing~$X$ and~$X'$}.
                      \item[\textbf{Teleporting.}] If $(-1)^{|X|} = (-1)^{|X'|}$ (i.e., $n$ is odd) and $X \in \calC$ and $X' \notin \calC$ then,
			defining~$\calC'$ by $\calC' \defeq (\calC \setminus \{X\}) \cup \{X'\}$, we have $\calC \rewr{$\pm \ap_{\bbmB_S}(P)$}{8}^* \calC'$.
			We say that we go from~$\calC$ to~$\calC'$ by \emph{teleporting~$X$ to~$X'$}.
	\end{description}
\end{lemma}
\begin{proof}
        We only explain the erasing part, as teleporting works
        similarly. Let $n=2i$. For $0 \leq j < i$, do the
        following: add the adjacent pair $\{X_{2j+1}, X_{2j+2}\}$
        and remove the adjacent pair $\{X_{2j},X_{2j+1}\}$.
        Finally, remove the adjacent pair~$\{X_{2i}, X_{2i+1}\}$.
\end{proof}

Teleporting is illustrated in Figure~\ref{fig:teleporting} (adapted from~\cite{monet2020solving}). 

\begin{figure}
\centering
\input{figures/chainswap}
\caption{Imagine that the path at the top occurs in $G_S$ for some configuration~$\calC$ of~$\bbmB_S$ (in orange), and let~$P = \{X_0,\ldots,X_4\}$.
	The consecutive orange configurations of~$\bbmB_S$ are obtained by single steps of the transformation.
      The total transformation illustrates what is called \emph{teleporting} in Lemma~\ref{lem:chaining}: we go from~$\calC$ to~$\calC'$ in the bottom path by teleporting $X_1$ to~$X_4$.}
\label{fig:teleporting}
\end{figure}

We will also reuse the \emph{fetching lemma} from~\cite{monet2020solving},
extending it to connected subsets of~$\bbmB_S$ (instead of
the full Boolean lattice).
Given the right circumstances, this lemma fetches for us two sets~$X,X'\in
\calC$ and a suitable path so that we can erase~$X$ and $X'$. Formally:

\begin{lemma}[Fetching lemma, slightly extending Lemma 5.11 of \cite{monet2020solving}]
\label{lem:fetch}
	Let~$\calG$ be a subset of~$\bbmB_S$ and~$\calC \subseteq \calG$ a configuration such that $|\calC| \neq |\eul(\calC)|$.
	Then there exist $X,X'\in \calC$ with $(-1)^{|X|} \neq (-1)^{|X'|}$ and a simple path
	$X = X_0 - \cdots - X_{n+1} = X'$ from $X$ to $X'$ in 
	$G_S$ (hence with $n$ even) with all nodes in this path being in~$\calG$ such that $X_i \notin \calC$ for $1 \leq i \leq n$.
\end{lemma}
\begin{proof}
	Since $|\calC| \neq |\eul(\calC)|$, there exist $X'',X''' \in \calC$ with $(-1)^{|X''|} \neq (-1)^{|X'''|}$.
	Let $X'' = X''_0 - \cdots - X''_{m+1} = X'''$ be an arbitrary simple path from $X''$ to $X'''$ in 
	$G_S$ with all nodes being in~$\calG$ (such a path clearly exists because $\calG$ is a connected subset of~$G_S$).
	Now, let $k_1 \defeq \max(0 \leq j \leq m \mid (-1)^{|X''_j|} = (-1)^{|X''|} \text{ and }X''_j \in \calC)$, and then
	let $k_2 \defeq \min(k_1 < j \leq m+1 \mid (-1)^{|X''_{j}|} = (-1)^{|X'''|} \text{ and }X''_j \in \calC)$, which are well defined.
        Then we can take $X$ to be $X''_{k_1}$ and $X'$ to be $X''_{k_2}$, which satisfy the desired property.
\end{proof}


With these in place we are ready to prove
Proposition~\ref{prp:eul-equiv}.

\begin{proof}[Proof of Proposition~\ref{prp:eul-equiv}]
  As mentioned above, we only need to prove the “only if” part of the statement.
  Starting from~$\calC_1$, we repeatedly use the fetching lemma and
  Erasing until we obtain a configuration~$\calC_1' \subseteq
  \calG$ such that $\calC_1' \simeq_{\ap_{\bbmB_S}(\calG)} \calC_1$
  and~$|\calC_1'| = \eul(\calC_1')$. We do the same with~$\calC_2$,
  obtaining~$\calC_2' \subseteq \calG$ with $\calC_2'
  \simeq_{\ap_{\bbmB_S}(\calG)} \calC_2$ and~$|\calC_2'| =
  \eul(\calC_2')$. If~$\calC_1' = \calC_2'$ we are done.
  Otherwise, let~$n \defeq |\calC_1'
  \setminus \calC_2'|$, which is~$>0$ because~$|\calC_1'| = |\calC_2'|$. We build by induction a
  sequence of configurations~$\calC_1' = \calC''_0,\ldots,\calC''_n
  = \calC_2'$ that are all included in~$\calG$ and that satisfy (1)  $\calC''_i
  \simeq_{\ap_{\bbmB_S}(\calG)} \calC''_{i+1}$ for all~$0 \leq i
  \leq n-1$ and (2) $|\calC''_i| = \eul(\calC''_i)$ and~$|\calC''_i \setminus \calC_2'| =
  n - i $ for all~$0 \leq i \leq n$. It is clear that this indeed
  implies the claim. As~$\calC''_0$ satisfies condition (2), it is enough to explain how to build $\calC''_{i+1}$
  from~$\calC''_i$ for~$i< n$, and then to argue that the~$\calC''_n$ from this construction is indeed equal to~$\calC_2'$. We have $|\calC''_i \setminus \calC_2'| =
  n - i > 1$ by induction hypothesis, so there exists~$X'\in \calC''_i \setminus \calC_2'$ and~$X'' \in \calC_2' \setminus \calC''_i$ (because~$|\calC_i''|  = \eul(\calC_i'') = \eul(\calC_1') = |\calC_1'| = |\calC_2'|$).
	Let $X' = X'_0 - \cdots - X'_{m+1} = X''$ be an arbitrary simple path from $X'$ to $X''$ in 
	$G_S$ with all nodes being in~$\calG$ (such a path exists because $\calG$ is a connected subset of~$G_S$, and~$m$ is odd and~$\geq 1$).
        Observe that all nodes~$X''_k$ with~$k$ odd are not in~$\calC''_i \cup \calC_2'$.
	Now, let $k_1 \defeq \max(0 \leq j \leq m \mid X'_j \in \calC''_i \setminus \calC_2')$, and then
        let $k_2 \defeq \min(k_1 < j \leq m+1 \mid X'_j \in \calC_2' \setminus \calC''_i)$, which are well defined. Define~$\calC''_{i+1} \defeq \calC''_i \setminus \{X_{k_1}\} \cup \{X_{k_2}\}$. 
        Notice that~$\calC''_{i+1}$ satisfies condition (2), so all we need
        to show is that~$\calC''_i \simeq_{\ap_{\bbmB_S}(\calG)}
        \calC''_{i+1}$.
        We know the following: for a node~$X'_k$ with~$k_1 < k <
        k_2$, if~$k$ is odd then~$X'_k \notin \calC''_i \cup
        \calC'_2$, and if~$k$ is even then either~$X'_k \in
        \calC''_i \cap \calC'_2$ or $X'_k \notin \calC''_i \cup
        \calC'_2$. If there is no node~$X'_k$ with~$k_1 < k < k_2$,
        $k$ even such that~$X'_k \in \calC_i''$, then we can simply
        use Lemma~\ref{lem:chaining} to teleport~$X_{k_1}$
        to~$X_{k_2}$ and we are done. Otherwise,
        let~$X'_{\ell_1},\ldots,X'_{\ell_m}$ with~$m\geq 1$ be all
        the nodes with~$k_1 <\ell_p < k_2$ even that are
        in~$\calC''_i$, in order, i.e.,~$\ell_1 < \ldots< \ell_m$.
        Then we can successively teleport~$X'_{\ell_{m}}$ to
        $X'_{k_2}$, $X'_{\ell_{m-1}}$ to $X'_{\ell_{m}}$, and so
        on, until we teleport~$X'_{\ell_1}$ to $X'_{\ell_2}$ and
        finally we teleport~$X'_{k_1}$ to~$X'_{\ell_1}$, thus
        obtaining~$\calC''_{i+1}$ as promised. 
        It is then clear that~$\calC''_n$ is $\calC_2'$, because we have~$|\calC''_n \setminus \calC'_2| = 0$ by condition (2), and~$|\calC''_n| = |\calC'_2|$ by construction.
        This concludes the
        proof.
\end{proof}


\subsection{Lifting lemma and simulating adjacent pairs with principal downsets}
  \label{subsec:lifting-and-simul}

  A simple, yet important component of the proof will be what we
  call the \emph{lifting lemma}. We first state and prove it before
  giving intuition.

  \begin{lemma}[Lifting lemma]
    \label{lem:lifting}
    Let~$Z \in \bbmB_S$ and $\calA \subseteq \frakF_{\bbmB_S}(Z)$. Let
    $I_\calA \defeq \{\frakI_{\bbmB_S}(X) \cap \frakF_{\bbmB_S}(Z) \mid X \in \calA\}$
    and $I'_\calA \defeq \{\frakI_{\bbmB_S}(X)\mid X \in \calA\}$. Then, for any
    $\calC \in \bullet(I_\calA)$, letting~$\lift_{\bbmB_S,Z}(\calC)$ be the configuration
    \[\lift_{\bbmB_S,Z}(\calC) \defeq \{X \in \bbmB_S \mid X \cup Z \in \calC\},\]
    we have that~$\lift_{\bbmB_S,Z}(\calC) \in \bullet(I'_\calA)$.
  \end{lemma}
  \begin{proof}
    This is shown by induction on~$\calC \in \bullet(I_\calA)$. The
    first base case is when~$\calC = \emptyset$, but then we
    have~$\lift_{\bbmB_S,Z}(\calC) = \emptyset$ as well, which is
    in~$\bullet(I'_\calA)$ by definition. The second base case is
    when~$\calC = \frakI_{\bbmB_S}(X) \cap \frakF_{\bbmB_S}(Z)$ for
    some~$X$ in $\calA$. In this case, it is easy to show
    that~$\lift_{\bbmB_S,Z}(\calC) = \frakI_{\bbmB_S}(X)$; this
    uses in particular that set union is the join operation of the
    lattice~$\bbmB_S$. But then this implies
    $\lift_{\bbmB_S,Z}(\calC) \in \bullet(I'_\calA)$ indeed.
    For the inductive case, we focus on~$\cupdot$ as~$\minusdot$ works similarly. Let~$\calC_1,\calC_2 \in
    \bullet(I_\calA)$ such that~$\calC \defeq \calC_1 \cupdot
    \calC_2$ is well defined, and let us show
    that $\lift_{\bbmB_S,Z}(\calC) \in \bullet(\calI'_\calA)$.
    By induction hypothesis we have that
    $\lift_{\bbmB_S,Z}(\calC_1) \in \bullet(\calI'_\calA)$ and
    $\lift_{\bbmB_S,Z}(\calC_2) \in \bullet(\calI'_\calA)$. Now,
    observe that~$\lift_{\bbmB_S,Z}(\calC_1) \cupdot
    \lift_{\bbmB_S,Z}(\calC_2)$ is well defined, and furthermore
    that we have $\lift_{\bbmB_S,Z}(\calC_1 \cupdot \calC_2) =
    \lift_{\bbmB_S,Z}(\calC_1) \cupdot \lift_{\bbmB_S,Z}(\calC_2)$.
    This implies $\lift_{\bbmB_S,Z}(\calC) \in
    \bullet(\calI'_\calA)$ as wanted, and concludes the proof.
  \end{proof}

The intuition behind the lifting lemma is the following. Notice
that~$\frakF_{\bbmB_S}(Z)$ is isomorphic to the Boolean
lattice~$\bbmB_{S'}$ with~$S' \defeq S \setminus Z$. Consider a
witnessing tree~$T'$ representing a configuration~$\calC$ of
$\bbmB_{S'}$ that is formed from principal downsets of~$\bbmB_{S'}$.
Consider the “lifted tree”~$T'$ that is obtained from~$T$ by
replacing every leaf of~$T$ of the form~$\calI_{\bbmB_{S'}}(X)$
for~$X\subseteq S'$ by the principal of~$\bbmB_S$ that is
$\calI_{\bbmB_{S}}(X\cup Z)$. The lifting lemma says that the
obtained tree~$T'$ is valid (i.e., the internal nodes are well
defined disjoint unions and subset complements) and that it
represents the configuration of~$\bbmB_S$ that
is~$\lift_{\bbmB_S,Z}(\calC)$.

This lemma will have two uses in the proof of
Theorem~\ref{thm:main}. The first use is that said proof will work
by top-down induction on~$\bbmB_S$, and this lemma is what we will invoke to lift the
configurations that will have been built by induction over some
principal upset of~$\bbmB_S$ to a configuration of~$\bbmB_S$. The
second use of this lemma is that it helps us show that adjacent
pairs can be simulated by principal downsets, while avoiding the
principal downset that is at the bottom. We explain this in the
remaining of this section.

We show:

\begin{lemma}
  \label{lem:pairs-to-PIs}
  Let~$\calP = \{X, X \minusdot \{x\}\}$ be an adjacent pair
  of~$\bbmB_S$. Then we have~$\calP \in \bullet(\{
  \frakI_{\bbmB_S}(Y)  \mid \emptyset \subsetneq Y \subseteq X\})$.
\end{lemma}

In this statement, pay attention that the principal downset
$\frakI_{\bbmB_S}(\emptyset)$ is not in the base set of the
dot-algebra.\footnote{Recall that to alleviate the notation we write
$\frakI_{\bbmB_S}(\emptyset)$ to mean
$\frakI_{\bbmB_S}(\{\emptyset\})$, which is then~$\{\emptyset\}$.}
In other words, we are allowed to use any principal downset that is
generated by an element that is below~$X$ and strictly
above~$\emptyset$ in~$\bbmB_S$. The reason is that in the proof of
Theorem~\ref{thm:main}, all of these elements will generate
non-cancelling principal downsets, whereas the “forbidden” principal
downset $\frakI_{\bbmB_S}(\emptyset)$ will have Möbius value zero. We now
prove Lemma~\ref{lem:pairs-to-PIs}.
\begin{proof}[Proof of Lemma~\ref{lem:pairs-to-PIs}]
  Clearly, it suffices to show the claim in the case that~$X = S$.
  To this end, it is enough to show that we have~$\bbmB_S \minusdot
  \calP \in \bullet(\{ \frakI_{\bbmB_S}(Y)  \mid \emptyset
  \subsetneq Y \subseteq X\})$: indeed we can then obtain~$\calP$
  as $\frakI_{\bbmB_S}(S) \minusdot (\bbmB_S \minusdot \calP)$.
  Let~$S' \defeq S \setminus \{x\}$, and consider the
  configuration~$\calC'$ of~$\bbmB_{S'}$ defined by~$\calC' = \{X
  \in \bbmB_{S'} \mid X \neq S'\}$.
  Then by Lemma~\ref{lem:allreach} we have that $\calC' \in
  \bullet(\{ \frakI_{\bbmB_{S'}}(Y) \mid Y \in \bbmB_{S'}, Y \neq
  S'\})$. Now, letting~$\calC'' \defeq \{Y \in \bbmB_S \mid x \in Y
  \}$, this implies that~$\calC'' \in \bullet(\{
  \frakI_{\bbmB_{S}}(Y) \cap \frakI_{\bbmB_{S}}(\{x\}) \mid \{x\}
\subseteq Y \subseteq S\})$. We now use the lifting lemma (invoked with~$Z = \{x\}$) to
obtain that~$\lift_{\bbmB_S, \{x\}}(\calC'') \in 
\bullet(\{ \frakI_{\bbmB_S}(Y)  \mid \{x\}
  \subseteq Y \subseteq S\})$, hence in particular
$\lift_{\bbmB_S, \{x\}}(\calC'') \in 
\bullet(\{ \frakI_{\bbmB_S}(Y)  \mid \emptyset
  \subsetneq Y \subseteq S\})$.
  But notice that~$\lift_{\bbmB_S, \{x\}}(\calC'')$ is actually
  $\bbmB_S \setminus \calP$, which concludes the proof.
\end{proof}


\subsection{Proof of Theorem~\ref{thm:main}}
  \label{subsec:proof}

  We now prove Theorem~\ref{thm:main}. Fix the finite set~$S$ and the
  configuration~$\calI$ that is a $2$-decomposable downset of~$\bbmB_S$. 
  For an element~$X \in \bbmB_S$, let~$I_X \defeq \{\frakI_{\bbmB_S}(Y) \cap
  \frakF_{\bbmB_S}(X) \mid Y \in \frakF_{\bbmB_S}(X), \muhat_{\bbmB_S,\calI}(Y) \neq 0 \}$.
  We will show the following stronger property: ($\dagger$)
  for every~$X \in \bbmB_S$, we have~$\calI \cap \frakF_{\bbmB_S}(X) \in
  \bullet(I_X)$. Applying this claim to~$X = \emptyset$ indeed yields the desired result.

  Property ($\dagger$) is true when~$X \notin \calI$, because in this case we
  have $\calI \cap \frakF_{\bbmB_S}(X) = \emptyset$, which is then in
  $\bullet(I_X)$ by definition of~$\bullet$. Next we show that~($\dagger$) is
  true for all elements~$X$ of~$\calI$, by top-down induction on~$\calI$.

  The base case, when~$X$ is a maximal element of~$\calI$, is trivial.
  For the inductive case, when~$X$ is not a maximal element of~$\calI$, we distinguish two cases, depending on
  whether~$\muhat_{\bbmB_S,\calI}(X)$ is zero or not.

  \begin{description}
    \item[CASE I.] We have~$\muhat_{\bbmB_S,\calI}(X) = 0$. We distinguish
      again three subcases, depending on the size of~$\ntcz_{\bbmB_S,\calI}(X)$,
      which can only be~$0$, $1$ or $2$ because~$\calI$ is $2$-decomposable.
      \begin{description}
        \item[SUBCASE I.A] We have~$\ntcz_{\bbmB_S, \calI}(X) = \emptyset$.
          This implies~$\muhat_{\bbmB_S,\calI}(Y) \neq 0$ for every~$Y\in
          \calI$, $Y\supsetneq X$. Define~$S' \defeq S \setminus X$ and~$\calI'
          \defeq \{Y \setminus X \mid Y \in \calI \cap \frakF_{\bbmB_S}(X)\}$.
          Notice that $\eul(\calI') = 0 = \eul(\emptyset)$, and that~$\calI'$
          is a connected subset of~$\bbmB_{S'}$. Therefore, by
          Proposition~\ref{prp:eul-equiv} we have $\calI'
          \simeq_{\ap_{\bbmB_{S'}}(\calI')} \emptyset$. We now use
          Lemma~\ref{lem:pairs-to-PIs} to replace these adjacent pairs by
          expressions formed with disjoint unions and subset complements of the
          relevant principal downsets and obtain that~$\calI' \in
          \bullet(\{\frakI_{\bbmB_{S'}}(Y) \mid Y \in
          \calI', Y \neq \emptyset \})$. This implies $\calI \cap \frakF_{\bbmB_S}(X) \in
          \bullet(I_X)$.

        \item[SUBCASE I.B] We have~$\ntcz_{\bbmB_S, \calI}(X) = \{Z\}$ (for~$X
          \subsetneq Z \subseteq S$). By induction hypothesis applied on~$Z$ we
          have that $\calI \cap \frakF_{\bbmB_S}(Z) \in \bullet(I_Z)$. Denote
          this configuration by~$\calC$, and let~$\calC'$ the configuration that we
          obtain when we lift~$\calC$ down to~$X$ (formally, we use that the
          sublattice~$\{X \subseteq Y \subseteq S\}$ is isomorphic
          to~$\bbmB_{S'}$).
          Define~$\calG \defeq \{(\calI \cap \frakF_{\bbmB_S}(X)) \setminus
          \frakF_{\bbmB_S}(Z)  \}$ and observe that it is a connected subset of~$\bbmB_S$. 
          Observe furthermore that~$\eul(\calC' \cap \calG) = 0 = \eul(\calI \cap \calG)$, which can be established using the linearity of the Euler characteristic, Proposition~\ref{prp:mob-eul} and the fact that~$\muhat_{\bbmB_S,\calI}(Z) = 0$.
          We can combine again Proposition~\ref{prp:eul-equiv} and
          Lemma~\ref{lem:pairs-to-PIs} to obtain that~$\calI \cap
          \frakF_{\bbmB_S}(X) \in \bullet(I_X)$.
        \item[SUBCASE I.C] We have~$\ntcz_{\bbmB_S, \calI}(X) = \{Z_1,Z_2\}$ (for~$X \subsetneq Z_1,Z_2 \subseteq S$ incomparable).
          We again distinguish two subcases, depending on whether~$Z_1 \cup Z_2$ is in~$\calI$ or not.
          \begin{description}
            \item[SUBCASE I.C.1] We have~$Z_1 \cup Z_2 \notin \calI$.
              Define~$\calG \defeq (\calI \cap \frakF_{\bbmB_S}(X)) \setminus
              (\frakF_{\bbmB_S}(Z_1) \cup \frakF_{\bbmB_S}(Z_2))$, and notice
              that~$\calG$ is a connected subset of~$\bbmB_S$. By induction
              hypothesis we have~$\calI \cap \frakF_{\bbmB_S}(Z_1) \in
              \bullet(I_{Z_1})$ and $\calI \cap \frakF_{\bbmB_S}(Z_2) \in
              \bullet(I_{Z_2})$. Call~$\calC_1'$ and~$\calC_2'$ the
              configurations we obtain by lifting these down to~$X$. 
              In particular we have $\calC_1' = \frakF_{\bbmB_S}(X) \cap \frakI_{\bbmB_S}(\calI \cap \frakF_{\bbmB_S}(Z_1))$
              and $\calC_2' = \frakF_{\bbmB_S}(X) \cap \frakI_{\bbmB_S}(\calI \cap \frakF_{\bbmB_S}(Z_2))$.
              Notice
              that~$\eul(\calC_1' \cap \calG) = 0$ by linearity of Euler
              characteristic and because~$\muhat_{\bbmB_S,\calI}(Z_1) = 0$ and
              similarly $\eul(\calC_1' \cap \calG) = 0$. Letting
              then~$\calC_1'' \defeq \calC_1' \setminus \calG$ and $\calC_2''
              \defeq \calC_2' \setminus \calG$, we have that~$\calC_1'' \in
              \bullet(I_X)$ and $\calC_1'' \in \bullet(I_X)$ (combining again
              Proposition~\ref{prp:eul-equiv} and
              Lemma~\ref{lem:pairs-to-PIs}), therefore~$\calC_1'' \cupdot
              \calC_2'' \in \bullet(I_X)$. But the we have $\eul((\calC_1''
              \cupdot \calC_2'') \cap \calG) = \eul(\emptyset) = 0 = \eul(\calI
              \cap \calG)$, with the last equality being again by linearity of
              Euler characteristic.  Combining once more
              Proposition~\ref{prp:eul-equiv} and Lemma~\ref{lem:pairs-to-PIs}
              we obtain~$\calI \cap \calG \in \bullet(I_X)$.
            \item[SUBCASE I.C.2] We have~$Z_{12} \defeq Z_1 \cup Z_2 \in \calI$. 
              By induction hypothesis we have~$\calC_1 \defeq \calI \cap
              \frakF_{\bbmB_S}(Z_1) \in \bullet(I_{Z_1})$ and $\calC_2 \defeq
              \calI \cap \frakF_{\bbmB_S}(Z_2) \in \bullet(I_{Z_2})$ and
              $\calC_{12} \defeq \calI \cap \frakF_{\bbmB_S}(Z_{12}) \in
              \bullet(I_{Z_{12}})$. Let now~$\calC_1'$, $\calC_2'$ and~$\calC_{12}'$ be the corresponding
              configurations when we lift those down to~$X$. In particular we have
              $\calC_1' = \frakF_{\bbmB_S}(X) \cap \frakI_{\bbmB_S}(\calI \cap \frakF_{\bbmB_S}(Z_1))$,
              $\calC_2' = \frakF_{\bbmB_S}(X) \cap \frakI_{\bbmB_S}(\calI \cap \frakF_{\bbmB_S}(Z_2))$, and
              $\calC_{12}' = \frakF_{\bbmB_S}(X) \cap \frakI_{\bbmB_S}(\calI \cap \frakF_{\bbmB_S}(Z_{12}))$.
              Then we have the~$\calC_1' \minusdot \calC_{12}' \in \bullet (I_X)$ as this subset complement is well defined.
              Now, TODO finish this

          \end{description}
      \end{description}
    \item[CASE II.] We have~$\muhat_{\bbmB_S,\calI}(X) \neq 0$. The proof
      follows the same lines but is easier since now the principal upset
      generated by~$X$ is authorized; in particular we can afford to use
      Lemma~\ref{lem:allreach} to “repair” the configuration in the connected
      subsets~$\calG$ from above. This concludes the proof of Theorem~\ref{thm:main}.
  \end{description}




