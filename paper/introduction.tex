We present in this note a conjecture on intersection lattices of set
families, which can be equivalently stated on the Boolean lattice.
The original motivation for the conjecture comes from a problem in database
theory about the existence of certain circuit representations for
probabilistic query evaluation (see~\cite{monet2020solving}), but in this note
we present the conjecture as a purely abstract claim without any database
prerequisites. A positive answer to this abstract conjecture implies a positive answer to
the database theory conjecture.

The conjecture can be understood in terms of the inclusion-exclusion formula.
Consider a family of sets $S_1, \ldots, S_n$. The inclusion-exclusion formula
can be used to express a quantity on the union $S_1 \cup \cdots \cup S_n$ (e.g.,
the cardinality, or the value of some measure) as a function of
the intersections of the $S_i$. 
%
%
%
%
In general, some of these intersections may in fact be identical: we can in
fact define the \emph{intersection lattice} of the set family to represent the
possible intersections that can be obtained. Further, we can
have \emph{cancellations}, i.e., some of the possible intersections may end up
having a coefficient of zero in the inclusion-exclusion formula, due to
cancellations. Thus, in the general case, inclusion-exclusion allows us to
express the measure of $S_1 \cup \cdots \cup S_n$ in terms of the measure of the
intersections $I_1, \ldots, I_m$ that have a non-zero coefficient.

Our conjecture asks whether, in this case, one can obtain the \emph{set} $S_1 \cup
\cdots \cup S_n$ from the intersections $I_1, \ldots, I_m$, using the set
operations of disjoint union and subset-complement. If this is true, then it
implies in particular the inclusion-exclusion formulation, provided that our
measure is additive in the sense that the measure of $S \cupdot S'$ (where~$\cupdot$ denotes disjoint union and
~$\minusdot$ subset complement, as per Definition~\ref{def:dots}) is the sum
of the measure of $S$ and of~$S'$. Obtaining
such a set expression can sometimes be done simply by re-ordering the
inclusion-expression formula: for instance, if we write $|X\cup Y| = |X| + |Y| -
|X \cap Y|$, we can reorder to $|X\cup Y| = |X| -
|X \cap Y| + |Y|$, and then express $X \cup Y = (X \minusdot (X \cap Y)) \cupdot
Y$. However, in general, it is unclear whether such an expression can be
obtained. 
Before continuing, let us give a first toy example.

\begin{example}

Consider the set family $\calF = \{S_1,S_2,S_3\}$ with  $S_1=\{a,b,d\}$, $S_2=\{a,b,c,e\}$,
$S_3=\{a,c,f\}$. Then:
\begin{align*}
  |S_1 \cup S_2 \cup S_3| &= |S_1| + |S_2| + |S_3| \\
                          &\phantom{=} - (|S_1 \cap S_2| + |S_1 \cap S_3| + |S_2 \cap S_3|) \\
                          &\phantom{=} + |S_1 \cap S_2 \cap S_3|.
\end{align*}
Since we have~$S_1 \cap S_3 = S_1 \cap S_2 \cap S_3$, we obtain
\begin{align*}
  |S_1 \cup S_2 \cup S_3| &= |\{a,b,d\}| + |\{a,b,c,e\}| + |\{a,c,f\}|- |\{a,b\}| - |\{a,c\}|.
\end{align*}
The non-cancelling intersections are the ones that remain, i.e., $\{a,b,d\}$,
$\{a,b,c,e\}$, $\{a,c,f\}$, $\{a,b\}$, and $\{a,c\}$, while $\{a\}$ ($=S_1 \cap
S_3 = S_1 \cap S_2 \cap S_3$) is a cancelling term.

Then, we can express~$S_1 \cup S_2 \cup S_3 = \{a,b,c,d,e,f\}$ with $
\big[\big((\{a,b,d\} \minusdot \{a,b\}) \cupdot \{a,b,c,e\}\big) \minusdot
\{a,c\}\big] \cupdot \{a,c,f\}$: the reader can easily check that each
$\cupdot$ (resp., each $\minusdot$) is a valid disjoint union (resp., subset
complement), and that we have only used the non-cancelling intersections. Note
that this is not the only valid expression, for instance we can also obtain the
union with the expression $[\{a,b,d\} \minusdot \{a,b\}] \cup [\{a,b,c,e\}
\minusdot \{a,c\}] \cup \{a,c,f\}$.

\end{example}

Our results imply that it is always possible when there are no cancellations
(Fact~\ref{fact:annoyingfact}), or exactly one cancellation
(in the equivalent formulation on Boolean lattices, Theorem~\ref{thm:partial}), but in general it seem challenging to do so while
avoiding those intersections which cancel out.
The goal of this note is to present the current status of our efforts in
attacking the conjecture, in particular showcasing some equivalent formulations,
presenting examples, and establishing a very partial result. After presenting
preliminaries in Section~\ref{sec:prelims}, we formally state the conjecture and
give examples in Section~\ref{sec:conj}. We show in Section~\ref{sec:tight} that
it suffices to study the conjecture on specific intersection lattices, which we
call \emph{tight}, where informally the set family ensures that every possible
intersection contains exactly one element that is ``specific'' to this
intersection (in the sense that it is present precisely in this intersection and
in larger intersections). We use this in Section~\ref{sec:alt} to give an
alternative formulation: instead of working with intersection lattices, we can
work with downsets on the Boolean lattice. We show that this is equivalent to
the original conjecture. The hope is that the setting of the Boolean lattice can
be more convenient to work with, as its structure is more restricted, and we can
define quantities such as the Euler characteristic that seem useful in
understanding the structure of the problem.

We next present in Section~\ref{sec:partial} a partial result that we can show
in the context of the Boolean lattice. In the rephrased problem, we must express
a downset of that lattice using disjoint union and subset complement on
principal downsets spanned by elements which do not ``cancel out'' in the sense
of having non-zero Möbius value. Our result establishes that the rephrased conjecture is true
when there is one single node of the downset that has such value; this non-trivially 
extends the fact that the result is true when no such nodes exist
(Lemma~\ref{lem:allreach}), but falls short of the goal as 
in general many such zeroes can occur.

We conclude in Section~\ref{sec:final} with further questions and
directions on the conjecture. We point out that the conjecture can be
strengthened, in two different ways, to restrict the shape of the disjoint-union
and subset-complement expressions: we also do not know the status of these
stronger conjectures. We briefly report on an unsuccessful experimental search
for counterexamples. We also hint at an incomplete proof of another partial
result where we can avoid  more than one zero, provided the targeted downset
satisfies a certain decomposability condition.

