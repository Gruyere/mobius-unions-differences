In this section we show that the $\nci$ conjecture 
can be studied without loss of generality on intersection lattices
satisfying a certain \emph{tightness} condition, that we define
next. Intuitively, an intersection lattice~$L$ is \emph{tight} if for
every~$U\in L$, $U\neq \onehat$, there is exactly one element~$x\in
\onehat$ such that~$\min_L(x) = U$. 
We also define the notion of an intersection lattice being
\emph{full}, which will be useful in the next section.

\begin{definition}
  \label{def:tight}
  An intersection lattice~$L$ is \emph{full} if for every~$U\in L$,~$U\neq
  \onehat$, we have~$\{x\in \onehat \mid \min_L(x) = U\} \neq \emptyset$.\footnote{Note that~$\{x\in
\onehat \mid \min_L(x) = \onehat\} = \emptyset$ because $\calF$ is
non-trivial.}
  The lattice~$L$ is in addition called \emph{tight} if for every~$U\in L$,~$U\neq
  \onehat$, we have~$|\{x\in \onehat \mid \min_L(x) = U\}| = 1$. 
  %
  %
\end{definition}



For instance, the intersection lattices~$L_2$ and~$L_4$ from
Figure~\ref{fig:int-latt} and~$L_5$ from Figure~\ref{fig:complex} are tight (hence full),
$L_1$ is full but not tight, and $L_3$ is not full (hence not
tight). Note that if an intersection lattice~$L=\bbmL_\calF$ is
tight, then~$\onehat$ is finite, i.e., the sets in~$\calF$ are
themselves finite: this can be seen by applying
Fact~\ref{fact:union-min} with~$U = \onehat$. 
We then claim that we can reformulate the $\nci$ conjecture as
follows:

\begin{conjecture}[$\nci$ conjecture, formulation II]
  \label{conj:tight}
  For every \emph{tight} intersection lattice~$L$, we have~$\onehat \in
  \bullet(\nci(L))$.
\end{conjecture}

We prove this in two steps.
First, we show that for every intersection lattice~$L$, there
exists a \emph{tight} intersection lattice~$L'$ that is isomorphic
to~$L$. 

\begin{lemma}
  \label{lem:to-tight}
  For every intersection lattice~$L$, there exists a tight
  intersection lattice~$L'$ such that~$L\simeq L'$.
\end{lemma}
\begin{proof}
  It suffices to observe that the intersection lattice constructed in
  Remark~\ref{rmk:lattices-are-intersection} (call it $L'$) is tight. 
  Indeed, letting $U\in L\setminus \{\onehat_L\}$, corresponding to $\downarrow_L(\{U\})$ in $L'$,
  we have that $\{x\in \onehat_{L'} \mid \min_{L'}(x) = \downarrow_L(\{U\})\} = \{U\}$.
\end{proof}

%
%
%
%
%
%

Second, we show that, for two isomorphic intersection
lattices~$L,L'$ such that~$L'$ is full, then~$\onehat_{L'} \in
\bullet(\nci(L'))$ implies $\onehat_L \in \bullet(\nci(L))$. 

\begin{lemma}
  \label{lem:from-full}
  Let $L,L'$ be isomorphic intersection lattices such that~$L'$ is full and such that
  $\onehat_{L'} \in \bullet(\nci(L'))$. Then we have $\onehat_L \in
  \bullet(\nci(L))$.
\end{lemma}
\begin{proof}
  Let~$h:L\to L'$ be an isomorphism. 
  Since~$L'$ is full, for~$U'\in L'$, $U' \neq \onehat_{L'}$, let
  $\alpha_{U'}$ be an element of $\{x'\in
  \onehat_{L'} \mid \min_{L'}(x') = U'\}$.
  Define~$g:\onehat_L \to \onehat_{L'}$
  by~$g(x) \defeq \alpha_{h(\min_{L}(x))}$ for~$x\in \onehat_L$. Note that~$g$
  is not necessarily injective. For~$x' \in \onehat_{L'}$ let~$g^{-1}(x')
  \defeq \{x\in \onehat_L \mid g(x) = x'\}$, and for~$X'\subseteq \onehat_{L'}$
  let $g^{-1}(X') \defeq \bigcup_{x'\in X'} g^{-1}(x')$. From
  Fact~\ref{fact:union-min} and because~$L'$ is full it is easy to see that
  ($\dagger$) for every~$U'\in L'$ we have~$g^{-1}(U') = h^{-1}(U')$. In
  particular we have~$g^{-1}(\onehat_{L'}) = \onehat_L$. Hence, it is enough to
  prove that for all~$X' \in \bullet(\nci(L'))$ we have~$g^{-1}(X')\in
  \bullet(\nci(L))$. We show this by induction on~$\nci(L')$. 
  The first base case is when~$X' = \emptyset$. But then observe
  that~$g^{-1}(X') = \emptyset$, and that~$\emptyset \in \bullet(\nci(L))$ by definition.
  The second base case is
  when~$X' \in \nci(L')$. But then~$g^{-1}(X') \in \nci(L)$ as well by
  ($\dagger$) and because~$h$ is an isomorphism so
  clearly~$\mu_{L'}(X',\onehat_{L'}) = \mu_L(h^{-1}(X'),\onehat_L)$. The first
  inductive case is when~$X' = X'_1 \cupdot X'_2$ with~$X'_1,X'_2 \in
  \bullet(\nci(L'))$. By induction hypothesis we have~$g^{-1}(X'_1),
  g^{-1}(X'_2) \in \bullet(\nci(L))$. Moreover clearly~$g^{-1}(X'_1)\cap
  g^{-1}(X'_2) = \emptyset$ since~$X'_1$ and~$X'_2$ are disjoint. So
  indeed~$g^{-1}(X') = g^{-1}(X'_1) \cupdot g^{-1}(X'_2) \in \bullet(\nci(L))$.
  The second inductive case is when~$X' = X'_1 \minusdot X'_2$ with~$X'_1,X'_2
  \in \bullet(\nci(L'))$. By induction hypothesis again we have~$g^{-1}(X'_1),
  g^{-1}(X'_2) \in \bullet(\nci(L))$. Moreover clearly~$g^{-1}(X'_2)\subseteq
  g^{-1}(X'_1)$ since~$X'_2 \subseteq X'_1$. So indeed~$g^{-1}(X') =
  g^{-1}(X'_1) \minusdot g^{-1}(X'_2) \in \bullet(\nci(L))$. This concludes the
  proof.
\end{proof}

This indeed proves that the two formulations are equivalent. We illustrate this last
construction in the following example.

\begin{example}
  Consider the intersection lattices~$L_3$ and~$L_4$ from
  Figure~\ref{fig:int-latt}, the isomorphism $h:L_3 \to L_4$ defined
  following their drawings, and~$\alpha_{U'}$ for~$U' \in L_4$~$U' \neq \onehat_{L_4}$ being the only element in $\{x' \in \onehat_{L_4} \mid \min_{L_4}(x') = U'\}$ (since~$L_4$ is tight). 
  %
  Then we have,
  for instance, $g^{-1}(d) = \{a\}$,~$g^{-1}(a) = g^{-1}(f) = \emptyset$.
  Moreover, consider the tree~$T'_1$ from Figure~\ref{fig:Tp1} witnessing
  that~$\onehat_{L_4} \in \bullet(\nci(L_4))$. Applying~$g^{-1}$ to every node
  of this tree yields the tree~$T_2$ from Figure~\ref{fig:T2} that witnesses
  $\onehat_{L_4} \in \bullet(\nci(L_4))$. By contrast, consider the
  expression $\{a\} \cupdot \{b\} \cupdot \{c\} \cupdot \{d\}$ that also
  witnesses $\onehat_{L_3} \in \bullet(\nci(L_3))$. This expression cannot be
  translated through~$h$ into a tree for~$L_4$ as, e.g., the
  operation~$\{b\} \cupdot \{c\}$ would translate to~$\{a,c\} \cupdot \{a,b\}$
  in~$L_4$, which is not valid.
\end{example}

In fact we claim that, when~$L$ is a full intersection lattice, any
tree~$T$ witnessing that~$\onehat \in \bullet(\nci(L))$ must use all
of the sets in~$\nci(L)$. (This is not the case of the expression
$\{a\} \cupdot \{b\} \cupdot \{c\} \cupdot \{d\}$ for~$L_3$ for
instance, but~$L_3$ is not full.)  
  In a sense, this means that the full
intersection lattices are the hardest cases for the $\nci$ conjecture.
We formalize and state a stronger claim (on the non-trivial
intersections instead of only the non-cancelling ones) in the
remaining of this section.

\begin{definition}
  \label{multiplicity}
  Let~$\calF$ be a set family,~$X\in \bullet(\calF)$ and~$T$ a tree
  witnessing this. For a leaf~$\ell$ of~$T$, the \emph{polarity of~$\ell$
  in~$T$}, denoted~$\pol_T(\ell)$, is $1$ if the number of times we take the
  right edge of a~$\minusdot$ node in the path from the root of~$T$ of~$\ell$
  is even; and~$-1$ otherwise. The \emph{multiplicity of~$U\in \calF \cup \{\emptyset\}$ in~$T$}
  is then
\[\mult_T(U) \defeq \sum_{\substack{\ell \text{ leaf of T}\\\text{of the form } U}} \pol_T(\ell).\]
  %
  %
  %
  %
  %
  %
%
\end{definition}

In other words, if we see each~$U\in \calF \cup \{\emptyset\}$ as a variable and
see~$T$ as an arithmetic expression over these variables (by
replacing~$\cupdot$ nodes by~$+$ and~$\minusdot$ nodes by~$-$), then
the multiplicity of~$U$ in~$T$ is the coefficient of the
corresponding variable once we develop this expression and factorize.
We then have:

\begin{propositionrep}
  \label{prp:full-multiplicity}
   Let~$L$ be a full intersection lattice such that~$\onehat \in
   \bullet(\nti(L))$, and~$T$ a tree witnessing this. Then we have~$\mult_T(U)
   = - \mu_L(U,\onehat)$ for every~$U\in \nti(L)$.
\end{propositionrep}
\begin{proof}
  For~$U\in L$, $U\neq \onehat$, let~$\alpha_U$ be an
  element of $\{x\in \onehat \mid \min_L(x) = U\}$.
  We first define, for each $X\subseteq \onehat$, a function
  $\mucheck_{L,X}:L \to \ZZ$, and state three useful properties of
  these functions. The function $\mucheck_{L,X}$ for~$X\subseteq
  \onehat$ is  defined by $\mucheck_{L,X}(\onehat) \defeq 0$, and,
  for~$U\in \nti(L)$, by~$\mucheck_{L,X}(U) \defeq
  1-\sum_{\substack{U' \in L\\ U \subsetneq U'}}
  \mucheck_{L,X}(U')$ if~$\alpha_U \in X$, and $\mucheck_{L,X}(U)
  \defeq -\sum_{\substack{U' \in L\\ U \subsetneq U'}}
  \mucheck_{L,X}(U')$ if~$\alpha_U \notin X$. 
  
  The three properties are: 
  \begin{enumerate}
    \item \label{itemzero} We have $\mucheck_{L,\emptyset}(U) = 0$ for~$U\in L$.
    \item \label{itema} For every~$U\in \nti(L)$ we
      have~$\mucheck_{L,\onehat}(U) = - \mu_L(U,\onehat)$. 
    \item \label{itemb} For~$X_1, X_2 \subseteq \onehat$ such that~$X_1 \cap X_2 =
     \emptyset$ (resp., such that~$X_2 \subseteq X_1$) we have
     $\mucheck_{L,X_1\cupdot X_2} = \mucheck_{L,X_1} +
     \mucheck_{L,X_2}$ (resp., $\mucheck_{L,X_1\minusdot X_2} =
     \mucheck_{L,X_1} - \mucheck_{L,X_2}$). 
   \item \label{itemc} For~$U\in \nti(L)$ and~$U'\in
     L$, the value $\mucheck_{L,U}(U')$ is $1$ if~$U'=U$ and~$0$
     otherwise.
  \end{enumerate}
  These can all easily be established by top-down induction on~$L$.
  Now for a node~$n \in T$, let~$X_n$ be the subset of~$\onehat$
  represented by the subtree of~$T$ rooted at~$n$. We show by
  bottom-up induction on~$T$ that for every node~$n$ of~$T$
  and~$U\in \nti(L)$ we have~$\mult_{T_n}(U) =
  \mucheck_{L,X_n}(U)$. This will indeed imply the result by applying
  it to the root of~$T$ and using item (\ref{itema}), since we then
  have that~$X_n = \onehat$. The base case, when~$n$ is a leaf
  of~$T$, follows from item (\ref{itemc}) and item (\ref{itemzero}), while the inductive
  case, when~$n$ is an internal node of~$T$, follows
  from~(\ref{itemb}). 
  %
\end{proof}

Since Proposition~\ref{prp:full-multiplicity} will not be strictly
needed to establish our main results, we defer its proof to
Appendix~\ref{apx:tight}. (We include this proposition as we
think that it is relevant to motivate the conjecture.)
Notice that Proposition~\ref{prp:full-multiplicity} can be false if we
do not impose intersection lattices to be full, see e.g., the expression
for~$L_3$ given in Example~\ref{expl:conj-expls}.

Furthermore, one can show that for any tight intersection lattice~$L$
we have~$\bullet(\nti(L)) = 2^{\onehat_L}$. In other words, intuitively, if we
use the non-trivial intersections instead of the non-canceling intersections
in the conjecture, then we can achieve all possible sets of elements:

\begin{fact}
  \label{fact:annoyingfact}
For every tight intersection lattice~$L$
we have~$\bullet(\nti(L)) = 2^{\onehat_L}$.
\end{fact}

\begin{proof}
  Generalizing from tight intersection lattices,
  we will show that the result holds more generally when we assume that for
  every $U \in L$, $U \neq \onehat$, there is \emph{at most} one element $x \in
  \onehat$ such that $\min_L(x) = U$.
  Let $L$ be an intersection lattice satisfying this weaker condition. We
  proceed by induction on the downsets of $L$, showing the following by
  induction on $0 \leq n < |L|$: for any downset $D$ of $L$ containing $n$ nodes, letting
  $X_D \subseteq \onehat$ be the set of elements introduced at nodes of~$D$,
  formally, $\{x \in \onehat \mid \min_L(x) \in D\}$, then we have $\bullet(D) =
  2^{X_D}$. Note that, when taking this claim with $n = |L|-1$, then the only
  downset $D$ of~$L$ containing $|L|-1$ nodes is the downset $D$ containing all
  intersections except $S_\emptyset$, i.e., precisely $\nti(L)$, and then $X_D =
  \onehat$, so this claim with $n = |L|-1$ is the result we want to show.

  The base case is $n = 0$, i.e., the downset $D=\emptyset$. Then~$X_D =
  \emptyset$ and $\bullet(\emptyset)=\{\emptyset\} = 2^{X_D}$ and we are done.

  For the induction case, let $D$ be a downset containing at least one node,
  let $U$ be some maximal node of~$D$, and let $D' = D \setminus \{U\}$. The
  first case is then
  there is no element introduced at~$U$, i.e., $U = \bigcup_{U' \in L, U'
  \subsetneq U} U'$, then we have $X_D = X_{D'}$, and the induction hypothesis
  gives us that $\bullet(D') = 2^{X_{D'}} = 2^{X_D}$, which concludes because
  $\bullet(D) \supseteq \bullet(D')$ (as $D' \subseteq D$)
  and clearly $\bullet(D) \subseteq 2^{X_D}$.
  Let us thus focus on the second case, where there is an element $x$ introduced
  at~$U$.

  We first show that $\{x\} \in \bullet(D)$. Indeed, by definition we have
  $U \in \bullet(D)$ because $U \in D$. 
  Further, all elements of~$U$
  except~$x$ were introduced below~$U$ and in~$D'$, and therefore 
  we have that $U \setminus \{x\} \subseteq X_{D'}$. Hence, by induction
  hypothesis, we have $U \setminus \{x\} \in \bullet(D')$, so $U \setminus \{x\}
  \in \bullet(D)$ because $D' \subseteq D$. From $U \in
  \bullet(D)$ and $U \setminus \{x\} \in \bullet(D)$ we deduce $\{x\} \in
  \bullet(D)$.

  Now let us show that $\bullet(D) = 2^{X_D}$. Pick $Y \in 2^{X_D}$. If $x
  \notin Y$, then $Y \in 2^{X_{D'}}$, so by induction hypothesis $Y \in
  \bullet(D')$, hence $Y \in \bullet(D)$, so we conclude immediately. If $x \in
  Y$, let $Y' = Y \setminus \{x\}$. We have $Y' \in 2^{X_{D'}}$, so by induction
  hypothesis we have $Y' \in \bullet(D')$ hence $Y' \in \bullet(D)$. Further, we
  have established in the previous paragraph that $\{x\} \in \bullet(D)$. We
  conclude that $Y \in \bullet(D)$. Thus, we have shown the inductive claim
  $\bullet(D) = 2^{X_D}$.
  
  We have concluded the induction, which as we explained establishes the claim.
\end{proof}

By Proposition~\ref{prp:full-multiplicity}, in a tree~$T$
which witnesses that~$\onehat \in \bullet(\nti(L))$ for a full intersection
lattice~$L$, the multiplicity of a cancelling intersection is
zero. Intuitively, we can see this as suggesting that we do not
really need such intersections, and so that the $\nci$ conjecture
should morally be true. 
In addition notice that, for a measure~$\xi$ on~$2^{\onehat}$,
applying~$\xi$ to~$T$ (and replacing~$\cupdot$ nodes by~$+$
and~$\minusdot$ nodes by~$-$) and then factoring gives back exactly
Equation~\eqref{eq:incl-excl-mob} according to
Proposition~\ref{prp:full-multiplicity}.
