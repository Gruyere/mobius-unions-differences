\section{Sum Expressions}

We describe here yet another equivalent formulation of
Conjecture~\ref{conj:general}.  The advantage of this formulation is
that it does not mention the M\"obius function.  This means that, to
prove it by, say, induction, we don't need to reason about how the
M\"obius function changes during induction.


Fix a universe $\Omega$.  A {\em sum-expression} is a function
$E:2^\Omega \rightarrow \ZZ$.  Equivalently, it is a vector
$E \in \ZZ^{2^\Omega}$.  We write $E + E'$ or $E - E'$ for the usual
operations on vectors.  For each set $A \subseteq \Omega$ we use the
same letter to denote (with some abuse) the sum-expression that has a
single 1 on position $A$.  Similarly, for each family of sets
$\calA = \set{A_1, A_2, \ldots}$ we use the same letter to denote the
sum $A_1 + A_2 + \cdots$.  For any sum-expression $E$, denote its
support by $\supp(E) = \setof{A\subseteq \Omega}{E(A) \neq 0}$, and
denote its domain by $\dom(E) = \bigcup \supp(E)$.


\begin{example}
  Assume $\Omega = \set{a,b,c}$.  We abbreviate a set $\set{a,b}$ with
  $ab$.  Sum-expressions are 8-dimensional vectors.  Some examples:
  %
  \begin{align*}
    a = & (0,1,0,0,0,0,0,0) & \supp = & \set{a} & \dom = & \set{a}\\
    ab = & (0,0,0,0,1,0,0,0,0) & \supp = & \set{ab} & \dom = & \set{a,b}\\
    abc = & (0,0,0,0,0,0,0,1) \\
    ab+bc-a = & (0,-1,0,0,1,0,1,0) & \supp= & \set{a,ab,bc} & \dom  = & \set{a,b,c} \\
    \uparrow(ab) = ab+abc = & (0,0,0,0,1,0,0,1) \\
    \uparrow(\emptyset) = & (1,1,1,1,1,1,1,1)
  \end{align*}
\end{example}

\begin{definition} \label{def:very:good}
  A sum-expression $E$ is {\em good} if:
  %
  \begin{align}
    \forall a \in \dom(E):  \ \ E \cdot \uparrow(a) = & 1 \label{eq:max:principal:filter}  \\
    \forall A \in \downarrow(\supp(E)),\ \  A \neq\emptyset: \ \ E \cdot \uparrow(A) \geq & 1\nonumber
  \end{align}
  Here $\cdot$ means the dot product.  $E$ is called {\em very-good}
  if it satisfies:
  \begin{align}
        \forall A \in \downarrow(\supp(E)) \ \ E \cdot \uparrow(A) = & 1 \label{eq:principal:filter}
  \end{align}
\end{definition}

\dan{I don't like the names good, very good; they are temporary.}

Notice that, when $a\not\in\dom(E)$, or
$A \not\in\downarrow(\supp(E))$, then all dot-products above are $=0$.
If $E$ is very good, then condition~\eqref{eq:max:principal:filter} is
a special case of~\eqref{eq:principal:filter}, because $a \in \dom(E)$
is the same as saying that the singleton set $a$ is in the ideal
$\downarrow(\supp(E))$.

One way to think about a sum-expression $E$ is as an expression
computing a subset of $\Omega$.  But in general it is not a standard
subset, but it is a $\ZZ$-subset of $\Omega$, which we denote by
$[[E]]: \Omega \rightarrow \ZZ$, and define as follows:
%
\begin{align*}
\forall a \in \Omega: \ \  [[E]](a) = & E\cdot \uparrow(a)
\end{align*}
%
For example, if $E = ab+bc-b$ then $[[E]] = \set{a,b,c}$ which is a
standard set, and if $E = ab+bc-d$ then $[[E]] = \set{a,2b,c,-d}$.
Condition~\eqref{eq:max:principal:filter} says that $[[E]]$ is a
standard set, and $[[E]] = \dom(E)$.

\begin{conjecture} \label{conj:sum-expressions} If
  $E$ is very good, then $\dom(E) \in \bullet(\supp(E))$.
\end{conjecture}

\begin{conjecture}[Strong version] \label{conj:sum-expressions:strong}
  If $E$ is good, then $\dom(E) \in \bullet(\supp(E))$.
\end{conjecture}

Conjecture~\ref{conj:general} is equivalent to
Conjecture~\ref{conj:sum-expressions} (see below), but my guess is
that, in order to prove Conjecture~\ref{conj:sum-expressions}, we need
to prove the stronger version,
Conjecture~\ref{conj:sum-expressions:strong}.

\begin{example} \label{ex:good:and:not:good}
  (1) The tight version of the $Q9$ intersection lattice is a
  very-good sum-expression:
  %
  \begin{align*}
    E = & abxw + bcyw + aczw - aw - bw - cw + dw \\
    \supp(E) = & \set{abxw,bcyw,\ldots} \\
    \dom(E) = & \set{a,b,c,d,x,y,z,w}
  \end{align*}
  %
  The fact that $E$ is very-good follows from the proof of
  Lemma~\ref{lemma:general:vs:sum-exprssions}.  Here we check only a
  few conditions:
   %
   \begin{align*}
     E \cdot \uparrow(a) = & (abxw + aczw - aw) \cdot \uparrow(a) = 1+1-1=1\\
     E \cdot \uparrow(ac) = & (aczw) \cdot \uparrow(ac) = 1\\
     E \cdot \uparrow(xy) = & 0 \\
     E \cdot \uparrow(\emptyset) = & 1+1+1-1-1-1+1 = 1
   \end{align*}
   %
   Notice that $xy \not\in \downarrow(\supp(E))$.  We already know
   that $abcdxyzw \in \bullet(\supp(E))$, hence the conjecture holds
   here.

   (2) A ``good'' expression which is not ``very-good'' is the
   following:
   %
   \begin{align*}
     E = & ab + c +def
   \end{align*}
   %
   Obviously, $abcdef \in \bullet(\set{ab,c,def})$, hence the
   conjecture holds here trivially.

   (3) This expression is not good:
   %
   \begin{align*}
     E = & abx+bcy+acz - abc
   \end{align*}
   %
   While
   $E\cdot \uparrow(a) = E\cdot \uparrow(b) = E\cdot\uparrow(c) = 1$,
   we have
   $E\cdot \uparrow(ab) = E\cdot \uparrow(bc) = E\cdot\uparrow(ac) =
   0$, and (worse) $E \cdot \uparrow(abc) = -1$; thus, $E$ is not
   good.  And, indeed, the conjecture fails,
   $abcxyz \not\in \bullet(\set{abx,bcy,acz,abc})$.

   (4) Another example that comes from an intersection lattice:
   %
   \begin{align*}
     E = & aw + bw + cw - 2w
   \end{align*}
   %
   $E$ is very-good, and illustrates a coefficient other than $\pm 1$.
\end{example}


\begin{lemma} \label{lemma:general:vs:sum-exprssions}
  Conjecture~\ref{conj:sum-expressions} is equivalent to
  Conjecture~\ref{conj:general}.
\end{lemma}

Before we prove it, we establish a simple and useful property:

\begin{fact} \label{fact:principal:filters:to:eq}
  Let $E, E'$ be two sum-expressions such that, for every
  $A \subseteq \Omega$, $E \cdot \uparrow(A) = E'\cdot \uparrow(A)$.
  Then $E = E'$.
\end{fact}

\begin{proof}
  We prove by downwards induction on $|A|$ that $E(A)=E'(A)$ for all
  $A \subseteq \Omega$.  We start with $A = \Omega$:
  %
  \begin{align*}
    E(\Omega) = & E\cdot \uparrow(\Omega) = E'\cdot \uparrow(\Omega) = E'(\Omega)
  \end{align*}
  %
  For the induction step, we observe that:
  %
  \begin{align*}
    E\cdot \uparrow(A) = & \sum_{B: A\subseteq B \subseteq \Omega}E(B)\\
    E'\cdot \uparrow(A) = & \sum_{B: A\subseteq B \subseteq \Omega}E'(B)\\
  \end{align*}
  %
  Since $E\cdot \uparrow(A)=E'\cdot \uparrow(A)$ and $E(B)=E'(B)$ for
  all $B \supsetneq A$, it follows that we have $E(A)=E'(A)$.
\end{proof}

\begin{proof} (of Lemma~\ref{lemma:general:vs:sum-exprssions})
  We start by showing that Conjecture~\ref{conj:sum-expressions}
  implies Conjecture~\ref{conj:general}.  Let $L$ be an intersection
  lattice.  More precisely, we have a domain $\Omega$, a family of
  sets $\calF \subseteq 2^\Omega$ closed under intersection, and
  $(L,\leq) \simeq (\calF, \subseteq)$.  Notice that
  $\hat 1 = \Omega = \bigcup \calF$.  Define the following
  sum-expression $E : 2^\Omega \rightarrow \ZZ$:
  %
  \begin{align}
    E(A) = &
             \begin{cases}
               -\mu_L(A,\hat 1) & \mbox{ if } A \in L-\set{\hat 1}\\
               0 & \mbox{otherwise}
             \end{cases} \label{eq:mobius:to:sum-expression}
  \end{align}
  %
  Notice that $\supp(E) = NCI(L)$, and $\dom(E) = \Omega$.

  We prove that $E$ is very-good.  For that we need to compute
  $E \cdot \uparrow(A)$ for some $A \in \downarrow(\supp(E))$.
  %
  \begin{align}
    E\cdot \uparrow(A) = & \sum_{B: A \subseteq B \subseteq \Omega}E(B) = - \sum_{B \in L: A \subseteq B \subsetneq \Omega}\mu(B,\hat 1) \label{eq:sum:a:b:omega}
  \end{align}
  %
  Since $A \in \downarrow(\supp(E)) = \downarrow(NCI(L))$, we
  have that $A$ is a subset of some $B \in NCI(L)$, which means that
  the sum above is non-empty.  In general, $A$ need not belong to
  $NCI(L)$, and in fact it may not even belong to $L$.  Let
  $\bar A = \bigcap \setof{B \in L}{A \subseteq B}$.  Then
  $\bar A \in L$, and we obtain:
  %
  \begin{align*}
    E\cdot \uparrow(A) = & - \sum_{B \in L: \bar A \subseteq B \subsetneq \Omega}\mu(B,\hat 1)
= 1 - \sum_{B \in L: \bar A \subseteq B \subset \Omega}\mu(B,\hat 1) = 1
  \end{align*}
  %
  because $\mu(\Omega,\hat 1) = \mu(\hat 1, \hat 1) = 1$, and
  $\sum_{B: \bar A \subseteq B \subseteq \Omega} \mu(B,\hat 1) = 0$.
  Also notice (for later use) that, when
  $A \not\in \downarrow(\supp)(E)$, then the summation
  in~\eqref{eq:sum:a:b:omega} is empty (since there is no $B \in L$
  that contains $A$), therefore $E \cdot \uparrow(A) = 0$.

  This proves that $E$ is very-good.  Assuming
  Conjecture~\ref{conj:sum-expressions}, we have
  $\dom(E) \in \bullet(\supp(E))$, which is the same as saying
  $\hat 1 \in \bullet(NCI(L))$.

  It remains to show that that Conjecture~\ref{conj:general} implies
  Conjecture~\ref{conj:sum-expressions}.  Let $E_0$ be a very-good
  sum-expression, define $\Omega = \dom(E_0)$, $\calF = \supp(E_0)$.
  Let $L$ be the intersection lattice generated by $\calF$, and let
  $E$ be the sum-expression~\eqref{eq:mobius:to:sum-expression}
  defined using the M\"obius function of $L$.  We claim that
  $E_0 = E$.  Then, assuming Conjecture~\ref{conj:general}, we have
  $\hat 1 \in \bullet(NCI(L))$, or, equivalently,
  $\dom(E_0) = \dom(E) \in \bullet(\supp(E)) = \bullet (\supp(E_0))$.
  It remains to prove the claim $E_0 = E$, and for that we will prove
  the following:
  \begin{align}
    A \subseteq \Omega, \ \ E_0 \cdot \downarrow (A) = & E\cdot \downarrow (A) \label{eq:e0:eq:e}
  \end{align}
  %
  then use Fact~\ref{fact:principal:filters:to:eq}.  Since we have
  already shown that $E$ is also very-good, to
  prove~\eqref{eq:e0:eq:e} it suffices to show that
  $\downarrow(\supp(E_0)) = \downarrow(\supp(E))$ (because then, for
  each $A$, both sides of~\eqref{eq:e0:eq:e} are either 0 or 1).  This
  follows from $\downarrow(\supp(E_0)) = \downarrow(L-\set{\hat 1})$
  (by construction of $L$), $=\downarrow(NCI(L))$ (since each coatom
  of $L$ is in $NCI(L)$) $=\downarrow(\supp(E))$ (by the argument
  above).
\end{proof}

\textbf{Towards a proof} A proof could start as follows. Let $E$ be a
good expression.  Assume that there exists two sets $A \subset B$ such
that $E(A)>0$ and $E(B)<0$.  That is, we have a MINUS above a PLUS.
Define $C = B \minusdot A$, and let $E'$ be $E' = E + B - A - C$.  The
following picture suggests the change:
%
\begin{align*}
  \begin{array}[c]{c}
\Tree[.B A C ]
  \end{array}
  = &
      \begin{array}[c]{c}
  \Tree [.-1 +1 0 ]
      \end{array} &
      \Longrightarrow &
      \begin{array}[c]{c}
      \Tree [.0 0 -1 ]
      \end{array}
                        =
  \begin{array}[c]{c}
\Tree[.B' A' C' ]
  \end{array}
\end{align*}
%
Intuitively, we moved the MINUS down from $B$ to $C$ and removed the
PLUS from $A$.  It is easy to check that, if $E$ is good, then $E'$ is
good.  Notice that $E'$ cannot be very-good, for two reasons.
$E'\cdot \uparrow(\emptyset)$ strictly decreases, so it may become
$<0$.  And for several sets $X$, $E'\cdot \uparrow(X)$ may strictly
increase, for example
$E' \cdot \uparrow(B) = 1+E\cdot\uparrow(B) \geq 2$.  Assuming the
conjecture holds for $E'$, it also holds for $E$, because we can take
the dot-algbra expression for $E'$ and substitute $C$ with
$B\minusdot A$.  Thus, we make progress as long as there is a MINUS
over a PLUS. It remains to consider the case when $E$ is good, and no
MINUS occurs over a PLUS.  This seems to be the ``easy'' case, but I'm
stuck.  To give a sense of how such an expression may look like,
consider:
%
\begin{align*}
  E = & abcde - a - b - c - d + ad + bc
\end{align*}
%
$E$ is ``good'' but not ``very-good'', and has no MINUS over a PLUS.
Notice: $E \cdot \uparrow(\emptyset) = -1$.  One idea to handle such
$E$'s is to match each MINUS term to a PLUS term, and then we are
done, but obviously this fails in this example since there are 4 MINUS
terms and only 3 PLUS terms.  Still, this example is easy to write in
the dot-algebra
$E = ((((abcde - \minusdot a) \minusdot b) \minusdot c) \minusdot d)
\cupdot (ad \cupdot bc)$.


\textbf{Is ``good'' the right definition?}  It's unclear whether the
definition of ``good'' expression is the right one.  Ideally, we would
like to have the following equivalence: $E$ is ``good'' iff it
corresponds to a correct dot-algebra expression, by rearranging terms.
But this doesn't hold.  For example, the following is a correct
dot-algebra expression:
%
\begin{align*}
  E = & ((a \cupdot b) \minusdot ab) \cupdot a \cupdot b
\end{align*}
%
and it computes $\set{a,b}$.  But the associated sum-expressions is
not good:
%
\begin{align*}
  E = & 2a + 2b -ab
\end{align*}
%
It still computes $[[E]] = \set{a,b}$, but it is not good because
$E \cdot \uparrow(ab) = -1$.  Is this a problem?  Do we need to change
the definition of ``good''?  I don't know.  We cannot just drop the
condition $E \cdot \uparrow(A) \geq 0$, because then
Example~\ref{ex:good:and:not:good}(3) would be a ``good'' expression,
but it cannot be expressed in the dot-algebra.  It's unclear if we
need to change the definition: if we can prove
Conjecture~\ref{conj:sum-expressions:strong} with the current
definition of ``good'', this implies
Conjecture~\ref{conj:sum-expressions} and that's all we need.


\subsection{The Need for More Powerful Dot-Algebra Rewrite Steps}

Suppose we relax the definition of ``very-good'' as follows.  Call a
sum-expression $E$ ``good'' if it satisfies:

\begin{align}
  \forall a \in \Omega: && \uparrow(a) \cdot E = & 1 \label{eq:good:1} \\
  \forall S \in \supp(E): && \uparrow(S) \cdot E \geq & 0\label{eq:good:2} \\
  \forall A, B, E(A)>0, E(B)<0: && A \not\subseteq B \label{eq:good:3}
\end{align}

Condition~\eqref{eq:good:1} is easy to justify: it always holds during
the rewrites that we consider below.  Condition~\eqref{eq:good:2}
relaxes $\uparrow(S) \cdot E$ to $\geq 0$ from $\geq 1$.  We cannot
completely drop it, as the following conterexample shows (it
satisfies~\eqref{eq:good:1} and~\eqref{eq:good:3}, but it is not
expressible in the dot-algebra):
%
\begin{align*}
  E = & abc + cde + efa - ace
\end{align*}
%
Also notice that Condition~\eqref{eq:good:2} only checks the sets $S$
in the support of $E$, and, by Condition~\eqref{eq:good:3}, we only
need to check it when $E(S)<0$.  This is enough, because all the
rewrite rules we consider below will remove negative terms, never
introduce new negative terms.  The last condition says that no MINUS
term occurs above a PLUS.  We have seen that, if we start with a
``very good'' expression (Def.~\ref{def:very:good}), then we can
repeatedly rewrite $A - B$ to $- (B \minusdot A)$ whenever
$E(A)>0, E(B)<0$ and $A \subseteq B$, and enforce all three conditions
of ``goodness''.  Thus, the ``good'' definition here seems to be quite
reasonable.

Define a rewrite step to be $A - B \rightarrow (A \minusdot B)$, when
$B \subseteq A$.\footnote{When we write $A-B$ we assume here that
  $E(A)>0$, $E(B)<0$ and the we only apply the rewrite to 1 copy of
  each.  For example
  $3abc - 5bc = 2abc-4bc +(abc-bc) \rightarrow 2abc-4bc+a$.}  In other
words, a rewrite step applies only a dot-minus operation, and converts
an expression $E$ to $E'$, in notation $E \rightarrow E'$.  We want to
prove that there is a sequence of rewrite steps
$E \stackrel{*}{\rightarrow} E'$ where all intermediate steps are
good, and $E'$ has no negative terms: this would prove
Conjecture~\ref{conj:sum-expressions}.  I will drop the $*$ and simply
write $E \rightarrow E'$ for $E \stackrel{*}{\rightarrow} E'$.  Notice
that, for now, we do not allow a rewrite step of the form
$A + B \rightarrow A \cupdot B$, although the conclusion of this
section is that some limited application of $\cupdot$ is needed.  So
the question is:

\textbf{Question}: given a ``good'' expression $E$, does there exists
a rewriting $E \rightarrow E'$ where each intermediate expression is
good, and $E'$ has no negative terms?

The following example shows that this is not possible:
%
\begin{align*}
  E = & abxm + cdxn - ax - bx - cx - dx + ac + bd + xu + xv + xw
\end{align*}
%
One can check that $E$ is ``good'':
%
\begin{align*}
  \uparrow(ax)\cdot E = & \uparrow (bx) \cdot E = \uparrow (cx)\cdot E = \uparrow (dx)\cdot E = 0
\end{align*}
%
and for all other sets $S \in \supp(E)$, we have
$\uparrow(S) \cdot E \geq 1$.  We will show that it is impossible to
rewrite $E \rightarrow E'$, where all intermediate expressions are
good and $E'$ has only positive terms.  However, if we use a limited
application of dot-union combined with dot-minus, then such a
rewriting is possible.

To prove this, note that the first rewrite step must be the following
(there are three other symmetric options, omitted):
%
\begin{align*}
  (abxm - ax) \rightarrow   (abxm \minusdot ax) = & bm
\end{align*}
%
Thus, we rewrite $E \rightarrow E'$ where:
%
\begin{align*}
  E' = & bm + cdxn - bx - cx - dx + ac + bd + xu + xv + xw
\end{align*}
%
$E'$ is no longer good, because $\uparrow(bx)\cdot E'=-1$.  We can
still continue, by rewriting $cdxn \minusdot cx = dn$.  More
precisely, the first two rewrite steps will be $E \rightarrow E_2$,
where:
%
\begin{align*}
  E = & (abxm-ax) + (cdxn - cx) - bx - dx + ac + bd + xu + xv + xw\\
  \rightarrow E_2  = & bm + dn - bx - dx + ac + bd + xu +xv + xw
\end{align*}
%
$E_2$ is also not good (but I like it more than $E'$ because we
restored symmetry).  Since we allow only dot-minus rules, we are stuck
at $E_2$: no more rewriting is possible.  To continue with $E_2$, we
need to also allow dot-union rules, then we can apply the following
rewrite steps:
%
\begin{align*}
  bm \cupdot xu = & bmxu & dn \cupdot xv = & dnxv
\end{align*}
%
We obtain the following sequence of expressions:
%
\begin{align*}
  E_2 =  & (bm+xu) + (dn+xv) - bx - dx + ac + bd + xw\\
  \rightarrow E_3  = & bxmu + dnxv - bx - dx + ac + bd + xw\\
  =  & (bxmu - bx) + (dnxv - dx) + ac + bd + xw \\
  \rightarrow E_4 = & mu + nv + ac + bd + xw \\
  \rightarrow E_5 = & abcdmnxuvw
\end{align*}

We succeeded writing $E$ into $abcdmnxuvw$ using only dot-algebra
rewrite steps, but the intermediate expression $E_2$ is not good.

One possibility is to allow the unrestricted use of both dot-minus and
dot-union rewrite rules; I haven't thought about that.  Another
possibility is to allow a rewrite rule consisting of a dot-minus
followed by a dot-union.  More precisely, if
$A \cap B \subseteq C \subseteq A$ holds, then we rewrite:
%
\begin{align}
  A+B - C \rightarrow & (A \minusdot C) \cupdot B \label{eq:stronger}
\end{align}
%
This is a correct dot-algebra expression.  We consider this a single
rule.

In our example, rule~\eqref{eq:stronger} becomes:
%
\begin{align*}
  abxm - ax + xu \rightarrow & bxmu & cdxn - cx + xv \rightarrow & dnxv 
\end{align*}
%
which allows us to rewrite $E$ directly to $E_3$.

The conjecture becomes: if $E$ is ``good'', then we can rewrite it
$E \rightarrow E'$ where each rewrite step is a dot-minus
or~\eqref{eq:stronger}, and $E'$ has only positive terms.
