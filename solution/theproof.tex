We show that when we have a template of principal filters in a principal
ideal~$\calI(\{z\})$ of~$\calB_S$, then we can extend it to a template
of~$\calB_S$. We call this \emph{lifting}.
\begin{proposition}
\label{prp:lifting}
Let~$z \in \calB_S$, $A \subseteq \calB_z$ and~$T'$ a~$\{\calF_{z}(\{x\}) \mid x \in A\}$-template
of~$\calB_{z}$. Then replacing every leaf of~$T'$ of the form~$E(\calF_{z}(\{x\}))$
(hence for~$x\in \calB_{z}$) by~$E(\calF_{S}(\{x\}))$ yields a valid
$\{\calF_{S}(\{x\}) \mid x \in A\}$-template of~$\calB_S$ (i.e., the $\oplus$
and~$\ominus$ operations are indeed disjoint in~$\calB_S$).  
We say that~$T$ is the result of \emph{lifting}~$T'$ to~$\calB_S$.
Moreover
if~$\mathbf{s'}$ is the configuration of~$\calB_{z}$ represented by~$T'$, then
the configuration~$\mathbf{s}$ of $\calB_S$ represented by~$T$ is given by
\[\mathbf{s} = \{x \in \calB_S \mid x \meet z \in \mathbf{s'}\}.\]
\end{proposition}
\begin{proof}
We easily show by induction of~$T'$ that, for every node~$t$ of~$T'$,
letting~$T_t$ be the template obtained from~$T'_t$ using the operation
described above, and~$\mathbf{s'}_t$ the configuration of~$\calB_z$ represented
by~$T'_t$, then~$T_t$ is a $\{\calF_{S}(\{x\}) \mid x \in A\}$-template of~$\calB_S$ representing $\mathbf{s}_t
\defeq \{x \in \calB_S \mid x \meet z \in \mathbf{s'}_t\}$.
(In particular, the base case of principal filters uses the fact that~$\calB_S$
is a lattice.) Taking~$t$ to be the root of~$T'$ gives the claim.
\end{proof}

Next, we explain how to build templates for adjacent pairs using principal
filters, without using the principal filter~$\{S\}$.

\begin{lemma}
\label{lem:pair_to_PF}
Let~$\mathbf{p} = \{x,x\cup \{\ell\} \}$ be an adjacent pair of~$\calB_S$
(hence with~$\ell \notin x$).  Then there exists an~$\{\calF_S(\{y\}) \mid x
\leq y < S\}$-template~$T$ of~$\calB_S$ representing~$\mathbf{p}$.  
(Crucially, notice that the template does not use the principal
filter~$\{S\}$.)
\end{lemma}
\begin{proof}
We only prove the claim when~$\mathbf{p} = \{\emptyset, \{\ell\}\}$ for
some~$\ell \in S$, as the proof for when~$\mathbf{p}$ is not of this form is
similar.  To this end, it suffices to show that there exists an
$\{\calF_S(\{y\}) \mid \emptyset \leq y < S\}$-template~$T'$ of~$\calB_S$
representing~$\calB_S \setminus \mathbf{p}$: indeed we then obtain~$T$ with~$T
\defeq E(\calF_S(\{\emptyset\})) \ominus T'$.  Let~$S_{-\ell} \defeq S
\setminus \{\ell\}$, and let~$\mathbf{s} \defeq \calB_{S_{-\ell}} \setminus
\{\emptyset\}$.  By Lemma~\ref{lem:allreach} there exists
an~$\{\calF_{S_{-\ell}}(\{x\}) \mid \emptyset < x \leq
S_{-\ell}\}$-template~$T''$ of~$\calB_{S_{-\ell}}$ representing~$\mathbf{s}$.
By Proposition~\ref{prp:lifting}, we can lift this template to an
$\{\calF_{S}(\{x\}) \mid \emptyset < x \leq S_{-\ell}\}$-template~$T'$
of~$\calB_S$ that represents~$\{x \in \calB_S \mid x\meet S_{-\ell} \in
\mathbf{s}\}$.  Observing that we have $\{x \in \calB_S \mid x\meet S_{-ell}
\in \mathbf{s}\} = \calB_S \setminus \mathbf{p}$ concludes.
\end{proof}

Using this Lemma, we can modify Corollary~\ref{cor:gen_pods} to use principal
filters instead of adjacent pairs, while ensuring that we do not use the
principal filter~$\{S\}$.

\begin{corollary}
\label{cor:simul_gen_pods}
Let~$I$ be an ideal of~$\calB_S$ and~$\mathbf{s},\mathbf{s'}$ be two configuration
of~$\calB_S$ such that $\mathbf{s} \cap I = \mathbf{s'} \cap I$
and $\eul(\mathbf{s} \cap (\calB_S \setminus I)) = \eul(\mathbf{s'} \cap
(\calB_S \setminus I))$.  Then there exists an~$(\{\calF_S(\{y\}) \mid y\in
\calB_S \setminus (I \cup \{S\}) \} \cup
\{\mathbf{s'}\})$-template of~$\calB_S$ representing~$\mathbf{s}$.
\end{corollary}
\begin{proof}
By Corollary~\ref{cor:gen_pods} there exists an~$(\ap_S(I) \cup
\{\mathbf{s'}\})$-template of~$\calB_S$ representing~$\mathbf{s}$.  For every
leaf of the form~$E(\mathbf{p})$ in~$T$ where~$\mathbf{p} = \{x,x\cup
\{\ell\}\}$ is an adjacent pair, use Lemma~\ref{lem:pair_to_PF} to replace this
leaf by a $\{\calF_S(\{y\}) \mid x \leq y < S\}$-template~$T$ of~$\calB_S$
representing~$\mathbf{p}$. The resulting template satisfies the requirements.
\end{proof}

\begin{lemma}
\label{lem:eul_is_mob}
Let~$z\in \calB_S$ with~$z \neq S$ and~$T$ an~$\{\calF_S(\{x\}) \mid x \leq
z\}$-template of~$\calB_S$ representing configuration~$\mathbf{s}$.
Then, letting~$I\defeq \calI(\{z\})$, we have~$\eul(\mathbf{s} \cap (\calB_S \setminus I)) = -\mu_\mathbf{s}(z)$.
\end{lemma}
\begin{proof}
First, observe that~$(\dagger)$ we have $\eul(\mathbf{s}) = 0$. Indeed, for
arbitrary finite sets of finite sets~$\mathbf{s}_1$ and $\mathbf{s}_2$, we
clearly have~$\eul(\mathbf{s}_1 \oplus \mathbf{s}_2) = \eul(\mathbf{s}_1) +
\eul(\mathbf{s}_2)$ (resp., $\eul(\mathbf{s}_1 \ominus \mathbf{s}_2) =
\eul(\mathbf{s}_1) - \eul(\mathbf{s}_2)$); call this the \emph{linearity of the Euler
characteristic under disjoint unions and differences}. Moreover, if~$x \leq z$, we have
that~$\eul(\calF_S(\{x\})) = 0$.  Conbining these, we can
easily show by bottom-up induction that for every node~$t$ of $T$ we
have~$\eul(\mathbf{s}_t) = 0$, hence~$\eul(\mathbf{s})=0$ indeed.

Then, observe that $\eul(\mathbf{s}) = \eul(\mathbf{s} \cap I) +
\eul(\mathbf{s} \cap (\calB_S \setminus I))$, by linearity again
because~$\mathbf{s} = (\mathbf{s} \cap I) \oplus (\mathbf{s} \cap (\calB_S
\setminus I))$.  Therefore, by~$(\dagger)$, to conclude the proof it suffices
to show that $\eul(\mathbf{s} \cap I) = \mu_\mathbf{s}(z)$.  But notice that
if~$x \leq z$, we have that~$\eul(\calF_S(\{x\})\cap I) = 1$ if~$x=z$ and~$0$
otherwise.  Using Lemma~\ref{lem:multiplicity} and the linearity of the Euler
characteristic under disjoint unions and differences gives us $\eul(\mathbf{s}
\cap I) = \mu_\mathbf{s}(z)$, which is what we wanted.
\end{proof}

We are now ready to prove Theorem~\ref{thm:main}.

\begin{proof}[Proof of Theorem~\ref{thm:main}]
Fix the filter~$F'$ of~$\calB_{S'}$.  We show by induction on~$S \leq S'$ that
there exists an~$\nzpf_{S}(F' \cap \calB_S)$-template of~$\calB_S$ that
represents~$F'\cap \calB_S$. Indeed, taking~$S=S'$ will conclude. The base
case, when~$S=\emptyset$, is simple: recall that~$2^\emptyset = \{\emptyset\}$,
and we only need to distinguish whether~$\emptyset \in F'$ or~$\emptyset \notin
F'$, either case being trivial by using the dummy template~$\top$ (that
represents~$\{\emptyset\}$) when~$\emptyset \in F'$, and using the template~$\top \ominus
\top$ (that represents~$\emptyset$) when~$\emptyset \notin F'$. 
So let~$S \leq S'$, ~$S\neq \emptyset$, and assume by induction that the claim
is true for all~$S'' < S$.  
As there will be other nested inductive proofs, let us call this assumption the
\emph{main
induction hypothesis}.
For convenience define~$F \defeq F' \cap \calB_S$.
Let~$Z'$ be the set of maximal zeros of~$\mu_F$, i.e.,
\[Z' \defeq \{z \in \calB_S \mid \mu_F(x) = 0 \text{ and } \forall z' \in \calB_S, z' > z \implies \mu_F(z')\neq 0\}.\]
If~$Z' = \emptyset$ then we are done thanks to Lemma~\ref{lem:allreach}
(because then~$A_\mathbf{F} \subseteq \nzpf_S(F)$), so
assume~$Z'$ is not empty,
and let~$M \defeq \bigvee Z'$ be the join of all elements of~$Z'$.
We distinguish two cases, depending on whether~$M<S$ or~$M=S$. We start with the easy case. 
\subparagraph*{Case $M < S$.} By the main induction hypothesis there exists
  an~$\nzpf_M(F\cap M)$-template $T''$ of~$\calB_M$ representing~$F \cap M$ (notice that~$F' \cap M = F\cap M$). Use
  Proposition~\ref{prp:lifting} to lift this template to an $\nzpf_M(F\cap
  M)$-template~$T'$ of~$\calB_S$ representing configuration~$\mathbf{s'}$
  of~$\calB_S$.  Define~$\mathbf{s}_1 \defeq \{x\in \calB_S \setminus \calB_M)
  \mid x\in F\}$, and $\mathbf{s}_2 \defeq \{x\in \calB_S \setminus \calB_M) \mid
  x\in \mathbf{s'}\}$.  Then by Lemma~\ref{lem:allreach} we deduce that there
  exist~$\{\calF_S(\{x\}) \mid x\in \calB_S \setminus \calB_M\}$-templates~$T_1$
  and~$T_2$ of~$\calB_S$ that represent~$\mathbf{s}_1$ and~$\mathbf{s}_2$. Notice
  that $\{\calF_S(\{x\}) \mid x\in \calB_S \setminus \calB_M\}\subseteq \nzpf_S(F)$
  by definition of~$M$. We then obtain a~$\nzpf_S(F)$-template~$T$ of~$\calB_S$
  representing~$F$ with~$T \defeq (T' \ominus \mathbf{s}_2) \oplus \mathbf{s}_1$, which
  concludes this case.

\subparagraph*{Case $M = S$.} Here we distinguish again two subcases; we start with the 
  most interesting.
\begin{itemize}
  \item $\mu_F(S) = 0$. Notice that this means~$Z' = \{S\}$.  
%Observe that
%    if~$\emptyset \in F$ then this is simple because the template
%    $E(\calF_S(\emptyset))$ meets our requirements, so we assume without loss of generality
%    that~$\emptyset \notin F$.  
  We first deal with the case when~$S$ is the only zero of~$\mu_F$ on~$\calB_S$.
  In this case we have that~$\eul(F) = 0$ by Proposition~\ref{prp:mob_to_eul},
  and we can then use Corollary~\ref{cor:simul_gen_pods} with~$I \defeq
  \emptyset$, $\mathbf{s} \defeq F$, $\mathbf{s'} = \emptyset$ to obtain an
  $(\{\calF_S(x)\mid \emptyset \leq x <S\} \cup \{\mathbf{s'}\})$-template~$T'$
  of~$\calB_S$ representing~$F$. We then replace in the template all leaves of
  the form~$E(\mathbf{s'})$ by the template~$\top \ominus \top$, and this gives
  us an~$\{\calF_S(x)\mid \emptyset \leq x <S\}$-template of~$\calB_S$
  representing~$F$, which is what we wanted since~$S$ is the only zero of~$\mu_F$
  on~$\calB_S$.

    So let us now assume that~$S$ is not the only zero of~$\mu_F$ on~$\calB_S$,
  and let~$Z=\{z_1,\ldots,z_k\}$ (so with~$k\geq 1$) be the maximal zeros of~$\mu_F$
  that are not~$S$. We will prove the following:
\begin{claim}
\label{clm:PIE_on_meets}
There exists an~$\nzpf_S(F)$-template~$T_1$ of~$\calB_S$ representing the
configuration~$\calF_S(\calI(Z)\cap F)$.
\todo[inline]{Warning: this claim is false! The proof that uses it is still interesting though}
\end{claim}
Before embarking on the proof of this claim, let us explain how we use it to
conclude the subcase~$\mu_F(S) = 0$.  Assume we have~$T_1$ as described in
the claim.
Our goal is to apply
Corollary~\ref{cor:simul_gen_pods} with~$\mathbf{s} \defeq F$, $I \defeq
\calI(Z)$, and~$\mathbf{s'} = \calF_S(I\cap F)$. To this end, we must first
show that the conditions of this corollary are met.
The first condition, 
$\mathbf{s} \cap I = \mathbf{s'} \cap I$, is clear.
We now check the second condition, i.e.,
$\eul(\mathbf{s} \cap (\calB_S \setminus I)) = \eul(\mathbf{s'} \cap
(\calB_S \setminus I))$. This
requires some careful
accounting.
By linearity of the Euler characteristic
and because the leaf~$E(\calF_S(\{S\}))$ does not occur in~$T_1$, we deduce 
that~$\eul(\mathbf{s'}) = 0$. Moreover, since~$\mu_F(S)=0$ by
hypothesis, we have by Proposition~\ref{prp:mob_to_eul} that~$\eul(\mathbf{s}) = 0$ as well.
But, by linearity again, and defining~$C \defeq \mathbf{s} \ominus
\mathbf{s'}$, notice that $\eul(C) = \eul(\mathbf{s})
- \eul(\mathbf{s'})$, so that in fact~$\eul(C) = 0$ by what precedes.
Now, define~$B\defeq I\cap F$ and~$D \defeq \mathbf{s'} \setminus B$.
Let~$f \defeq \eul(B)$.
Then~$\eul(\mathbf{s}') = \eul(B) + \eul(D) = 0$, so that~$\eul(D) = -f$.
But~$D = \mathbf{s'} \cap
(\calB_S \setminus I)$ so we have $\eul(\mathbf{s'} \cap
(\calB_S \setminus I))=-f$.
On the other hand, we have~$\mathbf{s} \cap
(\calB_S \setminus I) = D \oplus C$, therefore
also $\eul(\mathbf{s} \cap
(\calB_S \setminus I)) = \eul(D) + \eul(C) = -f$. Hence indeed
$\eul(\mathbf{s} \cap (\calB_S \setminus I)) = \eul(\mathbf{s'} \cap
(\calB_S \setminus I))$ and we can use Corollary~\ref{cor:simul_gen_pods} as we wanted.
We obtain
an~$(\{\calF_S(\{y\}) \mid y\in
\calB_S \setminus (I \cup \{S\}) \} \cup
\{\mathbf{s'}\})$-template~$T'$ of~$\calB_S$ representing~$\mathbf{s}$, that is,~$F$.
We now replace in~$T'$ every leaf of the form~$E(\mathbf{s'})$
by the template~$T_1$. This indeed gives us a~$\nzpf_S(F)$-template of~$\calB_S$ representing~$F$, which is what we wanted and concludes this subcase. Hence, we now prove
Claim~\ref{clm:PIE_on_meets}. 

\begin{proof}[Proof of Claim~\ref{clm:PIE_on_meets}.]
We prove by induction on~$U \subseteq Z$,~$U\neq \emptyset$, that
there exists an~$\nzpf_S(F)$-template~$T_U$ of~$\calB_S$ representing the
configuration~$\calF_S(\calI(U)\cap F)$.
\begin{itemize}
  \item The base case of this induction is when~$|U|=1$, say~$U=\{z_i\}$.
        We then easily conclude by using the main induction hypothesis with~$S' \defeq z_i$,
        and using Proposition~\ref{prp:lifting} to lift the resulting template to~$S$.
  \item Let~$U\subseteq Z$, $|U|>1$ and assume the claim is true for all~$\emptyset <U'<U$.
        We will call this hypothesis the \emph{nested induction hypothesis}.
	\todo[inline]{TODO for~$|U|=2$ we distinguish~$z_1 \meet z_2\in F$ and
$z_1 \meet z_2\notin F$, they both work but using different method... How to
generalize to arbitrary~$U$?}
\end{itemize}
\end{proof}

  \item $\mu_F(S) \neq 0$. \todo[inline]{TODO similar but less compilated: we
  can simply combine  Claim~\ref{clm:PIE_on_meets} with Lemma~\ref{lem:allreach}
  because we don't have annoying zeros above those in~$Z$.}
\end{itemize}
\end{proof}
