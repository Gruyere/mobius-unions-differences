from numpy.linalg import matrix_rank
from itertools import chain, combinations

# https://stackoverflow.com/a/43774637
def LI_vecs(dim,M):
  LI=[M[0]]
  for i in range(dim):
      tmp=[]
      for r in LI:
          tmp.append(r)
      tmp.append(M[i])                #set tmp=LI+[M[i]]
      if matrix_rank(tmp)>len(LI):    #test if M[i] is linearly independent from all (row) vectors in LI
          LI.append(M[i])             #note that matrix_rank does not need to take in a square matrix
  return LI                           #return set of linearly independent (row) vectors

# the matrix when we have k sticky blocks
def create_mat(k):
  mat = []

  # euler of each filter of sticky blocks is zero
  for i in range(k):
    row = [0]* (2**k + k)
    for part in range(1,2**k):
      if part & (1 << i):
        row[part-1] = 1
    row[2**k + i - 1] = 1
    mat.append(row)

  # euler of F is zero
  mat.append([1]*(2**k + k))

  return mat

indices = {'a': 0, 'b': 1, 'c': 2, 'd': 3}

def string_to_pos(k, myset):
  res = 0
  for char in myset:
    if char == 'r':
      res = 2**k + k 
    else:
      res += 1 << indices[char]
  return res-1

def stringset_to_eulvect(k, stringset):
  row = [0]*(2**k + k)
  for myset in stringset:
      row[string_to_pos(k,myset)] = 1
  return row
      
def create_moving_eq(k, wehave, target):
  row1 = stringset_to_eulvect(k, wehave)
  row2 =  stringset_to_eulvect(k, target)
  return [x-y for x,y in zip(row1,row2)]

# https://stackoverflow.com/a/1482316
def powersetnotempty(iterable):
  "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
  s = list(iterable)
  return chain.from_iterable(combinations(s, r) for r in range(1,len(s)+1))

k = 3
mat = create_mat(k)
print("Equations: " + str(mat))
print("The equations without the moving equation are linearly independent: " + str(len(LI_vecs(k+1, mat)) == k+1))

# k=2 
#moving_eq = create_moving_eq(k, ['a','ab'], ['a','r'])
for target in powersetnotempty(['a','b','c','ab','ac','bc','abc','r']):
  print("target: " + str(target))
  matcopy = mat.copy()
  moving_eq = create_moving_eq(k, ['a','ab','ac','abc'], target)
  matcopy.append(moving_eq)
  print("The equations with the moving equation are linearly dependent: " + str(len(LI_vecs(k+2, matcopy)) == k+1))
