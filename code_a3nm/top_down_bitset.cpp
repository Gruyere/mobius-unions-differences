#include<cstdio>
#include<unordered_map>
#include<unordered_set>
#include<bitset>
#include<map>
#include<cassert>
#include<vector>
#include<algorithm>

// use ulimit -s to increase the stack size otherwise it will segfault

// bruteforce to decompose a configuration into elementary configurations

#ifndef N
#warning "N should have been defined on the command line"
#define N 3
#endif
#define M (1 << N)

// if 1, play the cone game (the RHS of any operation must be a cone)
#ifndef ONLY_CONES
#define ONLY_CONES 0
#endif

// do not test reachability but only irreducibility
#ifndef TEST_IRRED
#define TEST_IRRED 0
#endif

// whether to display explanations
#ifndef EXPLAIN
#define EXPLAIN 1
#endif

// whether to test all configurations (instead of those provided on stdin)
#ifndef ALL
#define ALL 0
#endif

// if ALL=1, only test...
#ifndef ONLY_MONOTONE
#define ONLY_MONOTONE 0
#endif
#ifndef ONLY_MINIMAL
#define ONLY_MINIMAL 1
#endif

// smart way to prune exploration of cones, seems to work on N=5
#ifndef EXPERIMENTAL_PRUNE_CONES
#define EXPERIMENTAL_PRUNE_CONES 0
#endif

typedef unsigned long long ull;
typedef long long ll;

// interval to print progress
#ifndef STEP
#define STEP 100000000
#endif

// size after which to drop cache
#ifndef CLEAR_EVERY
#define CLEAR_EVERY 1000000
#endif

// disallow dupes in input
#ifndef DISALLOW_DUPES
#define DISALLOW_DUPES 0
#endif

using namespace std;

typedef array<ll, M> vect;
typedef bitset<M> config;

// global cache of which configurations were reached
unordered_map<config, bool> reachable_cache;
// global cache of the left operand, right operand, operation to reach a config
unordered_map<config, config> reach_l, reach_r;
unordered_map<config, char> reach_op;

// print a configuration
void print_val(config val, unsigned int n) {
  for (unsigned int i = 0; i < n; i++) {
    printf("%d", val[i] ? 1 : 0);
  }
}

// reverse binary value of n bits
ull myreverse(ull x, ull n) {
  ull v = 0;
  for (unsigned int i = 0; i < n; i++) {
    v = v << 1;
    v = v | (x & 1);
    x = x >> 1;
  }
  return v;
}

// print a mobius vector
void print_vect(vect v) {
  for (int i = 0; i < M; i++)
    printf("%d:%lld ", i, v[i]);
  printf("\n");
}

// weight of a mobius vector
ll weight(vect v) {
  ll w = 0;
  for (int i = 0; i < M; i++)
    w += v[i];
  return w;
}

// absolute weight of a mobius vector
ull absweight(vect v) {
  ull w = 0;
  for (int i = 0; i < M; i++)
    w += llabs(v[i]);
  return w;
}

// sign of a number
inline int sgn(ll x) {
  if (x < 0) return -1;
  if (x > 0) return 1;
  return 0;
}

// fill the table of how many pluses and minuses are remaining below each cell
void prepare_remaining(vect v, vect &remaining_pluses, vect &remaining_minuses) {
  for (int i = 0; i < M; i++) {
    ll plus = 0, minus = 0;
    for (int j = 0; j <= i; j++)
      if ((i | j) == i) { // j \subseteq i
        if (v[j] > 0)
          plus += v[j];
        else
          minus += -v[j]; // also covers 0 in which case nothing happens
      }
    remaining_pluses[i] = plus;
    remaining_minuses[i] = minus;
    //printf("INIT rp %d rm %d v:%d at %d\n", remaining_pluses[i], remaining_minuses[i], v[i], i);
  }
}

void apply_nullify(vect v, int i, int f, vect &remaining_pluses, vect &remaining_minuses) {
  ll w = v[i];
  //printf("remove %d to all vertices above %d\n", f*w, i);
  for (int j = i; j < M; j++)
    if ((i | j) == j) {
      //printf("before rp %d rm %d in %d\n", remaining_pluses[j], remaining_minuses[j], j);
      if (w > 0)
        remaining_pluses[j] -= f*w;
      else
        remaining_minuses[j] -= f*llabs(w);
      //printf("now rp %d rm %d in %d\n", remaining_pluses[j], remaining_minuses[j], j);
    }
}

// take into account that we made our choice for cell i
// so edit remaining_pluses and remaining_minuses to get rid of those of cell i
void nullify(vect v, int i, vect &remaining_pluses, vect &remaining_minuses) {
  apply_nullify(v, i, 1, remaining_pluses, remaining_minuses);
}
// undo nullify when backtracking
void unnullify(vect v, int i, vect &remaining_pluses, vect &remaining_minuses) {
  apply_nullify(v, i, -1, remaining_pluses, remaining_minuses);
}

// The vector clr stores the "weights assigned below each cell" (for the left or right)
// This function takes into account that we have added weight w to cell i on the
// side of the vector clr.
// So we must propagate in the cells above i in clr this new weight
// While doing so, we compute for each cell above its minimal and maximal
// weight, to identify a problem.
// If we are going to be stuck, still perform the changes in the whole vector,
// so that we can undo them more easily.
// coef swaps the pluses and minuses when doing a difference
bool add_lr(int i, ll w, vect &remaining_pluses, vect &remaining_minuses, vect &clr, bool special, bool side, bool enablecheck, int coef) {
  // in special mode, for the game with cones, check that the function of
  // everything to the left and right is OK
  // side=false: left side, we take everything
  // side=right: right side, we take nothing
  // only if enablecheck
  bool ok = true;
  for (int j = i; j < M; j++)
    if ((i | j) == j) {
      clr[j] += w;
      ll mn = clr[j] - ((coef == 1) ? remaining_minuses : remaining_pluses)[j];
      ll mx = clr[j] + ((coef == 1) ? remaining_pluses : remaining_minuses)[j];
      if (mn > 1 || mx < 0) {
        // at cell j, we will not be able to assign weights in the cells below j
        // (including j) so as to give it a weight in {0, 1}
        //printf("clr[%d] is %d but sum of rp %d and rm %d is %d\n", j, clr[j], remaining_pluses[j], remaining_minuses[j] ,remaining_pluses[j] - remaining_minuses[j]);
        ok = false;
      }
      if (EXPERIMENTAL_PRUNE_CONES && enablecheck && ONLY_CONES && special && j > i) {
        if (!side) {
          ll nv = clr[j] + ((coef == 1) ? remaining_pluses : remaining_minuses)[j] - ((coef == 1) ? remaining_minuses : remaining_pluses)[j];
          if (nv != 0 && nv != 1) {
            //printf("clr[%d] is %d but sum of rp %d and rm %d is %d\n", j, clr[j], remaining_pluses[j], remaining_minuses[j] ,remaining_pluses[j] - remaining_minuses[j]);
            //printf("BAD at %d\n", j);
            ok = false;
          }
        } else {
          if (clr[j] != 0 && clr[j] != 1) {
            //printf("clr[%d] is %d\n", j, clr[j]);
            ok = false;
          }
        }
      }
    }
  return ok;
}

// converts a vector to a config
void vect_to_config(const vect v, config &c) {
  for (int i = 0; i < M; i++) {
    c[i] = 0;
    ll w = 0;
    for (int j = 0; j <= i; j++)
      if ((i | j) == i) // j \subseteq i
        w += v[j];
    assert(w == 0 || w == 1); // the configuration should be acceptable
    if (w == 1)
      c[i] = 1;
  }
}

// computes the vector of config x and stores it in v
void config_to_vect(config x, vect &v) {
  for (unsigned int i = 0; i < M; i++) {
    ll s = 0;
    for (unsigned int j = 0; j < i; j++) {
      if ((j & i) != j)
        continue; // not subset
      // we have j subset of i
      s += v[j];
    }
    // value for i is 1 or 0 depending on whether i is lit, minus the sum
    v[i] = (x[i] ? 1 : 0) - s;
  }
}

// pre-declare reachable (mutually recursive functions)
bool reachable(vect);

// try to split the weights of vector v, at cell i
// - sgnop is the operand (+ for union, - for difference)
// - remaining_pluses and remaining_minuses store the number of remaining pluses
// and minuses below each cell (for the unaffected weights in v after cell i
// inclusive)
// - current0 current1 is the vector of the currently build left and right
// configuration (up to cell i exclusive)
// - clr0 clr1 indicates the current total weight below each cell given what was
// assigned up to cell i exclusive (to detect impossibilities)
// - one_right stores if we have already assigned something to the right (useful
// only for ONLY_CONES)
bool partition_split(const vect v, int i, int sgnop, vect &remaining_pluses, vect &remaining_minuses, vect &current0, vect &current1, vect &clr0, vect &clr1, bool one_right) {
  //printf("try split %d for sign %d\n", i, sgnop);
  if (i == M) {
    // we are done partitioning
    config cfg;
    vect_to_config(v, cfg);
    if (!absweight(current0) || !absweight(current1)) {
      return false; // trivial partition
    }
    // now recursively test if the operands are reachable
    // (or if TEST_IRRED is set we don't care about the next level)
    bool reach0 = reachable(current0);
    bool reach1 = reachable(current1);
    /*if (!reach0 || !reach1) {
      printf("BACKTRACK on a weight %d = %d+%d (reach %d %d)!!!\n", absweight(v), absweight(current0), absweight(current1), reach0?1:0, reach1?1:0);
    }*/
    //assert(reach0 && reach1);
    if (TEST_IRRED || (reach0 && reach1)) {
      if (EXPLAIN) {
        // store the operands
        reach_op[cfg] = sgnop;
        config reachlconf, reachrconf;
        vect_to_config(current0, reachlconf);
        vect_to_config(current1, reachrconf);
        reach_l[cfg] = reachlconf;
        reach_r[cfg] = reachrconf;
        assert(reach_l[cfg] != (ull) 0);
      }
      return true;
    } else {
      return false; // this partition does not work, one of the operands is unreachable
    }
  }
  // ok let's try to split the weights at cell i
  if (v[i] == 0) {
    // no weight to split, let's only make sure that the weights are legal
    if (clr0[i] != 0 && clr0[i] != 1)
      assert(false);
    if (clr1[i] != 0 && clr1[i] != 1)
      assert(false);
    // ok that's fine, so go to the next cell
    return partition_split(v, i+1, sgnop, remaining_pluses, remaining_minuses, current0, current1, clr0, clr1, one_right);
  }
  // we have weight to split at cell i, what are the possible values?
  for (ull j = 0; j <= llabs(v[i]); j++) {
    // note that only two j in this loop can give to a recursive call
    //
    ll goes_left = j*sgn(v[i]); // weight that goes left: j with the right sign
    ll goes_right = sgnop*(v[i] - goes_left); // weight that goes right: remaining weight, but depends on sgnop

    if (ONLY_CONES && one_right && goes_right)
      continue; // only cones, so we only put one thing to the right

    // sanity checks
    assert(llabs(goes_left) + llabs(goes_right) == llabs(v[i]));
    assert(goes_left + sgnop*goes_right == v[i]);

    // this will be our final cumulative values to the right and left at cell i
    // we don't need to store them, only to check them
    ll vlr_left = clr0[i] + goes_left;
    ll vlr_right = clr1[i] + goes_right;
    if (vlr_left != 0 && vlr_left != 1) {
      continue; // this weight assignment gives an unacceptable configuration to the left
    } else if (vlr_right != 0 && vlr_right != 1) {
      continue; // this weight assignment gives an unacceptable configuration to the right
    }
    //printf("try weight %d %d at %d op %d\n", goes_left, goes_right, i, sgnop);

    // ok, it is acceptable to split the weight in goes_left and goes_right
    // let's try it
    // first let's clear remaining_pluses and remaining_minuses of the weight
    nullify(v, i, remaining_pluses, remaining_minuses);
    bool reasonable = true; // check that the split has a chance of working
    // assign the weights
    current0[i] = goes_left;
    current1[i] = goes_right;
    // try to add the weight to the cells above i in clr0 and clr1
    if (!add_lr(i, current0[i], remaining_pluses, remaining_minuses, clr0, goes_right != 0, false, true, 1)) {
      reasonable = false; // our weight assignment will necessarily break some cell left
      //printf("not reasonable left\n");
    }
    if (!add_lr(i, current1[i], remaining_pluses, remaining_minuses, clr1, goes_right != 0, true, true, (sgnop==1) ? 1 : -1 )) {
      reasonable = false; // our weight assignment will necessarily break some cell right
      //printf("not reasonable right\n");
    }
    if (reasonable) { // we can try to continue the recursive split
      //printf("actually reasonable\n");
      reasonable = partition_split(v, i+1, sgnop, remaining_pluses,
          remaining_minuses, current0, current1, clr0, clr1, 
          one_right || (goes_right != 0)); // store info of if we have put something right
    }
    // now undo our changes
    // no need to undo the changes in current0
    // reset clr0 and clr1
    add_lr(i, -current0[i], remaining_pluses, remaining_minuses, clr0, goes_right != 0, false, false, 1);
    add_lr(i, -current1[i], remaining_pluses, remaining_minuses, clr1, goes_right != 0, true, false, (sgnop==1) ? 1 : -1);
    // undo the changes to remaining_pluses and remaining_minuses
    unnullify(v, i, remaining_pluses, remaining_minuses);
    if (reasonable)
      return true; // yay! that split worked
  }
  return false; // noes! no split worked
}

// try to partition the mobius vector v, with operand sgnop (1 for +, -1 for -)
bool partition(const vect v, int sgnop) {
  //printf("enter partition %d\n", absweight(v));
  vect remaining_pluses, remaining_minuses;
  vect current[2]; // current situations to the left and right
  vect clr[2]; // current cumulative weights in the configuration left and right

  // initialize remaining_pluses and remaining_minuses
  prepare_remaining(v, remaining_pluses, remaining_minuses);
  // initialize current and clr
  for (int i = 0; i < M; i++)
    for (int b = 0; b <= 1; b++)
      current[b][i] = clr[b][i] = 0;

  // start the recursive split
  bool val = partition_split(v, 0, sgnop, remaining_pluses, remaining_minuses, current[0], current[1], clr[0], clr[1], false);

  return val;
}

// check if Möbius vector v is reachable
bool reachable(const vect v) {
  config cfg;
  vect_to_config(v, cfg);
  /*printf("REACHABLE: ");
  print_val(cfg, M);
  printf("\n");
  print_vect(v);*/
  ull w = absweight(v);
  if (w == 0)
    return true; // 0 vector
  if (w == 1 && weight(v) == 1)
    return true; // base cone

  // try the global cache
  unordered_map<config,bool>::const_iterator it = reachable_cache.find(cfg);
  if (it != reachable_cache.end())
    return it->second; // already computed

  // try to partition with a sum or a difference
  bool val = (partition(v, 1) || partition(v, -1));

  // store in cache
  reachable_cache[cfg] = val;
  /*if (val && EXPLAIN) {
    assert(reach_l[vect_to_config(v)] != ((ull) 0));
  }*/
  return val;
}

// explain how to reach a configuration config, d is the depth for formatting
void explain(config cfg, int d) {
  vect v;
  config_to_vect(cfg, v);
  if (absweight(v) == 0)
    return; // trivial 
  if (absweight(v) == 1) {
    // basic cone
    if (!ONLY_CONES)
      for (int i = 0; i < d; i++)
        printf("  "); // indentation
    print_val(cfg, M);
    printf(" is a basic cone\n");
    return;
  }
  if (!ONLY_CONES)
    for (int i = 0; i < d; i++)
      printf("  "); // indentation
  print_val(cfg, M);
  printf(" = ");
  assert(reach_l[cfg] != ((ull) 0));
  assert(reach_r[cfg] != ((ull) 0));
  print_val(reach_l[cfg], M);
  if (reach_op[cfg] == 1)
    printf(" + ");
  else
    printf(" - ");
  vect vr;
  config_to_vect(reach_r[cfg], vr);
  if (ONLY_CONES && absweight(vr) == 1) {
    // print cones nicely
    int mn = 0;
    for (int i = 0; i < M; i++)
      if (vr[i])
        mn = i;
    printf("CONE:");
    print_val(myreverse(mn, N), N);
  } else {
    print_val(reach_r[cfg], M);
  }
  printf("\n");
  if (!TEST_IRRED) {
    explain(reach_l[cfg], d+1);
    if (!ONLY_CONES)
      explain(reach_r[cfg], d+1);
  }
}

// check if config is monotone
bool monotone(config conf) {
  for (unsigned int i = 0; i < M; i++) {
    for (unsigned int j = 0; j < N; j++) {
      if (conf[i] && !conf[i | (1 << j)])
        return false;
    }
  }
  return true;
}

// check if config is minimal
bool minimal(ull conf) {
  vector<int> P;
  for (unsigned int i = 0; i < N; i++)
    P.push_back(i);
  do {
    ull conf2 = 0;
    for (unsigned int p = 0; p < M; p++) {
      int np = 0;
      for (unsigned int q = 0; q < N; q++)
        if (p & (((ull) 1) << q))
          np = np | (((ull) 1) << P[q]);
      if (conf & (((ull) 1) << p))
        conf2 = conf2 | (((ull) 1) << np);
    }
    if (conf2 < conf)
      return false; // faster to exit
  } while (next_permutation(P.begin(), P.end()));
  return true;
}

bool config_lt(config c1, config c2) {
  for (int i = 0; i < M; i++)
    if (c1[i] != c2[i])
      if (!c1[i])
        return true; // c1 < c2 indeed
  return false; // tie
}

// get the minimal configuration of conf under permutations to the base elements
// of the lattice
void get_minimal(config conf, config &ans) {
  vector<int> P;
  for (unsigned int i = 0; i < N; i++)
    P.push_back(i);
  do {
    config conf2 = 0;
    for (unsigned int p = 0; p < (((ull) 1) << N); p++) {
      int np = 0;
      for (unsigned int q = 0; q < N; q++)
        if (p & (((ull) 1) << q))
          np = np | (((ull) 1) << P[q]);
      if (conf[p])
        conf2[np] = 1;
    }
    if (config_lt(conf2, ans)) 
      for (int i = 0; i < M; i++)
        ans[i] = conf2[i];
  } while (next_permutation(P.begin(), P.end()));
}


void read_config(char *s, unsigned int n, config &v) {
  for (unsigned int i = 0; i < n; i++) {
    if (s[i] == '1')
      v[i] = 1;
  }
}

void test_input_configs() {
  ull count = 0;
  unordered_set<config> tested;
  while (true) {
    char buf[M + 3];
    int ret = scanf("%s", buf);
    if (ret != 1)
      break;
    vect v;
    config c;
    read_config(buf, M, c);
    config crc;
    //get_minimal(c, crc);
    crc = c;
    if (ONLY_MONOTONE && !monotone(crc)) {
      printf("ERROR: not monotone: ");
      print_val(crc, M);
      printf("\n");
      return;
    }
//    if (!minimal(crc)) {
//      printf("ERROR: %llu is not minimal\n", crc);
//      print_val(crc, M);
//      printf("\n");
//      return 1;
//    }
    if (DISALLOW_DUPES && tested.find(crc) != tested.end()) {
      printf("DUPE already tested: ");
      print_val(crc, M);
      printf("\n");
      return;
    }
    tested.insert(crc);
    printf("####### CONSIDERING (%llu): ", count);
    print_val(crc, M);
    printf("\n");

    config_to_vect(crc, v);
    printf("weight %lld\n", absweight(v));
    if (reachable(v)) {
      printf(" reached\n");
      if (EXPLAIN) 
        explain(crc, 0);
    } else {
      print_val(crc, M);
      if (TEST_IRRED)
        printf(" NOT REDUCTIBLE\n");
      else
        printf(" NOT REACHED\n");
      return;
    }
    count++;
  }
  printf("reached %llu minimal non-constant monotone functions\n", count);
  return;
}

/*void test_all_monotone_configs() {
  vect v;
  unsigned long long count = 0;
  // do monotone functions, naively
  // this excludes the null function
  ull MX = 0;
  for (int p = 0; p < M; p++)
    MX |= ((ull) 1) << p;
  for (ull i = MX; i >= ((ull) 1) << (M-1); i--) {
    if (!(i % 100000000)) {
      fprintf(stderr, "%llu of %llu (%llu monotone seen)...\n", MX - i, ((ull) 1) << (M-1), count);
    }
    if (!monotone(i) || !minimal(i))
      continue;
    printf("####### CONSIDERING: ");
    print_val(i, M);
    printf("\n");

    config_to_vect(i, v);
    if (reachable(v)) {
      printf(" reached\n");
      if (EXPLAIN) 
        explain(i, 0);
    } else {
      print_val(i, M);
      if (TEST_IRRED)
        printf(" NOT REDUCTIBLE\n");
      else
        printf(" NOT REACHED\n");
      return;
    }
    count++;
  }
  printf("reached %llu minimal nonzero monotone functions\n", count);
  return;
}

void test_all_configs() {
  vect v;
  unsigned long long count = 0;
  unsigned long long reached = 0, notreached = 0;
  // do monotone functions, naively
  // this excludes the null function
  ull MX = 0;
  for (int p = 0; p < M; p++)
    MX |= ((ull) 1) << p;
  for (ull i = 0; i <= MX; i++) {
    // hack
    if (i == 1 && ONLY_MONOTONE) {
      i = (((ull) 1) << (M-1)) - 1;
      continue;
    }
    if (!(i % STEP)) {
      fprintf(stderr, "At %llu of 1+%llu (%llu seen of which got %llu missed %llu)...\n", i, MX, count, reached, notreached);
    }
    if (ONLY_MONOTONE && !monotone(i))
      continue;
    if (ONLY_MINIMAL && !minimal(i))
      continue; // this test is not so fast
    //printf("####### CONSIDERING: ");
    //print_val(i, M);
    //printf("\n");

    config_to_vect(i, v);
    if (reachable(v)) {
      reached++;
      if (EXPLAIN) {
        print_val(i, M);
        printf(" reached\n");
        explain(i, 0);
      }
    } else {
      notreached++;
      print_val(i, M);
      if (TEST_IRRED)
        printf(" NOT REDUCTIBLE\n");
      else
        printf(" NOT REACHED\n");
    }
    count++;
    if (reachable_cache.size() > CLEAR_EVERY) {
      fprintf(stderr, "clearing caches\n");
      reachable_cache.clear();
      reach_l.clear();
      reach_r.clear();
      reach_op.clear();
    }
  }
  printf("reached %llu and missed %llu out of %llu considered and 1+%llu total\n", reached, notreached, count, MX);
  return;
}
*/

int main() {
  test_input_configs();
  return 0;
}

