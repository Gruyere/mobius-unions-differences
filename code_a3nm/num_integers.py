#!/usr/bin/python3

import sys
import os

# Complicated handling required for SIGPIPE issues
# https://docs.python.org/3/library/signal.html#note-on-sigpipe
try:
    for l in sys.stdin.readlines():
        l = l.strip()
        f = l.split(' ')
        print ("%d %s" % (len(f), l))
    sys.stdout.flush()
except BrokenPipeError:
    pass
    # Python flushes standard streams on exit; redirect remaining output
    # to devnull to avoid another BrokenPipeError at shutdown
    devnull = os.open(os.devnull, os.O_WRONLY)
    os.dup2(devnull, sys.stdout.fileno())
    sys.exit(1)  # Python exits with error code 1 on EPIPE

