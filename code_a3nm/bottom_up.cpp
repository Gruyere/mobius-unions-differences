#include<cstdio>
#include<cassert>
#include<queue>
#include<set>
#include<unordered_set>
#include<array>
#include<algorithm>

typedef unsigned long long ull;

#ifndef N
#warning "N not defined from command line when compiling"
#define N 1 // set from command line
#endif

#define M (1 << N)
#define MAX (((ull) 1) << M)

// cone-reachable instead of reachable
#ifndef ONLY_CONES
#define ONLY_CONES 0
#endif

// only wellformed
#ifndef ONLY_WELLFORMED
#define ONLY_WELLFORMED 0
#endif

// test abs
#ifndef TEST_ABS
#define TEST_ABS 1
#endif


using namespace std;

typedef array<int, M> vect;

set<vect> reached; // all vects reached
deque<vect> allvect; // all vects reached (as queue)
deque<vect> newvect; // vects that we haven't tried to combine yet
deque<vect> allvect2; // stuff that will get added to reached
deque<vect> cones;

void print_vect(vect v) {
  for (int i = 0; i < M; i++)
    printf("%d:%d ", i, v[i]);
  printf("\n");
}

// test if vect is admissible
bool admissible_vect(vect v) {
  //print_vect(v);
  for (int i = 0; i < M; i++) {
    int sum = 0;
    for (int j = 0; j <= i; j++)
      if ((i | j) == i)
        sum += v[j];
    if (sum != 0 && sum != 1)
      return false;
  }
  return true;
}

// print a configuration
// beware: LSB (corresponding to emptyset) is at the LEFT
void print_config(ull val, unsigned int n) {
  for (unsigned int i = 0; i < n; i++) {
    printf("%llu", val%2);
    val = val >> 1;
  }
}

// get configuration
ull get_config(vect v) {
  ull config = 0;
  for (int i = 0; i < M; i++) {
    int sum = 0;
    for (int j = 0; j <= i; j++)
      if ((i | j) == i)
        sum += v[j];
    assert(sum == 0 || sum == 1);
    if (sum)
      config |= (((ull) 1) << i);
  }
  return config;
}

bool is_wellformed_ull (ull config) {
  for (unsigned int i = 0; i < M; i++)
    for (unsigned int j = i+1; j < M; j++)
      if (((i | j) == j) && (config & (((ull) 1) << i)) && !(config & (((ull) 1) << j)))
        for (unsigned int k = j+1; k < M; k++)
          if (((j | k) == k) && (config & (((ull) 1) << k))) {
            return false;
          }
  return true;
}

void add_vect(vect v) {
  if (reached.find(v) != reached.end())
    return;
  if (ONLY_WELLFORMED)
    if (!is_wellformed_ull(get_config(v)))
        return;
  /*printf("got config: ");
  print_config(get_config(v), M);
  printf("\n");
  printf("aka vect: ");
  print_vect(v);
  printf("\n");*/
  reached.insert(v);
  newvect.push_back(v);
  allvect2.push_back(v);
}

void transfer() {
  for (auto w: allvect2) {
    allvect.push_back(w);
  }
  allvect2.clear();
}

// check if config is minimal
bool minimal(ull conf) {
  vector<int> P;
  for (unsigned int i = 0; i < N; i++)
    P.push_back(i);
  do {
    ull conf2 = 0;
    for (unsigned int p = 0; p < M; p++) {
      int np = 0;
      for (unsigned int q = 0; q < N; q++)
        if (p & (((ull) 1) << q))
          np = np | (((ull) 1) << P[q]);
      if (conf & (((ull) 1) << p))
        conf2 = conf2 | (((ull) 1) << np);
    }
    if (conf2 < conf)
      return false; // faster to exit
  } while (next_permutation(P.begin(), P.end()));
  return true;
}

bool combine_vect(vect v1, vect v2, vect &w, int op) {
  for (unsigned int i = 0; i < M; i++) {
    int vv1 = v1[i], vv2 = v2[i];
    int ww = vv1 + op*vv2;
    if (TEST_ABS) {
      if (abs(ww) < abs(vv1) + abs(vv2))
        return false; // must be cancellation-free
    } else {
      if (ww == 0)
        if (vv1 != 0 || vv2 != 0)
          return false; // cannot play on zeroes
    }
    w[i] = ww;
  }
  if (!admissible_vect(w))
    return false;
  return true;
}

bool is_monotone_ull(ull config) {
  for (unsigned int i = 0; i < M; i++)
    for (unsigned int j = i+1; j < M; j++)
      if (((i|j) == j) && (config & (((ull) 1) << i)) && !(config & (((ull) 1) << j)))
        return false;
  return true;
}

int main() {
  reached.clear();
  newvect.clear();
  allvect.clear();
  allvect2.clear();
  cones.clear();

  vect zero = {};
  add_vect(zero);
  for (int i = 0; i < M; i++) {
    vect cone = {};
    cone[i] = 1;
    add_vect(cone);
    cones.push_back(cone);
  }

  transfer();
  printf("starting the part, newvect %lu\n", newvect.size());

  while(!newvect.empty()) {
    vect v = newvect.front();
    newvect.pop_front();

    // try combining v with either cones, or all vectors
    for (auto w: (ONLY_CONES ? cones : allvect)) {
      vect ww;
      if (combine_vect(v, w, ww, 1))
        add_vect(ww);
      if (combine_vect(v, w, ww, -1))
        add_vect(ww);
      if (!ONLY_CONES)
        if (combine_vect(w, v, ww, -1))
          add_vect(ww);
    }
    // note that we will not try to combine v with any ww
    // but it can be shown (TODO) that this can never be an admissible operation

    transfer();

    fprintf(stderr, "reached:%lu newvect:%lu\n", reached.size(), newvect.size());

    if (reached.size() == MAX) {
      printf("EVERYTHING REACHED\n");
      return 0;
    }
  }

  printf("REACHED %llu elements\n", reached.size());

  // find missing elements
  unordered_set<ull> seen_configs = {};

  for (auto v: allvect)
    seen_configs.insert(get_config(v));

  for (ull c = 0; c < (((ull) 1) << M); c++)
    if (seen_configs.find(c) == seen_configs.end()) {
      printf("missing config: ");
      print_config(c, M);
      if (minimal(c))
        printf(" (minimal)");
      if (is_monotone_ull(c))
        printf(" (monotone)");
      if (is_wellformed_ull(c))
        printf(" (wellformed)");
      //else
        //printf(" (notwellformed)");
      printf("\n");
    }

  return 0;
}
