#include <cstdio>
#include <iostream>
#include <vector>
#include <bitset>
#include <unordered_set>

// this code checks that we cannot get the configuration of Figure 4 in the note
// by combining the indicated cones

#define N 4

using namespace std;

typedef unsigned long long ull;

ull get_cone(const ull generator) {
  ull cone = 0;
  for (int s = 0; s < ((ull) 1) << N; s++) {
    //printf("consider %lld, generator %lld and is %lld\n", s, generator, s & generator);
    if (((ull) generator & s) == ((ull) generator)) {
      cone |= ((ull) 1) << s;
    }
    //printf("cone is now %lld added %lld\n", cone, ((ull) 1) << s);
  }
  //printf("cone for %lld is %lld\n", generator, cone);
  return cone;
}

int main() {
  unordered_set<ull> seen;
  seen.insert(get_cone(0b0100));
  seen.insert(get_cone(0b1000));
  seen.insert(get_cone(0b1100));
  seen.insert(get_cone(0b1010));
  seen.insert(get_cone(0b0011));
  seen.insert(get_cone(0b0101));
  ull target = 0;
  for (int s = 0; s < ((ull) 1) << N; s++)
    if (s == 0b0100 || s == 0b1000 || s == 0b0011 || s == 0b1001 || s == 0b0110 || s == 0b1100 || s == 0b0111 || s == 0b1011)
      target |= ((ull) 1) << s;
  bool changed = true;
  while (changed) {
    changed = false;
    unordered_set<ull> newthings;
    for (auto a: seen)
      for (auto b: seen) {
        if (((ull) ((ull) a) & ((ull) b)) == ((ull) 0)) {
          newthings.insert((ull) ((ull) a) | ((ull) b));
        }
        if (((ull) ((ull) a) & ((ull) b)) == ((ull) b)) {
          newthings.insert((ull) (((ull) a) & ((ull) (~((ull) b)))));
        }
        newthings.insert(~a & ((((ull) 1) << (1 << N)) - 1));
      }
    for (auto newthing: newthings) {
      if (seen.find(newthing) == seen.end()) {
        std::bitset<16> x(newthing);
        std::cout << x << '\n';
        if (newthing == target)
          printf("FOUND TARGET\n");
        changed = true;
        seen.insert(newthing);
      }
    }
  }
  printf("found %d things\n", seen.size());
  return 0;


}
