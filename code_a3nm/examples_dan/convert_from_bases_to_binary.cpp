#include<cstdio>
#include<cstring>

typedef unsigned long long ull;

int main() {
  while (1) {
    char buf[13];
    int ret = scanf("%s", buf);
    if (ret != 1)
      break;
    int len = strlen(buf);
    ull val = 0;
    for (int i = 0; i < len; i++) {
      int digit = buf[i] - '0';
      val += ((ull) 1) << digit;
    }
    printf("%lld\n", val);
  }
}
