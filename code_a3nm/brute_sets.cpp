// try the conjecture on random sets

#include<cstdio>
#include<iostream>
#include<cassert>
#include<random>
#include<algorithm>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<vector>
#include<queue>
#include<stack>

typedef unsigned long long ull;

// type representing a set of elements
typedef ull bset;
// maximal number of elements in a set (should be <= sizeof(bset))
#define MAXN 63
// number of elements used in base sets (the other elements are used to complete/ the lattice)
int NUMBASE;
// number of sets to consider
int NSET = 7;
// value which is not a value of the mobius function
#define UNDEF 4242
// maximum cardinality of sets
#define MAXCARD 0

using namespace std;

void gen_random(bset *base) {
  // draw NSET distinct random sets
  
  random_device r;
  default_random_engine e1(r());
  uniform_int_distribution<ull> uniform_dist(0, (((ull) 1) << NUMBASE)-1);

  for (int i = 0; i < NSET; i++) {
    // generate the i-th set at random
    // until we get one that does not occur before
    while (!base[i]) {
      base[i] = uniform_dist(e1);
      for (int j = 0; j < i; j++) {
        if (base[i] == base[j]) {
          base[i] = 0;
          break;
        }
      }
      // respect maximum cardinality (if MAXCARD != 0)
      if (base[i] && MAXCARD) {
        int popcount = 0;
        for (int j = 0; j < NUMBASE; j++) {
          if ((((ull) 1) << j) & base[i])
            popcount++;
        }
        if (popcount > MAXCARD)
          base[i] = 0;
      }
    }
  }

}


unordered_map<bset, pair<bset, bset> > R; // reached

bool explore_bfs(bset top, vector<int> Mob, vector<bset> N) {
  // now generate everything reachable and try to reach top
  // we use a priority queue where we priorize by popcount
  priority_queue<pair<int, bset> > Q2;
  Q2.push(make_pair(0, 0));
  R[0] = make_pair(0, 0);
  int greatest_size_reached = 0;
  bool done = false;
  ull last_printed_size = -1;
  while (!done && !Q2.empty()) {
    pair<int, bset> p = Q2.top();
    //cout << "  got: " << bitset<MAXN>(x) << endl;
    Q2.pop();
    bset x = p.second;
    if (R.size() - last_printed_size > 1000000) {
      printf("reached %ld sets, queue size %ld, greatest reached %d of %d (%ld nodes)\n", R.size(), Q2.size(), greatest_size_reached, __builtin_popcountll(top), N.size());
      last_printed_size = R.size();
    }
    for (unsigned int i = 0; i < N.size(); i++) {
      // only non-zero mobius
      if (!Mob[i])
        continue;
      // do not use top
      if (N[i] == top)
        continue;
      // try taking union
      if (!(N[i] & x)) {
        bset y = (N[i] | x);
        if (R.find(y) == R.end()) {
          //cout << "  " << bitset<MAXN>(y) << " = " << bitset<MAXN>(x) << " + " << bitset<MAXN>(N[i]) << endl;
          R[y] = make_pair(x, N[i]);
          if (y == top) {
            done = true;
            break;
          }
          greatest_size_reached = max(greatest_size_reached, __builtin_popcountll(y));
          Q2.push(make_pair(__builtin_popcountll(y), y));
        }
      }
      if ((x & N[i]) == N[i]) {
        bset y = x & (~N[i]);
        if (R.find(y) == R.end()) {
          //cout << "  " << bitset<MAXN>(y) << " = " << bitset<MAXN>(x) << " - " << bitset<MAXN>(N[i]) << endl;
          R[y] = make_pair(x, N[i]);
          if (y == top) {
            done = true;
            break;
          }
          greatest_size_reached = max(greatest_size_reached, __builtin_popcountll(y));
          Q2.push(make_pair(__builtin_popcountll(y), y));
        }
      }
    }
  }
  return done;
}





vector<int> Mob_remain;
vector<bset> N_dfs;
bset top_dfs;
int greatest_size_reached_dfs;
unsigned long int last_size_display_dfs;
unordered_set<bset> R_dfs; // reached

bool explore_dfs(bset cur, int remain) {
  //printf("call at status %lld remains %d\n", cur, remain);
  if (R_dfs.find(top_dfs & (~cur)) != R_dfs.end())
    return true; // already seen the complement
  if (!remain)
    return false; // ran out of moves!
  
  greatest_size_reached_dfs = max(greatest_size_reached_dfs, __builtin_popcountll(cur));
  if (R_dfs.size() > last_size_display_dfs + 1000000) {
    printf("DFS reached %ld sets, greatest reached %d of %d (%ld nodes)\n", R_dfs.size(), greatest_size_reached_dfs, __builtin_popcountll(top_dfs), N_dfs.size());
    last_size_display_dfs = R_dfs.size();
  }
  //printf("at cur %lld remain %d\n", cur, remain);

  for (unsigned int i = 0; i < N_dfs.size(); i++) {
    // only those with moves remaining (in particular never those with zero mobius)
    if (!Mob_remain[i])
      continue;
    // do not use top
    if (N_dfs[i] == top_dfs)
      continue;
    if (Mob_remain[i] < 0) {
      if (!(N_dfs[i] & cur)) {
        bset y = N_dfs[i] | cur;
        if (R_dfs.find(y) == R_dfs.end()) {
          //cout << "  " << bitset<MAXN>(y) << " = " << bitset<MAXN>(cur) << " + " << bitset<MAXN>(N_dfs[i]) << endl;
          R_dfs.insert(y);
          Mob_remain[i]--;
          if (explore_dfs(y, remain-1))
            return true;
          Mob_remain[i]++;
        }
      }
    } else if (Mob_remain[i] > 0) {
      if ((cur & N_dfs[i]) == N_dfs[i]) {
        bset y = cur & (~N_dfs[i]);
        if (R_dfs.find(y) == R_dfs.end()) {
          //cout << "  " << bitset<MAXN>(y) << " = " << bitset<MAXN>(cur) << " - " << bitset<MAXN>(N_dfs[i]) << endl;
          R_dfs.insert(y);
          Mob_remain[i]++;
          if (explore_dfs(y, remain-1))
            return true;
          Mob_remain[i]--;
        }
      }
    }
  }
  return false; // backtrack
}


bool explore_dfs_start(bset top, vector<int> Mob, vector<bset> N) {
  greatest_size_reached_dfs = 0;
  last_size_display_dfs = 0;
  Mob_remain.clear();
  N_dfs.clear();
  R_dfs.clear();
  for (unsigned int i = 0; i < N.size(); i++) {
    //printf("THE VECTOR %lld Mob %d: ", N[i], Mob[i]);
    //cout << "  " << bitset<MAXN>(N[i]) << endl;
    N_dfs.push_back(N[i]);
  }
  top_dfs = top;
  int mob_sum = 0;
  for (unsigned int i = 0; i < N.size(); i++) {
    Mob_remain.push_back(Mob[i]);
    mob_sum += abs(Mob[i]);
  }
  bool val = explore_dfs(0, mob_sum-mob_sum/2);

  for (unsigned int i = 0; i < N.size(); i++)
    if (!Mob[i])
      if (R_dfs.find(N[i]) != R_dfs.end()) {
        printf("WE GOT A ZERO, WEIRD\n");
        return false;
      }

  return val;
}








vector<bset> N_dfs_fwbw;
bset top_dfs_fwbw;
unordered_set<bset> R_dfs_fw, R_dfs_bw; // reached
typedef pair<int, bset> pibset;
typedef pair<pair<int, int>, pibset> piiibset;

bool advance(stack<piiibset> &S, unsigned int dlimit, vector<int> &Mrem, unordered_set<bset> &Rcache, unordered_set<bset> &Rocache, bool reverse) {
  if (S.empty())
    return false;
  // process next item on fw stack
  piiibset p = S.top();
  S.pop();
  int idx = p.second.first;
  bset cur = p.second.second;
  //printf("found depth %ld idx %d of %ld bset %lld\n", S.size(), idx, N_dfs_fwbw.size(), cur);
  //printf("remmob depth %ld:", S.size());
  //for (unsigned int i = 0; i < Mrem.size(); i++)
    //printf(" %d", Mrem[i]);
  //printf("\n");
  if (Rocache.find(cur) != Rocache.end())
    return true; // intersected!!
  if (S.size() <= dlimit) {
    // can we stack something new? try possibilities from idx+1 onwards
    for (unsigned int ii = idx+1; ii < N_dfs_fwbw.size(); ii++) {
      int i = reverse ? (N_dfs_fwbw.size() - ii - 1) : ii;
      if (N_dfs_fwbw[i] == top_dfs_fwbw)
        continue;
      // note: only one of these options at a time
      if (!(N_dfs_fwbw[i] & cur) && Mrem[i] < 0) {
        bset y = N_dfs_fwbw[i] | cur;
        if (Rcache.find(y) == Rcache.end()) {
          Rcache.insert(y);
          Mrem[i]++; // will be undone when unstacking
          S.push(make_pair(p.first, make_pair(ii, cur))); // push current index
          S.push(make_pair(make_pair(ii, 1), make_pair(-1, y)));
          //printf("PUSHED union index %d old %lld op %lld new %lld depth %ld\n", i, cur, N_dfs_fwbw[i], y, S.size());
          return false;
        }
      }
      if ((cur & N_dfs_fwbw[i]) == N_dfs_fwbw[i] && Mrem[i] > 0) {
        bset y = cur & (~N_dfs_fwbw[i]);
        if (Rcache.find(y) == Rcache.end()) {
          Rcache.insert(y);
          Mrem[i]--; // will be undone when unstacking
          S.push(make_pair(p.first, make_pair(ii, cur))); // push current depth
          S.push(make_pair(make_pair(ii, -1), make_pair(-1, y)));
          //printf("PUSHED difference index %d old %lld op %lld new %lld depth %ld\n", i, cur, N_dfs_fwbw[i], y, S.size());
          return false;
        }
      }
    }
  }
  // if found something new, we moved forward and stacked current index and then -1
  // if not, we unstack
  if (S.size() > 0) { // must not undo after last unstack
    if (reverse)
      Mrem[N_dfs_fwbw.size() - p.first.first - 1] -= p.first.second;
    else
      Mrem[p.first.first] -= p.first.second;
  }
  return false;
}

bool explore_dfs_fwbw_start(bset top, vector<int> Mob, vector<bset> N) {
  N_dfs_fwbw.clear();
  R_dfs_fw.clear();
  R_dfs_bw.clear();
  vector<int> Mob_remain_fw, Mob_remain_bw;
  for (unsigned int i = 0; i < N.size(); i++) {
    //printf("THE VECTOR %lld Mob %d\n", N[i], Mob[i]);
    N_dfs_fwbw.push_back(N[i]);
  }
  top_dfs_fwbw = top;
  int mob_sum = 0;
  for (unsigned int i = 0; i < N.size(); i++) {
    Mob_remain_fw.push_back(Mob[i]);
    Mob_remain_bw.push_back(-Mob[i]);
    mob_sum += abs(Mob[i]);
  }
  stack<piiibset> S_fw, S_bw;
  S_fw.push(make_pair(make_pair(-1, 0), make_pair(-1, 0)));
  S_bw.push(make_pair(make_pair(-1, 0), make_pair(-1, top_dfs_fwbw)));
  R_dfs_fw.insert(0);
  R_dfs_bw.insert(top_dfs_fwbw);
  int depth_limit_fw = mob_sum/2;
  int depth_limit_bw = mob_sum - mob_sum/2;
  bool val = false;
  //printf("Ndfsfwbw %ld mobsum %d\n", N_dfs_fwbw.size(), mob_sum);
  ull n_round = 0;
  while (!S_fw.empty() || !S_bw.empty()) {
    n_round++;
    if (!(n_round % 1000000))
      printf("round %lld, fw is %ld of %d bw is %ld of %d fwcache %ld bwcache %ld\n", n_round, S_fw.size(), depth_limit_fw, S_bw.size(), depth_limit_bw, R_dfs_fw.size(), R_dfs_bw.size());
    if ((val = advance(S_fw, depth_limit_fw, Mob_remain_fw, R_dfs_fw, R_dfs_bw, false)))
      break;
    if ((val = advance(S_bw, depth_limit_bw, Mob_remain_bw, R_dfs_bw, R_dfs_fw, true)))
      break;
  }

  for (unsigned int i = 0; i < N.size(); i++)
    if (!Mob[i])
      if (R_dfs_fw.find(N[i]) != R_dfs_fw.end() || R_dfs_bw.find(N[i]) != R_dfs_bw.end()) {
        printf("WE GOT A ZERO (index %d value %d), WEIRD\n", i, N[i]);
        return false;
      }

  return val;
}


















int compute_mobius(vector<int> &Mob, vector<bset> &N, unsigned int val) {
  // compute Möbius value of intersection val

  // return value if already computed
  if (Mob[val] != UNDEF)
    return Mob[val];

  // otherwise, sum across all subsets
  int sum = 0;
  for (unsigned int i = 0; i < N.size(); i++)
    if (i != val && ((N[i] & N[val]) == N[val]))
      sum += compute_mobius(Mob, N, i);
  Mob[val] = -sum;
  assert(Mob[val] != UNDEF);
  return Mob[val];
}

int main(int argc, char **argv) {
  NUMBASE = atoi(argv[1]);
  assert(NUMBASE > 0);
  assert(MAXN <= 8 * sizeof(bset));

  ull n_tested = 0, n_nontriv_tested = 0;
  while (++n_tested) {
    int ret = scanf("%d", &NSET);
    if (ret != 1)
      break;
    // if (!(n_tested % 1000))
    //   printf("tested %lld of which %lld nontrivial\n", n_tested, n_nontriv_tested);

    vector<bset> base;
    for (int i = 0; i < NSET; i++) {
      bset buf;
      scanf("%lld", &buf);
      base.push_back(buf);
    }

    //gen_random(base);
    /*// fig4 of note.pdf
    base[0] = 1049;
    base[1] = 533;
    base[2] = 75;
    base[3] = 39;
    base[4] = 4791;
    base[5] = 2415;
    // with extra orange set
    base[6] = 7608;
     * Fig 1d
    base[0] = 178;
    base[1] = 212;
    base[2] = 232;
    base[3] = 129;
    *
     * Fig 2 
    base[0] = 2050;
    base[1] = 3360;
    base[2] = 2824;
    base[3] = 3648;
    base[4] = 3204;
    base[5] = 2704;
    base[6] = 2049;
    */


    // check non-triviality
    bset top = base[0];
    for (int i = 1; i < NSET; i++)
      top |= base[i];
    bool ok = true;
    for (int i = 0; i < NSET; i++)
      if (base[i] == top) {
        ok = false; // trivial
        break;
      }
    if (!ok)
      continue;

    // compute all intersections and number them
    vector<bset> N; // maps index to bset
    unordered_map<bset, int> M; // maps bset to index
    queue<bset> Q;
    Q.push(top);
    while (!Q.empty()) {
      bset x = Q.front();
      Q.pop();
      // if it already exists, ignore
      if (M.find(x) != M.end())
        continue;
      // add element to M and N
      M[x] = N.size();
      N.push_back(x);
      // consider all intersections
      for (int i = 0; i < NSET; i++)
        Q.push(x & base[i]);
    }

    bool completed = false;
    if (NUMBASE < MAXN) {
      // ensure that every interection has a distinguishing element:
      // we will add elements from NUMBASE+1 to MAXN to the nodes without
      // distinguishing elt
      int pos = NUMBASE; // first available elt
      bool failed = false;
      // do not try to extend element 0, which is top!
      for (unsigned int i = 1; i < N.size(); i++) {
        bset cup = 0; // union of all the subsets
        for (unsigned int j = 0; j < N.size(); j++)
          if (i != j && ((N[j] & N[i]) == N[j]))
              cup |= N[j]; // a subelement
        if (cup == N[i]) {
          // ok, enlarge N[i]
          if (pos == MAXN) {
            // we ran out of extra elements, so abort
            failed = true;
            break;
          }
          bset newelt = (((ull) 1) << pos);
          pos++;
          for (unsigned int j = 0; j < N.size(); j++)
            if (i != j && ((N[j] & N[i]) == N[i]))
                N[j] |= newelt; // a superelement
          N[i] |= newelt; // we added to supersets before adding to N[i]
                          // otherwise the supersets would not be supersets
          top |= newelt;
          // added newelt to top and N[i] and all its supersets
        }
        if (failed)
          break;
      }
      if (failed) {
        printf("at lattice %lld: COULD NOT COMPLETE, ABORT\n", n_tested);
        return 42;
      }
      if (!failed)
        completed = true;
      //printf("there are %ld nodes\n", N.size());
      //printf("we completed using up to element %d\n", pos);
      // if we failed, we can still continue... not all elements are
      // distinguished but oh well
    }


    // compute mobius function
    vector<int> Mob;
    for (unsigned int i = 0; i < N.size(); i++)
      Mob.push_back(UNDEF);
    assert(N[0] == top);
    Mob[0] = 1;
    bool seen_zero = false;
    for (unsigned int i = 0; i < N.size(); i++) {
      int val = compute_mobius(Mob, N, i);
      if (!val)
        seen_zero = true;
    }
    // conjecture is trivial if there is no zero
    if (!seen_zero) {
      //printf("it is trivial: no zero mobius value\n");
      continue;
    }
    n_nontriv_tested++;

    // R.clear();
    // bool done = explore_bfs(top, Mob, N);
    /*// if we generated a zero and we had completed, something is wrong ?
    if (completed)
      for (unsigned int i = 0; i < N.size(); i++)
        if (!Mob[i])
          if (R.find(N[i]) != R.end()) {
            printf("WE GOT A ZERO, WEIRD\n");
            return 1;
          }
    */
    bool done = explore_dfs_fwbw_start(top, Mob, N);

    /*for (unsigned int i = 0; i < N.size(); i++) {
      cout << bitset<MAXN>(N[i]) << " " << Mob[i] << endl;
    }*/


    // if we did not get done, this is a countereaxmple to the strong conjecture
    if (!done) {
      printf("FOUND COUNTEREXAMPLE\n");
      for (int i = 0; i < NSET; i++)
        printf("%lld\n", base[i]);
      break;
    }

    if (done)
      printf("Got top (num %lld), moving on...\n", n_tested);

    // if we got top, let's see how
    /*bset c = top;
    while (c) {
      bset o1 = R[c].first;
      bset o2 = R[c].second;
      if (c == (o1 | o2))
        cout << bitset<MAXN>(c) << " = " << bitset<MAXN>(o1) << " + " << bitset<MAXN>(o2) << endl;
      else
        cout << bitset<MAXN>(c) << " = " << bitset<MAXN>(o1) << " - " << bitset<MAXN>(o2) << endl;
      c = o1; 
    }*/
  }
  printf("DONE, tested %lld of which %lld nontriv\n", n_tested, n_nontriv_tested);
  return 0;
}
