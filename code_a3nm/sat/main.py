#!/usr/bin/env python3

import sys
from collections import defaultdict
from itertools import combinations

tabvar = {}
tabvarinv = {}
numvar = 1
seen_zero = False


def var(s):
    global numvar
    if s in tabvar.keys():
        return tabvar[s]
    tabvar[s] = numvar
    tabvarinv[numvar] = s
    numvar += 1
    return tabvar[s]

def sets2moves(S):
    global seen_zero
    nodes = set()
    movespos = []
    movesneg = []
    for i in range(1,len(S)+1):
        for cc in combinations(S, i):
            inter = cc[0]
            for c in cc[1:]:
                inter = inter & c
            nodes.add(inter)
    #mobius = defaultdict(lambda x: 0)

    nodesl = list(nodes)
    old2new = {}
    for n in nodes:
        old2new[n] = 0

    print(nodesl)

    for i in range(len(nodes)):
        # add element i to node n and everything above
        n = nodesl[i]
        print("considering old node %d" % n)
        for n2 in nodes:
            if (n2 & n == n):
                old2new[n2] = old2new[n2] | (1 << i)
                print("put %d in element %d old %d" % (i, old2new[n2],
                                                       n2))

    new = set()
    for k in old2new.keys():
        n2 = old2new[k]
        assert(n2 not in new)
        new.add(n2)
        print(k, n2)

    mob = {}

    def mobius(n):
        global seen_zero
        if n in mob.keys():
            return mob[n]
        mysum = 0
        for n2 in new:
            if (n2 & n) == n and n2 != n:
                mysum += mobius(n2)
        mob[n] = 1-mysum
        if mob[n] == 0:
            seen_zero = True
        return mob[n]


    for n in new:
        mobius(n)
        if mob[n] < 0:
            for i in range(abs(mob[n])):
                movesneg.append(n)
        if mob[n] > 0:
            for i in range(abs(mob[n])):
                movespos.append(n)

    print(mob)
    return len(new), movespos, movesneg

    
for l in sys.stdin.readlines():
    S = l.strip().split(' ')
    S = [int(SS) for SS in S]
    L, movespos, movesneg = sets2moves(S)
    print(movespos)
    print(movesneg)

    P = len(movespos)
    N = len(movesneg)
    M = N + P
    assert(P == N + 1)

    with open("skipzero", 'w') as fh:
        if not seen_zero:
            print("skip", file=fh)
        else:
            print("doit", file=fh)

    if not seen_zero:
        sys.exit(0)

    C = []
    
    # pour i de 0 à M inclus
    # pour k de 0 à L exclu
    # variable sik: indique si l'élément k est présent après le i-ème coup

    # pour i de 0 à P exclu
    # pour j de 0 à P exclu
    # variable pij: indique si le coup positif i est joué comme coup numéro 2j
    # (à partir de 0)

    # pour i de 0 à N exclu
    # pour j de 0 à N exclu
    # variable nij: indique si le coup négatif i est joué comme coup numéro 2j+1
    # (à partir de 0)

    # chaque coup négatif est joué au plus une fois
    for i in range(N):
        for j1 in range(N):
            for j2 in range(j1+1,N):
                C.append((-var("n_%d_%d" % (i, j1)), -var("n_%d_%d" % (i, j2))))

    # chaque coup positif est joué au plus une fois
    for i in range(P):
        for j1 in range(P):
            for j2 in range(j1+1,P):
                C.append((-var("p_%d_%d" % (i, j1)), -var("p_%d_%d" % (i, j2))))

    # chaque position reçoit au plus un coup négatif
    for j in range(N):
        for i1 in range(N):
            for i2 in range(j1+1,N):
                C.append((-var("n_%d_%d" % (i1, j)), -var("n_%d_%d" % (i2, j))))

    # chaque position reçoit au plus un coup positif
    for j in range(P):
        for i1 in range(P):
            for i2 in range(i1+1,P):
                C.append((-var("p_%d_%d" % (i1, j)), -var("p_%d_%d" % (i2, j))))

    # chaque coup négatif est joué au moins une fois
    for i in range(N):
        C.append(tuple([var("n_%d_%d" % (i, j)) for j in range(N)]))
    # chaque coup positif est joué au moins une fois
    for i in range(P):
        C.append(tuple([var("p_%d_%d" % (i, j)) for j in range(P)]))
    # chaque position négative reçoit au moins un coup
    for j in range(N):
        C.append(tuple([var("n_%d_%d" % (i, j)) for i in range(N)]))
    # chaque position positive reçoit au moins un coup
    for j in range(P):
        C.append(tuple([var("p_%d_%d" % (i, j)) for i in range(P)]))

    # au début, tout est vide
    for k in range(L):
        C.append((-var("s_%d_%d" % (0, k)),))

    # après un coup positif:
    for k in range(L):
        # on considère le ième coup positif
        for i in range(P):
            for j in range(P):
                # si l'élément k est dans le i-ème coup positif parmi les coups
                # positifs à jouer
                if (1 << k) & movespos[i]:
                    # si on joue le coup j alors il fallait que k ne soit pas là avant
                    C.append((-var("p_%d_%d" % (i, j)), -var("s_%d_%d" % (2*j, k))))
                    # si on joue le coup j alors il faut que k soit là après
                    C.append((-var("p_%d_%d" % (i, j)), var("s_%d_%d" % (2*j+1, k))))
                else:
                    C.append((-var("p_%d_%d" % (i, j)),
                              var("s_%d_%d" % (2*j, k)),
                              -var("s_%d_%d" % (2*j+1, k))))
                    C.append((-var("p_%d_%d" % (i, j)),
                              -var("s_%d_%d" % (2*j, k)),
                              var("s_%d_%d" % (2*j+1, k))))

    # après un coup negatif:
    for k in range(L):
        # on considère le ième coup negatif
        for i in range(N):
            for j in range(N):
                # si l'élément k est dans le j-ème coup negatif parmi les coups
                # negatifs à jouer
                if (1 << k) & movesneg[i]:
                    # si on joue le coup j alors il fallait que k soit là avant
                    C.append((-var("n_%d_%d" % (i, j)), var("s_%d_%d" % (2*j+1, k))))
                    # si on joue le coup j alors il faut que k ne soit pas là après
                    C.append((-var("n_%d_%d" % (i, j)), -var("s_%d_%d" % (2*j+2, k))))
                else:
                    C.append((-var("n_%d_%d" % (i, j)),
                              var("s_%d_%d" % (2*j+1, k)),
                              -var("s_%d_%d" % (2*j+2, k))))
                    C.append((-var("n_%d_%d" % (i, j)),
                              -var("s_%d_%d" % (2*j+1, k)),
                              var("s_%d_%d" % (2*j+2, k))))

    # à la fin tout le monde est là
    for k in range(L):
        C.append((var("s_%d_%d" % (M, k)),))

    with open("varnames", 'w') as fh:
        for k in tabvar.keys():
            print("%d: %s" % (tabvar[k], k), file=fh)

    with open("movespos", 'w') as fh:
        for m in movespos:
            print(m, file=fh)

    with open("movesneg", 'w') as fh:
        for m in movesneg:
            print(m, file=fh)

    with open("clauses", 'w') as fh:
        print("p cnf %d %d" % (numvar-1, len(C)), file=fh)
        for CC in C:
            print(" ".join((str(x) for x in CC)) + " 0", file=fh)

    # for CC in C:
    #     print(" ".join((("+" if x > 0 else "-") + tabvarinv[abs(x)] for x in CC)))
