#!/bin/bash

set -e

NUM=0
while read l
do
  NUM=$(($NUM+1))
  echo "$l" | ./main.py 
  echo "===== GENERATED $NUM"
  if grep skip skipzero
  then
    echo "===== SKIP $NUM"
  else
    ~/apps/glucose/parallel/glucose-syrup clauses out -model > output || true
    echo "===== SOLVED $NUM"
    if ./verif.py < output
    then 
      echo OK
    else
      echo "PROBLEM"
      exit 42
    fi
    echo "===== VERIFIED $NUM"
  fi
done

