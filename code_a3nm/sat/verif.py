#!/usr/bin/env python3

import sys

universe = 0

valuation = {}
for l in sys.stdin.readlines():
    if l.startswith("v"):
        f = l.strip().split(' ')
        f = f[1:-1]
        for v in f:
            v = int(v)
            valuation[abs(v)] = 0 if v < 0 else 1

movespos = []
with open("movespos", 'r') as fh:
    for l in fh.readlines():
        movepos = int(l.strip())
        universe = universe | movepos
        movespos.append(movepos)
movesneg = []
with open("movesneg", 'r') as fh:
    for l in fh.readlines():
        moveneg = int(l.strip())
        universe = universe | moveneg
        movesneg.append(moveneg)

tabneg = []
tabpos = []
for i in range(len(movesneg)):
    tabneg.append(None)
for i in range(len(movespos)):
    tabpos.append(None)

status = []
for i in range(len(movesneg) + len(movespos) + 1):
    status.append(0)

with open("varnames", 'r') as fh:
    for l in fh.readlines():
        f = l.strip().split(":")
        num = int(f[0])
        name = f[1].strip()
        typ = name[0]
        ff = name.split("_")
        a1 = int(ff[1])
        a2 = int(ff[2])
        print(num, name, valuation[num])
        if typ == 's' and valuation[num]:
            print(a1, a2)
            status[a1] = status[a1] | (1 << a2)
        elif typ == 'p' and valuation[num]:
            print("tabpos[%d] = %d" % (a2, a1))
            tabpos[a2] = a1
        elif typ == 'n' and valuation[num]:
            print("tabneg[%d] = %d" % (a2, a1))
            tabneg[a2] = a1

# verify moves

print(movesneg)
print(movespos)
print(tabneg)
print(tabpos)
print(status)

for i in range(len(tabpos)):
    movenum = tabpos[i]
    moveval = movespos[movenum]
    assert(status[2*i] | moveval == status[2*i+1])
    assert(status[2*i] & moveval == 0)

for i in range(len(tabneg)):
    movenum = tabneg[i]
    moveval = movesneg[movenum]
    assert(status[2*i+2] | moveval == status[2*i+1])
    assert(status[2*i+2] & moveval == 0)

# check boundaries
assert(status[0] == 0)
assert(status[len(status)-1] == universe)
