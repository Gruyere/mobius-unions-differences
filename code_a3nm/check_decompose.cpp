// check if configuration can be split into decomposable configurations

#include<cstdio>
#include<array>
#include<cassert>
#include<vector>
#include<unordered_set>
#include<algorithm>

using namespace std;

#define N 4

// also check if decomposable with cones (slower)
#define SEARCH_CONES 0

typedef unsigned long long ull;

typedef array<char, 1 << N> vect;

// print a binary value of n bits (beware, LSB is left)
void print_val(ull val, unsigned int n) {
  for (unsigned int i = 0; i < n; i++) {
    printf("%lld", val%2);
    val = val >> 1;
  }
  printf("\n");
}

// reverse binary value of n bits
ull reverse(ull x, ull n) {
  ull v = 0;
  for (unsigned int i = 0; i < n; i++) {
    v = v << 1;
    v = v | (x & 1);
    x = x >> 1;
  }
  return v;
}

// compute euler characteristic of configuration x
int euler(ull x) {
  int v = 0;
  for (unsigned int i = 0; i < (1 << N); i++)
    v += ((__builtin_popcount(i) % 2) ? 1 : -1) * ((x & (1 << v)) ? 1 : 0);
  return v;
}

// write mobius value of x in vect v
void mobius(ull x, vect &v) {
  for (unsigned int i = 0; i < (1 << N); i++) {
    int s = 0;
    for (unsigned int j = 0; j < i; j++) {
      if ((j & i) != j)
        continue; // not subset
      // we have j subset of i
      s += v[j];
    }
    // value for i is 1 or 0 depending on whether i is lit, minus the sum
    v[i] = ((x & (1 << i)) ? 1 : 0) - s;
  }
}

// compute complement of value of n bits
inline ull comp(ull x, int n) {
  return ((((ull) 1) << n)-1) & (~x);
}

bool monotone (ull conf) {
  // is it monotone?
  for (unsigned int i = 0; i < (1 << N); i++) {
    for (unsigned int j = 0; j < N; j++) {
      if ((conf & (1 << i)) && !(conf & ((1 << (i | (1 << j))))))
        return false;
    }
  }
  return true;
}

ull read_config(char *s, unsigned int n) {
  ull v = 0;
  for (unsigned int i = 0; i < n; i++) {
    v = v << 1;
    if (s[i] == '1')
      v += 1;
  }
  return reverse(v, n);
}

void prepare_cones (unordered_set<ull> &cones) {
  for (int i = 0; i < (1 << N); i++) {
    ull c = 0;
    for (int j = i; j < (1 << N); j++) {
      if ((j & i) == i)
        c |= (((ull) 1) << j); // j is a superset of i;
    }
    cones.insert(c);
  }
}

bool is_cone(ull config) {
  // prepare the cones
  unordered_set<ull> cones;
  prepare_cones(cones);
  return (cones.find(config) != cones.end());
}

// print value of n bits together with info
void print_val_stat(ull val, unsigned int n, bool plus_dec, bool plus_cone_dec, bool minus_dec, bool minus_cone_dec, int skip) {
  ull nval = val;
  for (unsigned int i = 0; i < n; i++) {
    printf("%lld", nval%2);
    nval = nval >> 1;
  }
  printf(" ");
  printf("euler:%d ", euler(val));
  printf("hamming:%d ", __builtin_popcount(val & ((((ull)1) << (1 << N))-1)));
  if (!skip) {
    if (!plus_dec && !minus_dec)
      printf("irreductible! ");
    if (SEARCH_CONES && !plus_cone_dec && !minus_cone_dec)
      printf("cone_irred! ");
  }
  if (monotone(val))
    printf("MONOTONE ");
  if (is_cone(val))
    printf("ISCONE ");
  printf("\n");

}

// return if irred
bool test (ull crc, bool detail) {
  // prepare the cones
  unordered_set<ull> cones;
  prepare_cones(cones);

  bool plus_dec = false;
  bool plus_cone_dec = false;
  bool minus_dec = false;
  bool minus_cone_dec = false;

  // test if decomposable by a +
  ull v = 0;
  while (true) {
    v |= comp(crc, 1 << N);
    v += 1;
    v &= crc;
    // v iterates over subsets
    ull v2 = crc - v;
    // v2 is the complement
    // only check when v!=0 and v2!=0
    if (v && v2) {
      assert ((v + v2) == crc);
      assert ((v | v2) == crc);
      vect m, m2;
      mobius(v, m);
      mobius(v2, m2);
      // check that the combination is legal
      bool ok = true;
      for (unsigned int i = 0; i < (1 << N); i++) {
        if (abs(m[i] + m2[i]) != abs(m[i]) + abs(m2[i])) {
          ok = false; // illegal
          break;
        }
      }
      if (ok) {
        plus_dec = true; //plus_decomposable
        if (detail) {
          printf("Plus decomposable\n");
          print_val(v, 1 << N);
          print_val(v2, 1 << N);
        }
        if (SEARCH_CONES) {
          if (cones.find(v) != cones.end() || cones.find(v2) != cones.end()) {
            plus_cone_dec = true;
            break; // now we know everything
          }
        } else {
          break; // we know plus_decomposable
        }
      }
      //print_val(v, 16);
    }
    if (v == crc)
      break; // done looping
  }

  // test if decomposable by a -
  v = 0;
  ull ncrc = comp(crc, 1 << N);
  while (true) {
    v |= (~ncrc);
    v += 1;
    v &= ncrc;
    // v is a subset of the complement
    ull v2 = crc | v;
    assert(v2 - v == crc);
    if (v) {
      vect m, m2;
      mobius(v, m);
      mobius(v2, m2);
      // check that legal
      bool ok = true;
      for (unsigned int i = 0; i < (1 << N); i++) {
        if (abs(m[i] - m2[i]) != abs(m[i]) + abs(m2[i])) {
          ok = false;
          break;
        }
      }
      if (ok) {
        minus_dec = true;
        if (detail) {
          printf("Minus decomposable\n");
          print_val(v2, 1 << N);
          print_val(v, 1 << N);
        }
        if (SEARCH_CONES) {
          if (cones.find(v) != cones.end() || cones.find(v2) != cones.end()) {
            minus_cone_dec = true;
            break; // now we know everything
          }
        } else {
          break; // we know plus_decomposable
        }
        break;
      }
    }
    if (v == ncrc)
      break;
  }

  //print_val_stat(crc, (1 << N), plus_dec, plus_cone_dec, minus_dec, minus_cone_dec, false);
  return (!plus_dec && !minus_dec);
}

int main (int argc, char **argv) {
  ull crc = read_config(argv[1], 32);

  // let's test all numbers
  int upper_irred = -1, upper_irred_with_0 = -1, lower_irred = -1, lower_irred_with_0 = -1;
  for (int i = 0; i < 5; i++) {
    ull upper = 0, lower = 0;
    // construct the upper and lower configuration
    for (int pos = 0; pos < (1 << 5); pos++) {
      int pos_except_i = 0;
      for (int k = 0; k < i; k++)
        pos_except_i |= (pos & (1 << k)) ? (1 << k) : 0;
      for (int k = i+1; k < 5; k++)
        pos_except_i |= (pos & (1 << k)) ? (1 << (k-1)) : 0;
      //printf("pos %d i %d pos_except_i %d\n", pos, i, pos_except_i);
      if (pos & (1 << i)) {
        // i is present
        upper |= (crc & (1 << pos)) ? (1 << pos_except_i) : 0;
      } else {
        // i is absent
        lower |= (crc & (1 << pos)) ? (1 << pos_except_i) : 0;
      }
    }
    //print_val(upper, 16);
    //print_val(lower, 16);
    if (test(upper, false)) {
      upper_irred = i;
      if (!lower)
        upper_irred_with_0 = i;
    }
    if (test(lower, false)) {
      lower_irred = i;
      if (!upper)
        lower_irred_with_0 = i;
    }
  }
  ull nval = crc;
  for (unsigned int i = 0; i < 32; i++) {
    printf("%lld", nval%2);
    nval = nval >> 1;
  }
  printf(" ");
  if (upper_irred >= 0)
    printf(" upper_irreductible:%d ", upper_irred);
  if (lower_irred >= 0)
    printf(" lower_irreductible:%d ", lower_irred);
  if (upper_irred_with_0 >= 0)
    printf(" upper_irred_with0:%d ", upper_irred_with_0);
  if (lower_irred_with_0 >= 0)
    printf(" lower_irred_with0:%d ", lower_irred_with_0);
  printf("\n");

  return 0;

}
