#include<cstdio>
#include<iostream>
#include<cstdlib>
#include<bitset>
#include<random>

#ifndef N
#warning "N should have been defined on the command line"
#define N 8
#endif
#define M (1 << N)

using namespace std;

typedef bitset<M> config;

void print_val(config val, unsigned int n) {
  for (unsigned int i = 0; i < n; i++) {
    printf("%d", val[i] ? 1 : 0);
  }
}

int main(int argc, char **argv) {
  int num = atoi(argv[1]);
  int rndfun = atoi(argv[2]);
  config cones[M];
  for (int i = 0; i < M; i++) {
    cones[i] = 0;
    for (int j = i; j < M; j++)
      if ((i | j) == j)
        cones[i][j] = 1;
  }
  for (int i = 0 ; i < num; i++ ) {
    config c = 0;
    random_device r;
    default_random_engine e1(r());
    uniform_int_distribution<int> uniform_dist(0, rndfun);
    int ntimes = uniform_dist(e1);
    for (int j = 0; j < ntimes; j++) {
      uniform_int_distribution<int> uniform_dist(0, M-1);
      int val = uniform_dist(e1);
      c |= cones[val];
    }
    print_val(c, M);
    printf("\n");

  }

}
