#include <cstdio>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <cassert>
#include <vector>
#include <algorithm>
#include <bitset>

// use ulimit -s to increase the stack size otherwise it will segfault

// bruteforce to decompose a configuration into elementary configurations

#ifndef N
#warning "N should have been defined on the command line"
#define N 3
#endif
#define M (1 << N)

// whether to display explanations
#ifndef EXPLAIN
#define EXPLAIN 1
#endif

// // whether to test all configurations (instead of those provided on stdin)
// #ifndef ALL
// #define ALL 0
// #endif

// if ALL=1, only test...
#ifndef ONLY_MONOTONE
#define ONLY_MONOTONE 0
#endif
#ifndef ONLY_MINIMAL
#define ONLY_MINIMAL 1
#endif

typedef unsigned long long ull;
typedef long long ll;

// // interval to print progress
// #ifndef STEP
// #define STEP 100000000
// #endif

// disallow dupes in input
#ifndef DISALLOW_DUPES
#define DISALLOW_DUPES 0
#endif

using namespace std;

typedef array<ll, M> vect;
typedef std::bitset<M> config;

// print a configuration
void print_val(config val, unsigned int n) {
  for (unsigned int i = 0; i < n; i++) {
    printf("%d", val[i] ? 1 : 0);
  }
}

// reverse binary value of n bits
ull myreverse(ull x, ull n) {
  ull v = 0;
  for (unsigned int i = 0; i < n; i++) {
    v = v << 1;
    v = v | (x & 1);
    x = x >> 1;
  }
  return v;
}

// print a mobius vector
void print_vect(vect v) {
  for (int i = 0; i < M; i++)
    printf("%d:%lld ", i, v[i]);
  printf("\n");
}

// weight of a mobius vector
ll weight(vect v) {
  ll w = 0;
  for (int i = 0; i < M; i++)
    w += v[i];
  return w;
}

// absolute weight of a mobius vector
ull absweight(vect v) {
  ull w = 0;
  for (int i = 0; i < M; i++)
    w += llabs(v[i]);
  return w;
}

// sign of a number
inline int sgn(ll x) {
  if (x < 0) return -1;
  if (x > 0) return 1;
  return 0;
}

// converts a vector to a config
void vect_to_config(const vect v, config &c) {
  for (int i = 0; i < M; i++) {
    c[i] = 0;
    ll w = 0;
    for (int j = 0; j <= i; j++)
      if ((i | j) == i) // j \subseteq i
        w += v[j];
    assert(w == 0 || w == 1); // the configuration should be acceptable
    if (w == 1)
      c[i] = 1;
  }
}

// computes the vector of config x and stores it in v
void config_to_vect(config x, vect &v) {
  for (unsigned int i = 0; i < M; i++) {
    ll s = 0;
    for (unsigned int j = 0; j < i; j++) {
      if ((j & i) != j)
        continue; // not subset
      // we have j subset of i
      s += v[j];
    }
    // value for i is 1 or 0 depending on whether i is lit, minus the sum
    v[i] = (x[i] ? 1 : 0) - s;
  }
}


vect gv;
vect norm_below, minus_below, lowest_plus, lowest_minus, height;
config gcfg, tcfg;


pair<int, int> criterion(ull w, int sgnop) {
  //return make_pair(min (__builtin_popcount(lowest_plus[w]), __builtin_popcount(lowest_minus[w])), 0);
  return make_pair(-norm_below[w], 0);
  /*if (sgnop == 1)
    return make_pair(__builtin_popcount(lowest_plus[w]), -minus_below[w]);
  else // -1
    return make_pair(__builtin_popcount(lowest_minus[w]), -minus_below[w]);*/
}

void init() {
  for (ull i = 0; i < M; i++) {
    norm_below[i] = 0;
    minus_below[i] = 0;
    lowest_minus[i] = M-1;
    lowest_plus[i] = M-1;
    if (gv[i] > 0)
      lowest_plus[i] = i;
    if (gv[i] < 0)
      lowest_minus[i] = i;
    height[i] = 0;
    for (ull j = 0; j <= i; j++)
      if ((j | i) == i) {
        if (__builtin_popcount(lowest_plus[j]) < __builtin_popcount(lowest_plus[i]))
          lowest_plus[i] = lowest_plus[j];
        if (__builtin_popcount(lowest_minus[j]) < __builtin_popcount(lowest_minus[i]))
          lowest_minus[i] = lowest_minus[j];
        norm_below[i] += llabs(gv[j]);
        if (gv[j] < 0)
          minus_below[i] += llabs(gv[j]);
      }
    for (int o = 0; o < N; o++)
      if (i & (((ull) 1) << o))
        height[i] = max(height[i], height[i ^ (((ull) 1) << o)]);
    if (gv[i] != 0)
      height[i]++;
  }
}

bool playable(ull v, int sgnop) {
  // can we play sgnop on v?
  if (!(gv[v] && (sgn(gv[v]) == sgn(sgnop))))
    return false;
  for (ull j = v; j < M; j++)
    if ((j | v) == j) {
      if (sgnop == 1) {
        if(gcfg[j] != 0)
          return false;
      } else { // sgnop == -1
        if (gcfg[j] != 1)
          return false;
      }
    }
  return true;
}

ll act2(int sgnop) {
  vector<pair<pair<int, int>, ull>> V;
  for (ull v = 0; v < M; v++)
    V.push_back(make_pair(make_pair(-norm_below[v], __builtin_popcount(v)), v));
  sort(V.begin(), V.end());
  for (ull i = 0; i < M; i++) {
    //printf("consider %d score %d %d\n", V[i].second, V[i].first.first, V[i].first.second);
    ull v = V[i].second;
    if (playable(v, sgnop)) {
      //printf("play %d\n", V[i].second);
      // yeah let's play!

      // update norm
      //norm_below[v]--;
      // update vector
      //printf("gv[%d] was %d will become %d\n", v, gv[v], gv[v]-sgnop);
      gv[v] -= sgnop;
      // toggle cone
      for (ull j = v; j < M; j++)
        if ((j | v) == j) {
          if (sgnop == 1) {
            assert(gcfg[j] == 0);
            gcfg[j] = 1;
          } else { // sgnop == -1
            assert(gcfg[j] == 1);
            gcfg[j] = 0;
          }
        }
      return v;
    }
  }
  return -1;
}

ll act(ull v, int sgnop) {
  // we want to do action at v
  // sort children by ascending criterion
  vector<pair<pair<int, int> , ull> > l;
  for (int o = 0; o < N; o++)
    if (v & (((ull) 1) << o)) {
      ull w = v ^ (((ull) 1) << o);
      //printf("child %d score %d\n", w, criterion(w, sgnop));
      l.push_back(make_pair(criterion(w, sgnop), w));
  }
  sort(l.begin(), l.end());
  for (unsigned int i = 0; i < l.size(); i++) {
    int w = l[i].second;
    ll val = act(w, sgnop);
    if (val >= 0) {
      // update norm
      //norm_below[v]--;
      // toggle cone done by the subcall
      return val;
    }
  }
  if (playable(v, sgnop)) {
    // yeah let's play!

    // update norm
    //norm_below[v]--;
    // update vector
    //printf("gv[%d] was %d will become %d\n", v, gv[v], gv[v]-sgnop);
    gv[v] -= sgnop;
    // toggle cone
    for (ull j = v; j < M; j++)
      if ((j | v) == j) {
        if (sgnop == 1) {
          assert(gcfg[j] == 0);
          gcfg[j] = 1;
        } else { // sgnop == -1
          assert(gcfg[j] == 1);
          gcfg[j] = 0;
        }
      }
    return v;
  }
  return -1;
}


bool process(config cfg) {
  gcfg = 0;
  tcfg = cfg;
  config_to_vect(cfg, gv);


  ull mx = M-1;
  ull num = absweight(gv);
  if (EXPLAIN) {
    printf("start norm %llu: ", num);
    print_val(gcfg, M);
    printf("\n");
    print_vect(gv);
  }
  for (int i = 0; i < num; i++) {
    int sgnop = (i % 2) ? (-1) : 1;
    init();
    //ll move = act(mx, sgnop);
    ll move = act2(sgnop);
    if (move == -1)
      return false;
    if (EXPLAIN) {
      printf("PLAY %s on CONE: ", sgnop == 1 ? "+" : "-");
      print_val(myreverse(move, N), N);
      printf(" config now ");
      print_val(gcfg, M);
      printf("\n");
    }
  }
  assert(tcfg == gcfg);
  // success
  return true;
}


// check if config is monotone
bool monotone(config conf) {
  for (unsigned int i = 0; i < M; i++) {
    for (unsigned int j = 0; j < N; j++) {
      if (conf[i] && !conf[i | (1 << j)])
        return false;
    }
  }
  return true;
}



// check if config is minimal
bool minimal(ull conf) {
  vector<int> P;
  for (unsigned int i = 0; i < N; i++)
    P.push_back(i);
  do {
    ull conf2 = 0;
    for (unsigned int p = 0; p < M; p++) {
      int np = 0;
      for (unsigned int q = 0; q < N; q++)
        if (p & (((ull) 1) << q))
          np = np | (((ull) 1) << P[q]);
      if (conf & (((ull) 1) << p))
        conf2 = conf2 | (((ull) 1) << np);
    }
    if (conf2 < conf)
      return false; // faster to exit
  } while (next_permutation(P.begin(), P.end()));
  return true;
}

bool config_lt(config c1, config c2) {
  for (int i = 0; i < M; i++)
    if (c1[i] != c2[i])
      if (!c1[i])
        return true; // c1 < c2 indeed
  return false; // tie
}

// get the minimal configuration of conf under permutations to the base elements
// of the lattice
void get_minimal(config conf, config &ans) {
  vector<int> P;
  for (unsigned int i = 0; i < N; i++)
    P.push_back(i);
  do {
    config conf2 = 0;
    for (unsigned int p = 0; p < (((ull) 1) << N); p++) {
      int np = 0;
      for (unsigned int q = 0; q < N; q++)
        if (p & (((ull) 1) << q))
          np = np | (((ull) 1) << P[q]);
      if (conf[p])
        conf2[np] = 1;
    }
    if (config_lt(conf2, ans)) 
      for (int i = 0; i < M; i++)
        ans[i] = conf2[i];
  } while (next_permutation(P.begin(), P.end()));
}


void read_config(char *s, unsigned int n, config &v) {
  for (unsigned int i = 0; i < n; i++) {
    if (s[i] == '1')
      v[i] = 1;
  }
}

int main() {
  ull count = 0, totalcount = 0;
  //unordered_set<config> tested;
  //tested.clear();
  while (true) {
    char buf[M + 3];
    int ret = scanf("%s", buf);
    if (ret != 1)
      break;
    vect v;
    config c;
    read_config(buf, M, c);
    config crc;
    //get_minimal(c, crc);
    crc = c;
    if (!monotone(crc)) {
      printf("ERROR: not monotone: ");
      print_val(crc, M);
      printf("\n");
      return 2;
    }
//    if (!minimal(crc)) {
//      printf("ERROR: %llu is not minimal\n", crc);
//      print_val(crc, M);
//      printf("\n");
//      return 1;
//    }
    /*if (DISALLOW_DUPES && tested.find(crc) != tested.end()) {
      printf("DUPE already tested: ");
      print_val(crc, M);
      printf("\n");
      return 2;
    }
    if (DISALLOW_DUPES)
      tested.insert(crc);*/
    //printf("####### CONSIDERING (%llu): ", count);
    //print_val(crc, M);
    //printf("\n");
    if (!process(crc)) {
      vect v;
      config_to_vect(crc, v);
      int nrm = absweight(v);
      printf("COUNTEREXAMPLE %d: ", nrm);
      print_val(crc, M);
      printf("\n");
      //return 1;
    } else {
    count++;
    }
    totalcount++;
  }
  printf("reached %llu minimal non-constant monotone functions out of %llu\n", count, totalcount);
  return 0;
}

