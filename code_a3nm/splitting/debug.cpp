#include<cstdio>
#include <nmmintrin.h>
#include <stdint.h>
  typedef unsigned __int128 state; 
  typedef unsigned long long ull;
int  popcnt_u128 (state n)
{
    const uint64_t      n_hi    = n >> 64;
    const uint64_t      n_lo    = n;
    const uint_fast8_t  cnt_hi  = __builtin_popcount(n_hi);
    const uint_fast8_t  cnt_lo  = __builtin_popcount(n_lo);
    const uint_fast8_t  cnt     = cnt_hi + cnt_lo;

    return  cnt;
}


int main () {
  state x = 0;
  x |= (((state) 1) << 100);
  ull val = x >> 64;
  printf("val: %lld\n", val);
  printf("popcount: %d\n", __builtin_popcountll(val));
  printf("value %d\n", __builtin_popcountll(68719476736LL));
  printf("%d", popcnt_u128(x));
}
