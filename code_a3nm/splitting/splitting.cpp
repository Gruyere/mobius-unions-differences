#include <cstdio>
#include <iostream>
#include <bitset>
#include <cassert>
#include <cmath>

using namespace std;

#define MAXN 128

typedef unsigned __int128 state;
typedef unsigned long long ull;

int N; // number of base elements
int K; // number of base elements


// https://stackoverflow.com/q/55008994

#include <nmmintrin.h>
#include <stdint.h>

int  popcnt_u128 (state n)
{
    const uint64_t      n_hi    = n >> 64;
    const uint64_t      n_lo    = n;
    const uint_fast8_t  cnt_hi  = __builtin_popcountll(n_hi);
    const uint_fast8_t  cnt_lo  = __builtin_popcountll(n_lo);
    const uint_fast8_t  cnt     = cnt_hi + cnt_lo;

    return  cnt;
}





ull euler (state config) {
  // euler
  ull euler = 0;
  for (state buf = 0; buf < (((state) 1) << N); buf++)
    if (config & (((state) 1) << buf))
      euler += (popcnt_u128(buf) % 2 ? -1 : 1);
  return euler;
}

ull eulerm (state config, int basis, int half) {
  // euler
  ull euler = 0;
  for (state buf = 0; buf < (((state) 1) << N); buf++)
    if (config & (((state) 1) << buf))
      if ((half == 1 && buf & (1 << basis)) |
          (half == 0 && !(buf & (1 << basis))))
      euler += (popcnt_u128(buf & ~(1 << basis)) % 2 ? -1 : 1);
  return euler;
}

int main(int argc, char **argv) {
  assert(argc == 3);
  N = atoi(argv[1]);
  K = atoi(argv[2]);

  assert(N > 0);
  assert((1 << N) <= MAXN);
  assert(MAXN <= 8 * sizeof(state));

  ull n_tested = 0, n_nontriv = 0;

  bool ok = true;

  while (++n_tested) {
    int NSET; // number of sets to consider
    int ret = scanf("%d", &NSET);
    if (ret != 1)
      break;

    state config = 0;

    for (int i = 0; i < NSET; i++) {
      int buf;
      scanf("%d", &buf);
      //printf("scan set %lld\n", buf);
      config |= (((state) 1) << buf);
      // close downwards
      int ncount = 0;
      for (int buf2 = 0; buf2 < buf;
          buf2 = (((buf2 | ~(buf))+1) & buf)) {
        ncount++;
        config |= (((state) 1) << buf2);
      }
    }
    if (!(n_tested % 1000000))
      cout << "Seen set (high bits are zero) " << n_tested << " (" << n_nontriv << " nontrivial): " << bitset<MAXN>(config) << endl;

    ull eulerv = euler(config);
    //printf("euler is %lld\n", eulerv);
    if (llabs(eulerv) > K)
      continue; // ignore, bad euler

    n_nontriv++;
    // try splits
    bool split = false;
    for (int i = 0; i < N; i++) {
      ull euler0 = eulerm(config, i, 0);
      ull euler1 = eulerm(config, i, 1);
      if (llabs(euler0) > K || llabs(euler1) > K)
        continue; // fail
      split = true;
      break;
    }
    if (!split) {
      printf("fail to split:\n");
      std::bitset<MAXN> hi{static_cast<unsigned long long>(config >> 64)},
                 lo{static_cast<unsigned long long>(config)},
                 bits{(hi << 64) | lo};
      cout << bits << endl;
      printf("euler: %lld\n", eulerv);
      printf("popcount: %d\n", popcnt_u128(config));
      ok = false;
      break;
    }
  }
  if (!ok) {
    printf("fail\n");
  } else {
    printf("success\n");
  }
  return 0;
}
